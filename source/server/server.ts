import * as http from 'http';
import * as url from 'url';
import * as path from 'path';
import * as fs from 'fs';
import * as socketio from 'socket.io';
import * as express from 'express';
import * as sizeOf from 'image-size';
import * as glob from 'glob';

import { GameConfig, GameInfo } from '../shared/gameinfo';
import * as Colors from '../shared/colors';
import { State, Command, User } from '../shared/state';


let unzipper = require('unzipper');


(async () => {
	let app = express();


	let games: GameInfo[] = [];
	for(const filename of fs.readdirSync('./games/')) {
		if(fs.existsSync('./games/' + filename + '/game.json')) {
			let game = JSON.parse(fs.readFileSync('./games/' + filename + '/game.json', 'utf8'));
			game.url = 'games/' + filename + '/';
			games.push(game);

			if(!fs.existsSync('./games/' + filename + '/resources.json')) {
				app.get('/games/' + filename + '/resources.json', (req, res) => {
					let path = './games/' + filename + '/';

					let resources: { [index: string]: { width: number, height: number }} = {};

					for(const imagepath of glob.sync(path + '**/*.{png,jpg}', { nosort: true })) {
						let dimensions = sizeOf(imagepath);
						resources[imagepath.substr(path.length)] = { width: dimensions.width, height: dimensions.height };
					}

					res.send(resources);
				});
			}

			app.use('/games/' + filename, express.static('games/' + filename));
		}
	}


	for(const filepath of glob.sync('./games/*.zip', { nosort: true })) {
		let name = path.basename(filepath, '.zip');

		let directory = await unzipper.Open.file(filepath);

		let gameConfigBuffer = null;
		let hasResources     = false;
		for(const entry of directory.files) {
			if(entry.path == "game.json")
				gameConfigBuffer = entry.buffer();
			if(entry.path == "resources.json")
				hasResources = true;
		}
		if(!gameConfigBuffer)
			continue;

		let game = JSON.parse(await gameConfigBuffer);
		game.url = 'games/' + name + '/';
		games.push(game);

		if(!hasResources) {
			app.get('/games/' + name + '/resources.json', (req, res) => {
				let resources: { [index: string]: { width: number, height: number }} = {};
				Promise.all(directory.files.map(async (entry: any) => {
					if(entry.path.match(/\.(png|jpg)$/)) {
						let buffer     = await entry.buffer();
						let dimensions = sizeOf(buffer);
						resources[entry.path] = { width: dimensions.width, height: dimensions.height };
					}
				})).then(() => {
					res.send(resources);
				});
			});
		}

		app.get('/games/' + name + '/*', (req, res) => {
			for(const entry of directory.files) {
				if(entry.path == req.params[0]) {
					entry.buffer().then((buffer: any) => {
						res.send(buffer);
					});
				}
			}
		});
	}


	games.sort((a: GameConfig, b: GameConfig) => {
		if(a.name > b.name)
			return 1;
		if(a.name < b.name)
			return -1;
		return 0;
	});


	app.use(express.static(path.join(__dirname, '../../public')));

	let server = new http.Server(app);
	let io = socketio(server, { serveClient: false });


	let state = new State();


	io.on('connection', (socket: SocketIO.Socket) => {
		let ip = socket.request.connection.remoteAddress;

		let userId = state.nextUserId;

		// find an unused color
		let colors = Colors.PlayerColors.slice();
		for(let key in state.users) {
			let c = state.users[key].color;
			for(let i = 0; i < colors.length; i++) {
				if(colors[i][0] == c[0] && colors[i][1] == c[1] && colors[i][2] == c[2]) {
					colors.splice(i, 1);
					break;
				}
			}
		}
		if(colors.length == 0)
			colors.push(Colors.PlayerColors[Colors.PlayerColors.length - 1]);

		let commands: Command[] = [
			{ type: 'CreateUserId', id: userId },
			{ type: 'CreateUser',   id: userId, properties: { color: colors[0] } }
		];
		state = state.mutable();
		for(let i = 0; i < commands.length; i++)
			state.execute(commands[i]);
		state.immutable();

		let user = <User>state.user(userId);

		console.log('connection from ' + ip + ': ' + user.name);

		socket.emit('hello', {
			'userId': userId,
			'state':  state,
			'games':  games
		});

		// notify everyone else
		socket.broadcast.emit('commands', {
			user:        userId,
			description: user.name + ' connected',
			commands:    commands
		});

		socket.on('commands', (msg: { description: string; commands: Command[]; }) => {
			// fs.appendFileSync('log.txt', JSON.stringify(msg) + "\n", encoding='utf8');
			// console.log(ip + ': ' + msg.description);

			try {
				let newState = state.mutable();
				for(let i = 0; i < msg.commands.length; i++)
					newState.execute(msg.commands[i]);
				state = newState;
				state.immutable();

				socket.emit('accept');
				socket.broadcast.emit('commands', {
					user:        userId,
					description: msg.description,
					commands:    msg.commands
				});
			}
			catch(e) {
				console.log(e);
				console.log('Command failed, rollback!');
				socket.emit('decline');
			}
		});

		socket.on('disconnect', () => {
			let userName = (<User>state.user(userId)).name;
			console.log('disconnect from ' + ip + ': ' + userName);

			let command: Command = { type: 'DeleteUser', id: userId };
			state = state.mutable();
			state.execute(command);
			state.immutable();
			socket.broadcast.emit('commands', {
				user:        userId,
				description: userName + ' left',
				commands:    [command]
			});
		});
	});


	server.listen(31337, () => {
		console.log('listening on *:31337');
	});

})();
