export class Environment {
	private nameCache: { [index: string]: any } = {};
	private evaler: (code: string) => any;

	constructor(scripts: string[], globals: { [index: string]: any }) {
		let s = '';

		// populate the current scope with all provided globals
		for(let name in globals)
			s += "let " + name + " = globals['" + name + "'];";

		// run the initialization script, augmenting the current scope
		for(let script of scripts)
			s += script;

		// Return a window into the current scope.
		// As soon as we leave this constructor, the current scope is lost.
		// This function allows us to access names from and run code in the current scope later on.
		s += 'return function(code) { return eval(code); }';

		s = '(function() { ' + s + ' })()';

		this.evaler = eval(s);
	}

	get(name: string): any {
		if(this.nameCache[name] !== undefined)
			return this.nameCache[name];

		let value = this.evaler('(typeof ' + name + ' === "undefined" ? undefined : ' + name + ')');
		this.nameCache[name] = value;
		return value;
	}

	eval(code: string): any {
		return this.evaler(code);
	}
}
