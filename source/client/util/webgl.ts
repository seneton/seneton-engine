import { mat2, mat4, vec2, vec3 } from 'gl-matrix';
import { dropShadow } from './shadows';


interface PickingRegion<Id> {
	pos:     vec2;
	a:       vec2;
	b:       vec2;
	id:      Id;
	content: HTMLCanvasElement | HTMLImageElement | number[];
}


enum CachedImageState { Error, Loading, Loaded }


interface CachedImage {
	state:   CachedImageState;
	image:   HTMLCanvasElement | HTMLImageElement;
	texture: WebGLTexture;
	smaller: WebGLTexture[];
	larger:  WebGLTexture[];
}


interface RenderBounds {
	left:   number;
	right:  number;
	top:    number;
	bottom: number;
}


export enum TextAlign { Left, Center, Right }


export interface RenderRectOptions<Id> {
	id?:          Id;
	color?:       number[];
	shadow?:      number;
	shadowColor?: number[];
}


export interface RenderTextOptions<Id> {
	id?:          Id;
	font?:        string;
	style?:       string;
	size?:        number;
	align?:       TextAlign;
	color?:       number[];
	shadow?:      number;
	shadowColor?: number[];
}


export interface RenderImageOptions<Id> {
	id?:          Id;
	tint?:        number[];
	shadow?:      number;
	shadowColor?: number[];
	width?:       number;
	height?:      number;
}


export class Context<Id> {
	gl:              WebGLRenderingContext;

	imageCache:      { [index: string]: CachedImage; } = {};
	onload:          Function;
	loadCounter:     number = 0;

	textureShader:   WebGLProgram;
	solidShader:     WebGLProgram;
	boxShadowShader: WebGLProgram;

	buffer:          WebGLBuffer;
	projection:      mat4;
	modelview:       mat4;
	modelviewStack:  mat4[];
	scaling:         number;
	scalingStack:    number[];
	regions:         PickingRegion<Id>[];
	pivots:          Map<Id, vec2>;

	constructor(gl: WebGLRenderingContext) {
		this.gl = gl;

		// initialize the shaders
		this.textureShader = this.createShader(
			'attribute vec2       aVertexPosition;' +
			'varying highp vec2   vTextureCoord;' +
			'uniform mediump vec2 uSize;' +
			'uniform mediump mat4 uProjection;' +
			'uniform mediump mat4 uModelView;' +
			'void main() {' +
				'vTextureCoord = vec2(aVertexPosition.x, aVertexPosition.y);' +
				'gl_Position   = uProjection * uModelView * vec4(aVertexPosition * uSize, 0.0, 1.0);' +
			'}',
			'varying highp vec2   vTextureCoord;' +
			'uniform sampler2D    uSampler;' +
			'uniform mediump vec4 uTint;' +
			'void main() {' +
				'gl_FragColor = texture2D(uSampler, vTextureCoord) * uTint;' +
			'}'
		);
		this.solidShader = this.createShader(
			'attribute vec2       aVertexPosition;' +
			'uniform mediump vec2 uSize;' +
			'uniform mediump mat4 uProjection;' +
			'uniform mediump mat4 uModelView;' +
			'void main() {' +
				'gl_Position = uProjection * uModelView * vec4(aVertexPosition * uSize, 0.0, 1.0);' +
			'}',
			'uniform mediump vec4 uColor;' +
			'void main() {' +
				'gl_FragColor = uColor;' +
			'}'
		);
		this.boxShadowShader = this.createShader(
			'attribute vec2       aVertexPosition;' +
			'uniform mediump vec2 uSize;' +
			'uniform mediump mat4 uProjection;' +
			'uniform mediump mat4 uModelView;' +
			'varying mediump vec2 vRelPos;' +
			'void main() {' +
				'vRelPos     = aVertexPosition * uSize;' +
				'gl_Position = uProjection * uModelView * vec4(aVertexPosition * uSize, 0.0, 1.0);' +
			'}',
			'uniform sampler2D     uSampler;' +
			'uniform mediump vec2  uSize;' +
			'uniform mediump vec4  uColor;' +
			'uniform mediump float uRadius;' +
			'varying mediump vec2  vRelPos;' +
			'void main() {' +
				'gl_FragColor = texture2D(uSampler, (1.0 - max(vec2(0.0, 0.0), abs(vRelPos - 0.5 * uSize) - (0.5 * uSize - vec2(uRadius, uRadius))) / uRadius) / 3.0) * uColor;' +
			'}'
		);

		//
		let buffer = this.gl.createBuffer();
		if(!buffer)
			throw new Error('cannot create GL buffer');
		this.buffer = buffer;
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
		this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array([
			0, 0,
			1, 0,
			0, 1,
			1, 1
		]), this.gl.STATIC_DRAW);

		//
		this.projection = mat4.create();
		this.resize(this.gl.canvas.width, this.gl.canvas.height);

		//
		this.modelview = mat4.create();
		this.modelviewStack = [];

		// stores the current scaling, read-only. this can be used to select the right mipmapping texture
		this.scaling = 1.0;
		this.scalingStack = [];

		//
		this.regions = [];
		this.pivots = new Map();

		//
		this.gl.useProgram(this.textureShader);
		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
		this.gl.vertexAttribPointer(this.gl.getAttribLocation(this.textureShader, 'aVertexPosition'), 2, this.gl.FLOAT, false, 0, 0);

		//
		this.gl.clearColor(0.0, 0.0, 0.0, 0.0);
		this.gl.enable(this.gl.BLEND);
		this.gl.blendFunc(this.gl.ONE, this.gl.ONE_MINUS_SRC_ALPHA);
		this.gl.activeTexture(this.gl.TEXTURE0);
	}

	clear(): void {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT);
		mat4.identity(this.modelview);
		this.regions = [];
		this.pivots.clear();
		this.scaling = 1.0;
	}

	resize(width: number, height: number): void {
		this.gl.viewport(0, 0, width, height);

		mat4.identity(this.projection);
		mat4.ortho(this.projection, 0, this.gl.canvas.width, this.gl.canvas.height, 0, -1.0, 1.0);
	}

	push(): void {
		this.modelviewStack.push(mat4.clone(this.modelview));
		this.scalingStack.push(this.scaling);
	}

	pop(): void {
		let m = this.modelviewStack.pop();
		if(m)
			this.modelview = m;

		let s = this.scalingStack.pop();
		if(s)
			this.scaling = s;
	}

	rotate(rad: number): void {
		mat4.rotateZ(this.modelview, this.modelview, rad);
	}

	scale(f: number): void {
		mat4.scale(this.modelview, this.modelview, vec3.fromValues(f, f, 0.0));
		this.scaling *= f;
	}

	translate(v: Float32Array): void {
		v[0] = Math.round(v[0]);
		v[1] = Math.round(v[1]);
		mat4.translate(this.modelview, this.modelview, vec3.fromValues(v[0], v[1], 0.0));
	}

	addRegion(width: number, height: number, id: Id, content: HTMLImageElement | HTMLCanvasElement | number[]): void {
		let pos = vec2.fromValues(0.0, 0.0), a = vec2.fromValues(width, 0.0), b = vec2.fromValues(0.0, height);
		vec2.transformMat4(pos, pos, this.modelview);
		vec2.transformMat4(a , a , this.modelview);
		vec2.subtract(a, a, pos);
		vec2.transformMat4(b , b , this.modelview);
		vec2.subtract(b, b, pos);
		this.regions.push({
			pos:     pos,
			a:       a,
			b:       b,
			id:      id,
			content: content
		});
	}

	setPivot(id: Id): void {
		let pos = vec2.fromValues(0.0, 0.0);
		vec2.transformMat4(pos, pos, this.modelview);
		this.pivots.set(id, pos);
	}

	pivot(id: Id): vec2 | undefined {
		return this.pivots.get(id);
	}

	pick(pos: vec2): Id | undefined {
		let context : CanvasRenderingContext2D | undefined = undefined; // this will be created on-demand and reused

		for(let r = this.regions.length - 1; r >= 0; r--) {
			let region = this.regions[r];

			let inv = mat2.create();
			mat2.invert(inv, mat2.fromValues(region.a[0], region.a[1], region.b[0], region.b[1]));

			let localPos = vec2.create();
			vec2.subtract(localPos, pos, region.pos);
			vec2.transformMat2(localPos, localPos, inv);

			if(localPos[0] >= 0.0 && localPos[0] <= 1.0 && localPos[1] >= 0.0 && localPos[1] <= 1.0) {
				if(region.content instanceof HTMLCanvasElement) {
					let ctx = region.content.getContext('2d');
					if(ctx) {
						let data = ctx.getImageData(localPos[0] * region.content.width, localPos[1] * region.content.height, 1, 1).data;
						if(data[3] == 0)
							continue;
					}
				}
				else if(region.content instanceof HTMLImageElement) {
					if(context === undefined) {
						let canvas = document.createElement('canvas');
						context = canvas.getContext('2d') || undefined;
					}
					if(context) {
						context.drawImage(region.content, -localPos[0] * region.content.width, -localPos[1] * region.content.height);
						let data = context.getImageData(0, 0, 1, 1).data;
						if(data[3] == 0)
							continue;
					}
				}
				else {
					if(region.content[3] == 0)
						continue;
				}

				return region.id;
			}
		}

		return undefined;
	}

	pickAll(pos: vec2): Id[] {
		let ids: Id[] = [];
		for(let r = this.regions.length - 1; r >= 0; r--) {
			let region = this.regions[r];

			let inv = mat2.create();
			mat2.invert(inv, mat2.fromValues(region.a[0], region.a[1], region.b[0], region.b[1]));

			let localPos = vec2.subtract(vec2.create(), pos, region.pos);
			vec2.transformMat2(localPos, localPos, inv);

			if(localPos[0] >= 0.0 && localPos[0] <= 1.0 && localPos[1] >= 0.0 && localPos[1] <= 1.0)
				ids.push(region.id);
		}
		return ids;
	}

	pickRect(left: number, top: number, right: number, bottom: number): Id[] {
		let inv = mat2.create();
		mat2.invert(inv, mat2.fromValues(right - left, 0.0, 0.0, bottom - top));

		let ids: Id[] = [];
		for(let r = this.regions.length - 1; r >= 0; r--) {
			let region = this.regions[r];

			let tl = vec2.subtract(vec2.create(), region.pos, vec2.fromValues(left, top));
			vec2.transformMat2(tl, tl, inv);
			if(tl[0] >= 0.0 && tl[0] <= 1.0 && tl[1] >= 0.0 && tl[1] <= 1.0) {
				let tr = vec2.subtract(vec2.create(), vec2.add(vec2.create(), region.pos, region.a), vec2.fromValues(left, top));
				vec2.transformMat2(tr, tr, inv);
				if(tr[0] >= 0.0 && tr[0] <= 1.0 && tr[1] >= 0.0 && tr[1] <= 1.0) {
					let bl = vec2.subtract(vec2.create(), vec2.add(vec2.create(), region.pos, region.b), vec2.fromValues(left, top));
					vec2.transformMat2(bl, bl, inv);
					if(bl[0] >= 0.0 && bl[0] <= 1.0 && bl[1] >= 0.0 && bl[1] <= 1.0) {
						let br = vec2.subtract(vec2.create(), vec2.add(vec2.create(), region.pos, vec2.add(vec2.create(), region.a, region.b)), vec2.fromValues(left, top));
						vec2.transformMat2(br, br, inv);
						if(br[0] >= 0.0 && br[0] <= 1.0 && br[1] >= 0.0 && br[1] <= 1.0)
							ids.push(region.id);
					}
				}
			}
		}

		return ids;
	}

	bounds(id: Id): RenderBounds | undefined {
		let bounds: RenderBounds | undefined;
		for(let i = 0; i < this.regions.length; i++) {
			let region = this.regions[i];

			if(region.id != id)
				continue;

			let tl = region.pos,
			    tr = vec2.add(vec2.create(), region.pos, region.a),
			    bl = vec2.add(vec2.create(), region.pos, region.b),
			    br = vec2.add(vec2.create(), bl, region.a);

			let b = {
				left:   Math.min(tl[0], tr[0], bl[0], br[0]),
				top:    Math.min(tl[1], tr[1], bl[1], br[1]),
				right:  Math.max(tl[0], tr[0], bl[0], br[0]),
				bottom: Math.max(tl[1], tr[1], bl[1], br[1])
			};

			if(bounds === undefined)
				bounds = b;
			else {
				bounds.left   = Math.min(bounds.left  , b.left  );
				bounds.top    = Math.min(bounds.top   , b.top   );
				bounds.right  = Math.max(bounds.right , b.right );
				bounds.bottom = Math.max(bounds.bottom, b.bottom);
			}
		}

		return bounds;
	}

	private createShader(vertex: string, fragment: string): WebGLProgram {
		let program = this.gl.createProgram();
		if(!program)
			throw new Error('cannot create GL shader program');

		{
			let s = this.gl.createShader(this.gl.VERTEX_SHADER);
			this.gl.shaderSource(s, vertex);
			this.gl.compileShader(s);
			if(!this.gl.getShaderParameter(s, this.gl.COMPILE_STATUS))
				throw 'Could not compile vertex shader:\n\n' + this.gl.getShaderInfoLog(s);
			this.gl.attachShader(program, s);
		}

		{
			let s = this.gl.createShader(this.gl.FRAGMENT_SHADER);
			this.gl.shaderSource(s, fragment);
			this.gl.compileShader(s);
			if(!this.gl.getShaderParameter(s, this.gl.COMPILE_STATUS))
				throw 'Could not compile fragment shader:\n\n' + this.gl.getShaderInfoLog(s);
			this.gl.attachShader(program, s);
		}

		this.gl.linkProgram(program);
		if(!this.gl.getProgramParameter(program, this.gl.LINK_STATUS))
			throw 'Could not link the shader program!';

		return program;
	}

	renderRect(width: number, height: number, opts?: RenderRectOptions<Id>): void {
		if(opts === undefined)
			opts = {};
		if(opts.color === undefined)
			opts.color = [1.0, 1.0, 1.0, 1.0];
		if(opts.shadow === undefined)
			opts.shadow = 0.0;
		if(opts.shadowColor === undefined)
			opts.shadowColor = [0.0, 0.0, 0.0, 1.0];

		if(opts.id !== undefined)
			this.addRegion(width, height, opts.id, opts.color);

		let corner = vec2.fromValues(-opts.shadow, -opts.shadow);
		vec2.transformMat4(corner, corner, this.modelview);
		let center = vec2.fromValues(0.5 * width, 0.5 * height);
		vec2.transformMat4(center, center, this.modelview);
		let radius = vec2.distance(corner, center);
		if(!collideAABBCircle(0, 0, this.gl.canvas.width, this.gl.canvas.height, center[0], center[1], radius))
			return;

		if(opts.shadow > 0.0) {
			let key = 'rectshadow:' + opts.shadow;

			if(this.imageCache[key] === undefined) {
				let canvas  = document.createElement('canvas');
				canvas.width  = Math.ceil(opts.shadow);
				canvas.height = Math.ceil(opts.shadow);
				let context = canvas.getContext('2d');
				if(context) {
					context.fillStyle = '#ffffff';
					context.fillRect(0, 0, canvas.width, canvas.height);

					let shadow = dropShadow(canvas, opts.shadow);
					if(shadow) {
						let texture = this.gl.createTexture();
						if(texture) {
							this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
							this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
							this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, shadow);
							this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
							this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
							this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
							this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
							this.gl.bindTexture(this.gl.TEXTURE_2D, null);

							this.imageCache[key] = {
								state:   CachedImageState.Loaded,
								image:   shadow,
								texture: texture,
								smaller: [],
								larger:  []
							}
						}
					}
				}
			}

			this.push();
				this.translate(vec2.fromValues(-Math.ceil(opts.shadow), -Math.ceil(opts.shadow)));

				this.gl.bindTexture(this.gl.TEXTURE_2D, this.imageCache[key].texture);
				this.gl.useProgram(this.boxShadowShader);
				this.gl.enableVertexAttribArray(this.gl.getAttribLocation(this.boxShadowShader, 'aVertexPosition'));
				this.gl.uniform1i(this.gl.getUniformLocation(this.boxShadowShader, 'uSampler'), 0);
				this.gl.uniform2f(this.gl.getUniformLocation(this.boxShadowShader, 'uSize'), width + 2.0 * Math.ceil(opts.shadow), height + 2.0 * Math.ceil(opts.shadow));
				this.gl.uniform4fv(this.getUniformLocationOrThrow(this.boxShadowShader, 'uColor'), new Float32Array(opts.shadowColor));
				this.gl.uniform1f(this.gl.getUniformLocation(this.boxShadowShader, 'uRadius'), opts.shadow);
				this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.boxShadowShader, 'uProjection'), false, this.projection);
				this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.boxShadowShader, 'uModelView'), false, this.modelview);

				this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
			this.pop();
		}

		this.gl.useProgram(this.solidShader);
		this.gl.enableVertexAttribArray(this.gl.getAttribLocation(this.solidShader, 'aVertexPosition'));
		this.gl.uniform2f(this.gl.getUniformLocation(this.solidShader, 'uSize'), width, height);
		this.gl.uniform4fv(this.getUniformLocationOrThrow(this.solidShader, 'uColor'), new Float32Array(opts.color));
		this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.solidShader, 'uProjection'), false, this.projection);
		this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.solidShader, 'uModelView'), false, this.modelview);

		this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
	}

	renderText(text: string, opts?: RenderTextOptions<Id>): void {
		if(text.length == 0)
			return;

		if(opts === undefined)
			opts = {};
		if(opts.color === undefined)
			opts.color = [1.0, 1.0, 1.0, 1.0];
		if(opts.size === undefined)
			opts.size = 20;
		if(opts.style === undefined)
			opts.style = '';
		if(opts.font === undefined)
			opts.font = 'Arial';
		if(opts.align === undefined)
			opts.align = TextAlign.Left;

		let key = 'text:' + opts.font + ':' + opts.style + ':' + opts.size + ':' + text;

		if(this.imageCache[key] === undefined) {
			let canvas  = document.createElement('canvas');
			let context = canvas.getContext('2d');
			if(!context)
				throw Error('2d context null');
			context.font  = opts.style + ' ' + opts.size + 'px ' + opts.font;
			canvas.width  = context.measureText(text).width;
			canvas.height = opts.size * 2.0;
			context.font         = opts.style + ' ' + opts.size + 'px ' + opts.font;
			context.textBaseline = 'top';
			context.fillStyle    = '#ffffff';
			context.fillText(text, 0, 0);

			let texture = this.gl.createTexture();
			if(!texture)
				throw Error('texture null');
			this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
			this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
			this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, canvas);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
			this.gl.bindTexture(this.gl.TEXTURE_2D, null);

			let t: CachedImage = {
				state:   CachedImageState.Loaded,
				image:   canvas,
				texture: texture,
				smaller: [],
				larger:  []
			};

			// generate additional mipmaps
			for(let i = 0; i < 4; i++) {
				let scale   = Math.pow(0.5, i + 1);
				let canvas  = document.createElement('canvas');
				let context = canvas.getContext('2d');
				if(!context)
					throw Error('2d context null');
				context.scale(scale, scale);
				context.font  = opts.style + ' ' + opts.size + 'px ' + opts.font;
				canvas.width  = scale * context.measureText(text).width;
				canvas.height = scale * opts.size * 2.0;
				if(canvas.width == 0 || canvas.height == 0)
					break;
				context.scale(scale, scale);
				context.font         = opts.style + ' ' + opts.size + 'px ' + opts.font;
				context.textBaseline = 'top';
				context.fillStyle    = '#ffffff';
				context.fillText(text, 0, 0);

				let texture = this.gl.createTexture();
				if(!texture)
					throw Error('texture null');
				this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
				this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
				this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, canvas);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
				this.gl.bindTexture(this.gl.TEXTURE_2D, null);

				t.smaller.push(texture);
			}
			for(let i = 0; i < 4; i++) {
				let scale   = Math.pow(2.0, i + 1);
				let canvas  = document.createElement('canvas');
				let context = canvas.getContext('2d');
				if(!context)
					throw Error('2d context null');
				context.scale(scale, scale);
				context.font  = opts.style + ' ' + opts.size + 'px ' + opts.font;
				canvas.width  = scale * context.measureText(text).width;
				canvas.height = scale * opts.size * 2.0;
				context.scale(scale, scale);
				context.font         = opts.style + ' ' + opts.size + 'px ' + opts.font;
				context.textBaseline = 'top';
				context.fillStyle    = '#ffffff';
				context.fillText(text, 0, 0);

				let texture = this.gl.createTexture();
				if(!texture)
					throw Error('texture null');
				this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
				this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
				this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, canvas);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
				this.gl.bindTexture(this.gl.TEXTURE_2D, null);

				t.larger.push(texture);
			}

			this.imageCache[key] = t;
		}

		let opts2: RenderImageOptions<Id> = {
			id:          opts.id,
			tint:        opts.color,
			shadow:      opts.shadow,
			shadowColor: opts.shadowColor
		};
		this.push();
			if(opts.align == TextAlign.Center)
				this.translate(vec2.fromValues(-Math.round(0.5 * this.imageCache[key].image.width), 0.0));
			else if(opts.align == TextAlign.Right)
				this.translate(vec2.fromValues(-this.imageCache[key].image.width, 0.0));
			this.renderImage(key, opts2);
		this.pop();
	}

	renderImage(src: (string | HTMLCanvasElement), opts?: RenderImageOptions<Id>): void {
		if(opts === undefined)
			opts = {};
		if(opts.tint === undefined)
			opts.tint = [1.0, 1.0, 1.0, 1.0];
		if(opts.shadow === undefined)
			opts.shadow = 0.0;
		if(opts.shadowColor === undefined)
			opts.shadowColor = [0.0, 0.0, 0.0, 1.0];

		if(typeof src === 'string' && this.imageCache[src] === undefined) {
			let img = new Image();
			let t: CachedImage = {
				state:   CachedImageState.Loading,
				image:   img,
				texture: null,
				smaller: [],
				larger:  []
			};
			this.loadCounter++;
			img.onload = () => {
				t.state = CachedImageState.Loaded;
				this.loadCounter--;

				let texture = this.gl.createTexture();
				if(!texture)
					throw Error('texture null');
				t.texture = texture;
				this.gl.bindTexture(this.gl.TEXTURE_2D, t.texture);
				this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
				this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, t.image);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
				this.gl.bindTexture(this.gl.TEXTURE_2D, null);

				// generate additional mipmaps
				let w     = t.image.width;
				let h     = t.image.height;
				let image = t.image;
				for(let i = 0; i < 4; i++) {
					let canvas = document.createElement('canvas');
					canvas.width  = Math.floor(w / 2);
					canvas.height = Math.floor(h / 2);
					let context = canvas.getContext('2d');
					if(!context)
						throw Error('2d context null');
					context.scale(0.5, 0.5);
					context.drawImage(image, 0, 0);

					let texture = this.gl.createTexture();
					if(!texture)
						throw Error('texture null');
					this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
					this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
					this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, canvas);
					this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
					this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
					this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
					this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
					this.gl.bindTexture(this.gl.TEXTURE_2D, null);

					t.smaller.push(texture);
					w     = Math.floor(w / 2);
					h     = Math.floor(h / 2);
					image = canvas;
				}

				if(this.onload && this.loadCounter == 0)
					this.onload();
			};
			img.onerror = () => {
				console.log("error: resource not found", img.src);
				t.state = CachedImageState.Error;
				this.loadCounter--;
				if(this.onload && this.loadCounter == 0)
					this.onload();
			};
			img.src = src;
			this.imageCache[src] = t;
			return;
		}

		let t: CachedImage;
		if(src instanceof HTMLCanvasElement) {
			let texture = this.gl.createTexture();
			if(!texture)
				throw Error('texture null');

			t = {
				state:   CachedImageState.Loaded,
				image:   src,
				texture: texture,
				smaller: [],
				larger:  []
			};

			this.gl.bindTexture(this.gl.TEXTURE_2D, t.texture);
			this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
			this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, src);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
			this.gl.bindTexture(this.gl.TEXTURE_2D, null);
		}
		else if(typeof src === 'string' && this.imageCache[src].state == CachedImageState.Loaded) {
			t = this.imageCache[src];
		}
		else
			return;

		let width   = opts.width  || t.image.width;
		let height  = opts.height || t.image.height;

		if(opts.id !== undefined)
			this.addRegion(width, height, opts.id, t.image);

		let corner = vec2.fromValues(-opts.shadow, -opts.shadow);
		vec2.transformMat4(corner, corner, this.modelview);
		let center = vec2.fromValues(0.5 * width, 0.5 * height);
		vec2.transformMat4(center, center, this.modelview);
		let radius = vec2.distance(corner, center);
		if(!collideAABBCircle(0, 0, this.gl.canvas.width, this.gl.canvas.height, center[0], center[1], radius))
			return;

		if(opts.shadow > 0.0) {
			let key = 'shadow:' + width + ':' + height + ':' + opts.shadow + ':' + src;

			if(this.imageCache[key] === undefined) {
				let image = t.image;
				if(width < t.image.width && height < t.image.height) {
					let cv = document.createElement('canvas');
					cv.width  = width;
					cv.height = height;
					let context = cv.getContext('2d');
					if(!context)
						throw Error('2d context null');
					context.scale(width / t.image.width, height / t.image.height);
					context.drawImage(t.image, 0.0, 0.0);
					image = cv;
				}

				let shadow = dropShadow(image, opts.shadow);

				let texture2 = this.gl.createTexture();
				if(!texture2)
					throw Error('texture null');
				this.gl.bindTexture(this.gl.TEXTURE_2D, texture2);
				this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
				this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, shadow);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S,     this.gl.CLAMP_TO_EDGE);
				this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T,     this.gl.CLAMP_TO_EDGE);
				this.gl.bindTexture(this.gl.TEXTURE_2D, null);

				this.imageCache[key] = {
					state:   CachedImageState.Loaded,
					image:   shadow,
					texture: texture2,
					smaller: [],
					larger:  []
				};
			}

			this.push();
				this.translate(vec2.fromValues(-Math.ceil(opts.shadow), -Math.ceil(opts.shadow)));
				this.renderImage(key, {
					width:  width  + 2 * Math.ceil(opts.shadow),
					height: height + 2 * Math.ceil(opts.shadow),
					tint:   opts.shadowColor
				});
			this.pop();
		}

		if(opts.tint[3] == 0.0)
			return;

		let mipmap = Math.floor(Math.log(this.scaling * width / t.image.width) / Math.log(0.5));
		let texture = t.texture;
		if(mipmap > 0 && t.smaller.length > 0)
			texture = t.smaller[Math.min(mipmap - 1, t.smaller.length - 1)];
		else if(mipmap < 0 && t.larger.length > 0)
			texture = t.larger[Math.min(-mipmap - 1, t.larger.length - 1)];

		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.useProgram(this.textureShader);
		this.gl.enableVertexAttribArray(this.gl.getAttribLocation(this.textureShader, 'aVertexPosition'));
		this.gl.uniform1i(this.gl.getUniformLocation(this.textureShader, 'uSampler'), 0);
		this.gl.uniform2f(this.gl.getUniformLocation(this.textureShader, 'uSize'), width, height);
		this.gl.uniform4fv(this.getUniformLocationOrThrow(this.textureShader, 'uTint'), new Float32Array(opts.tint));
		this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.textureShader, 'uProjection'), false, this.projection);
		this.gl.uniformMatrix4fv(this.getUniformLocationOrThrow(this.textureShader, 'uModelView'), false, this.modelview);

		this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, 4);
	}

	// helper function to avoid returning null and thereby satisfying typescript strict mode
	private getUniformLocationOrThrow(program: WebGLProgram, name: string): WebGLUniformLocation {
		let location = this.gl.getUniformLocation(program, name);
		if(!location)
			throw Error('invalid gl shared location');
		return location;
	}
}


function collideAABBCircle(x1: number, y1: number, w: number, h: number, x2: number, y2: number, r: number): boolean {
	if(collideAABBPoint(x1, y1, w, h, x2, y2))
		return true;
	if(!collideAABBPoint(x1 - r, y1 - r, w + 2 * r, h + 2 * r, x2, y2))
		return false;
	return collideAABBPoint(x1 - r, y1    , w + 2 * r, h        , x2, y2)
	    || collideAABBPoint(x1    , y1 - r, w        , h + 2 * r, x2, y2)
	    || collideCirclePoint(x1    , y1    , r, x2, y2)
	    || collideCirclePoint(x1 + w, y1    , r, x2, y2)
	    || collideCirclePoint(x1    , y1 + h, r, x2, y2)
	    || collideCirclePoint(x1 + w, y1 + h, r, x2, y2);
}


function collideCircleCircle(x1: number, y1: number, r1: number, x2: number, y2: number, r2: number): boolean {
	return collideCirclePoint(x1, y1, r1 + r2, x2, y2);
}


function collideAABBPoint(x1: number, y1: number, w: number, h: number, x2: number, y2: number): boolean {
	return (x2 >= x1 && x2 <= x1 + w && y2 >= y1 && y2 <= y1 + h);
}


function collideCirclePoint(x1: number, y1: number, r: number, x2: number, y2: number): boolean {
	let a = x1 - x2,
	    b = y1 - y2;
	return (Math.sqrt(a * a + b * b) <= r);
}
