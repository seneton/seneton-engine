import { vec2 } from 'gl-matrix';


export * from 'gl-matrix';


// these are some additional functions that vanilla glMatrix does not provide
export namespace vec2e {
	export function rotate(out: vec2, a: vec2, rad: number): vec2 {
		let a0 = a[0], a1 = a[1];

		out[0] = a0 * Math.cos(rad) - a1 * Math.sin(rad);
		out[1] = a0 * Math.sin(rad) + a1 * Math.cos(rad);

		return out;
	}

	export function angle(a: vec2): number {
		return Math.atan2(a[1], a[0]);
	}
}


export namespace radian {
	export function rad(grad: number): number {
		return (grad / 180 * Math.PI);
	}

	export function grad(rad: number): number {
		return (rad * 180 / Math.PI);
	}

	export function normalize(a: number): number {
		a -= Math.floor(a / (2.0 * Math.PI)) * 2.0 * Math.PI;
		return (a < 0.0 ? a + 2.0 * Math.PI : a);
	}

	export function lerp(a: number, b: number, t: number) {
		a = radian.normalize(a);
		b = radian.normalize(b);
		if(b - a > Math.PI)
			a += 2.0 * Math.PI;
		else if(a - b > Math.PI)
			a -= 2.0 * Math.PI;
		return (1 - t) * a + t * b;
	}
}
