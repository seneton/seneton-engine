export function dropShadow(image: HTMLImageElement | HTMLCanvasElement, radius: number): HTMLCanvasElement {
	let data0: ImageData;
	if(image instanceof HTMLImageElement) {
		let canvas0 = document.createElement('canvas');
		canvas0.width  = image.width;
		canvas0.height = image.height;
		let context0 = canvas0.getContext('2d');
		if(!context0)
			throw Error('2d context null');
		context0.drawImage(image, 0, 0);
		data0 = context0.getImageData(0, 0, image.width, image.height);
	}
	else { // if(image instanceof HTMLCanvasElement)
		let ctx = image.getContext('2d');
		if(!ctx)
			throw Error('2d context null');
		data0 = ctx.getImageData(0, 0, image.width, image.height);
	}

	// Chrome canvas does not support shadows with transparent images:
	// https://code.google.com/p/chromium/issues/detail?id=100703
	// If this gets fixed, we can simply use context.shadowBlur.

	let size  = Math.ceil(radius);
	let sigma = radius / 2;

	let kernel: number[] = [];
	if(size == 0)
		kernel.push(1.0);
	else {
		let sum = 0.0;
		for(let i = -size; i <= size; i++) {
			let gauss = Math.exp(-0.5 * (i / sigma) * (i / sigma));
			kernel.push(gauss);
			sum += gauss;
		}
		for(let i = 0; i < kernel.length; i++)
			kernel[i] /= sum; // normalize the kernel
	}

	let width1  = data0.width + 2 * size;
	let height1 = data0.height;
	let data1 = new Uint8ClampedArray(width1 * height1);
	for(let dy = 0; dy < height1; dy++) {
		for(let dx = size; dx < width1 + size; dx++) {
			let a     = 0;
			let same  = true;
			let value = (dx - 3 * size >= 0) ? data0.data[4 * (dy * data0.width + dx - 3 * size) + 3] : 0;
			for(let i = 0; i < 2 * size + 1; i++) {
				if(dx - 3 * size + i >= 0 && dx - 3 * size + i < data0.width) {
					let v = data0.data[4 * (dy * data0.width + dx - 3 * size + i) + 3];
					a += kernel[i] * v;
					if(v != value)
						same = false;
				}
			}
			data1[dy * width1 + dx - size] = a;
			if(same) {
				while(dx - size < data0.width && data0.data[4 * (dy * data0.width + dx - 3 * size + 2 * size) + 3] == value) {
					dx++;
					data1[dy * width1 + dx - size] = value;
				}
			}
		}

		// for(let dx = 0; dx < width1; dx++) {
		// 	let a = 0;
		// 	for(let i = -size; i <= size; i++) {
		// 		if(dx + i >= size && dx + i < data0.width + size)
		// 			a += kernel[i + size] * data0.data[4 * (dy * data0.width + dx + i - size) + 3];
		// 	}
		// 	data1[dy * width1 + dx] = a;
		// }
	}

	let width2  = width1;
	let height2 = height1 + 2 * size;
	let data2 = new Uint8ClampedArray(4 * width2 * height2);
	for(let dx = 0; dx < width2; dx++) {
		for(let dy = size; dy < height2 + size; dy++) {
			let a     = 0;
			let same  = true;
			let value = (dy - 3 * size >= 0) ? data1[(dy - 3 * size) * width1 + dx] : 0;
			for(let i = 0; i < 2 * size + 1; i++) {
				if(dy - 3 * size + i >= 0 && dy - 3 * size + i < height1) {
					let v = data1[(dy - 3 * size + i) * width1 + dx];
					a += kernel[i] * v;
					if(v != value)
						same = false;
				}
			}
			data2[4 * ((dy - size) * width2 + dx) + 0] = 255;
			data2[4 * ((dy - size) * width2 + dx) + 1] = 255;
			data2[4 * ((dy - size) * width2 + dx) + 2] = 255;
			data2[4 * ((dy - size) * width2 + dx) + 3] = Math.min(255, 2.0 * a);
			if(same) {
				while(dy - size < height1 && data1[(dy - size) * width1 + dx] == value) {
					dy++;
					data2[4 * ((dy - size) * width2 + dx) + 0] = 255;
					data2[4 * ((dy - size) * width2 + dx) + 1] = 255;
					data2[4 * ((dy - size) * width2 + dx) + 2] = 255;
					data2[4 * ((dy - size) * width2 + dx) + 3] = Math.min(255, 2.0 * value);
				}
			}
		}
	}

	// for(let dy = 0; dy < height2; dy++) {
	// 	for(let dx = 0; dx < width2; dx++) {
	// 		let a = 0;
	// 		for(let i = -size; i <= size; i++) {
	// 			if(dy + i >= size && dy + i < height1 + size)
	// 				a += kernel[i + size] * data1[(dy + i - size) * width1 + dx];
	// 		}
	// 		data2[4 * (dy * width2 + dx) + 0] = 255;
	// 		data2[4 * (dy * width2 + dx) + 1] = 255;
	// 		data2[4 * (dy * width2 + dx) + 2] = 255;
	// 		data2[4 * (dy * width2 + dx) + 3] = Math.min(255, 2.0 * a);
	// 	}
	// }

	let canvas2 = document.createElement('canvas');
	canvas2.width  = width2;
	canvas2.height = height2;
	let context2 = canvas2.getContext('2d');
	if(!context2)
		throw Error('2d context null');
	let imagedata2 = context2.getImageData(0, 0, width2, height2);
	for(let i = 0; i < imagedata2.data.length; i++)
		imagedata2.data[i] = data2[i];
	context2.putImageData(imagedata2, 0, 0);

	return canvas2;
}
