export class Event {
	private handlers: { (): void; }[] = [];

	public on(handler: { (): void }): void {
		this.handlers.push(handler);
	}

	public off(handler: { (): void }): void {
		this.handlers = this.handlers.filter(h => h !== handler);
	}

	public emit() {
		this.handlers.slice(0).forEach(h => h());
	}
}


export class EventT<T = void> {
	private handlers: { (data: T): void; }[] = [];

	public on(handler: { (data: T): void }): void {
		this.handlers.push(handler);
	}

	public off(handler: { (data: T): void }): void {
		this.handlers = this.handlers.filter(h => h !== handler);
	}

	public emit(data: T) {
		this.handlers.slice(0).forEach(h => h(data));
	}
}
