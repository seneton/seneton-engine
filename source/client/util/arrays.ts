// returns the array [0 .. n)
export function iota(n: number): number[] {
	let array = new Array(n);
	for(let i = 0; i < n; i++)
		array[i] = [i];
	return array;
}


// Fisher-Yates Shuffle
export function shuffle(array: any[]): any[] {
	for(let i = array.length - 1; i > 0; i--) {
		let j = Math.floor(Math.random() * (i + 1));
		let temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
}
