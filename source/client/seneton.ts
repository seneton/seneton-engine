import { Uid, GameInfo } from '../shared/gameinfo';
import { Id, Command, Element, Layer, Action, TurnSequence, Group, Drawing, Position, State, User, JsonState, CreateLayerProperties, CreateGroupProperties, BaseElementProperties, RegularElementProperties, StackElementProperties, TextElementProperties } from '../shared/state';

import * as grids from './grids';

import { iota, shuffle } from './util/arrays';
import { Environment } from './util/environment';
import { Event, EventT } from './util/events';
import { vec2, mat2, vec2e } from './util/geometry';


interface LayerProperties extends CreateLayerProperties {
	id?: Id;
}


interface GroupProperties extends CreateGroupProperties {
	id?: Id;
}


interface CreateBaseElementProperties extends Partial<BaseElementProperties> {
	id?:       Id;
	layer?:    Id;
	x?:        number;
	y?:        number;
	left?:     number;
	right?:    number;
	top?:      number;
	bottom?:   number;
	rotation?: number;
	relTo?:    Id;
}

interface CreateRegularElementProperties extends CreateBaseElementProperties, Partial<RegularElementProperties> {
}

interface CreateStackElementProperties extends CreateBaseElementProperties, Partial<StackElementProperties> {
}

interface CreateTextElementProperties extends CreateBaseElementProperties, Partial<TextElementProperties> {
}


export interface Bounds {
	x:      number;
	y:      number;
	width:  number;
	height: number;
	top:    number;
	left:   number;
	right:  number;
	bottom: number;
}


export interface ResourcesList {
	[index: string]: { width: number, height: number };
}


export class Seneton {
	public executedCommands: Command[] = [];

	public userId:      Id = 1;
	public game:        GameInfo | undefined;
	public resources:   ResourcesList = {};
	public environment: Environment = new Environment([], {});
	public state:       State;

	// Events
	public gameChanged        = new Event;
	public commitCreated      = new EventT<{ description?: string, commands: Command[] }>();
	public groupsChanged      = new Event;
	public layersChanged      = new Event;
	public historyEntryAdded  = new Event;
	public environmentChanged = new Event;
	public stateChanged       = new Event;
	public turnChanged        = new Event;
	public usersChanged       = new Event;

	constructor() {
		this.state = new State();
		this.state.execute({ type: 'CreateUserId', id: this.userId });
		this.state.execute({ type: 'CreateUser',   id: this.userId, properties: {} });
	}

	public setUser(userId: Id) {
		this.userId = userId;
	}

	public setGame(game: GameInfo | undefined, libraries: string[], resources: ResourcesList) {
		this.game      = game;
		this.resources = resources;

		// initialize the API for game scripts and callbacks
		let api: any = {};

		// predefined grids and helper functions
		api.vec2                           = vec2;
		api.vec2e                          = vec2e;
		api.rad                            = grids.rad;
		api.grad                           = grids.grad;
		api.sqr                            = grids.sqr;
		api.griddy                         = grids.griddy;
		api.vHexagonal                     = grids.vHexagonal;
		api.rectGridWithOffsets            = grids.rectGridWithOffsets;
		api.rectGridWithOffsetsAndRotation = grids.rectGridWithOffsetsAndRotation;
		api.pointGrid                      = grids.pointGrid;
		api.relativeTo                     = grids.relativeTo.bind(null, this);
		api.relativeToGroup                = grids.relativeToGroup.bind(null, this);
		api.magnet                         = grids.magnet.bind(null, this);
		api.fixRotation                    = grids.fixRotation.bind(null, this);
		api.rotationGrid                   = grids.rotationGrid;
		api.oneOf                          = grids.oneOf.bind(null, this);
		api.offsetBy                       = grids.offsetBy.bind(null, this);

		// main seneton environment
		api.Seneton = this;

		// shadow global variables (executeAction will catch the 'undefined' and try 'Seneton.<name>' instead)
		api.moveTo = undefined;

		this.environment = new Environment(libraries, api);

		this.gameChanged.emit();
		this.environmentChanged.emit();
	}

	public setState(state: State) {
		this.state = state;
		this.stateChanged.emit();
	}

	public emitEvents(command: Command): void {
		switch(command.type) {
			case 'StartGame':               this.gameChanged.emit(); this.turnChanged.emit(); break;
			case 'Load':                    this.gameChanged.emit(); this.turnChanged.emit(); this.stateChanged.emit(); break;
			case 'CreateUserId':            break;
			case 'CreateLayerId':           break;
			case 'CreateGroupId':           break;
			case 'CreateElementId':         break;
			case 'CreateDrawingId':         break;
			case 'CreateUser':              this.usersChanged.emit(); break;
			case 'DeleteUser':              this.usersChanged.emit(); break;
			case 'CreateLayer':             this.layersChanged.emit(); break;
			case 'CreateGroup':             this.groupsChanged.emit(); break;
			case 'CreateElement':           this.groupsChanged.emit(); break;
			case 'CreateTextElement':       this.groupsChanged.emit(); break;
			case 'CreateStack':             this.groupsChanged.emit(); break;
			case 'DeleteElement':           this.groupsChanged.emit(); break;
			case 'CloneElementAt':          this.groupsChanged.emit(); break;
			case 'SetColor':                this.usersChanged.emit(); break;
			case 'RenameUser':              this.usersChanged.emit(); break;
			case 'Pin':                     break;
			case 'Unpin':                   break;
			case 'SendToTop':               break;
			case 'SendToBottom':            break;
			case 'SendToLayer':             break;
			case 'AddToGroup':              this.groupsChanged.emit(); break;
			case 'RemoveFromGroup':         this.groupsChanged.emit(); break;
			case 'MakeStackable':           break;
			case 'MakeUnstackable':         break;
			case 'Flip':                    break;
			case 'Peek':                    break;
			case 'Select':                  break;
			case 'Deselect':                break;
			case 'Rename':                  break;
			case 'MoveTo':                  break;
			case 'ReorderChildren':         break;
			case 'Pop':                     break;
			case 'Push':                    break;
			case 'ClearDrawings':           break;
			case 'StartFreehandDrawing':    break;
			case 'ContinueFreehandDrawing': break;
			case 'SetTurn':                 this.turnChanged.emit(); break;
			case 'SetTurnSequence':         this.turnChanged.emit(); break;
		}
	}

	public commit(description?: string): void {
		this.commitCreated.emit({
			description: description,
			commands:    this.executedCommands
		});
		this.executedCommands = [];
	}

	public execute(command: Command): void {
		this.executedCommands.push(command);
		this.state.execute(command);
		this.emitEvents(command);
	}

	thumbnailUrl(): string | undefined {
		if(!this.game || !this.game.thumbnail)
			return undefined;

		return this.game.url + this.game.thumbnail;
	}

	helpUrl(): string | undefined {
		if(!this.game || !this.game.help)
			return undefined;

		return this.game.url + this.game.help;
	}

	resourceUrl(path: string): string | undefined {
		if(!this.game)
			return undefined;
		return this.game.url + path;
	}

	me(): User | undefined {
		return this.state.user(this.userId);
	}

	user(id: Id): User | undefined {
		return this.state.user(id);
	}

	layer(id: Id): Layer | undefined {
		return this.state.layer(id);
	}

	group(id: Id): Group | undefined {
		return this.state.group(id);
	}

	element(id: Id): Element | undefined {
		return this.state.element(id);
	}

	drawing(id: Id): Drawing {
		return this.state.drawing(id);
	}

	groupExists(id: Id): boolean {
		return this.state.groupExists(id);
	}

	elementExists(id: Id): boolean {
		return this.state.elementExists(id);
	}

	drawingExists(id: Id): boolean {
		return this.state.drawingExists(id);
	}

	isRoot(id: Id): boolean {
		return this.state.isRoot(id);
	}

	rootElement(id: Id): Element | undefined {
		return this.state.rootElement(id);
	}

	skipStackChildren(id: Id): Element | undefined {
		let element = this.element(id);
		if(!element)
			return undefined;

		let elements: Element[] = [element];
		while(element.parent !== undefined) {
			element = this.element(element.parent);
			if(!element)
				return undefined;
			elements.push(element);
		}
		for(let i = elements.length - 1; i >= 0; i--) {
			if(elements[i].type === 'stack')
				return elements[i];
		}
		return elements[0];
	}

	parents(id: Id): Id[] {
		let parents: Id[] = [];
		let element = this.element(id);
		if(!element)
			return [];
		while(element.parent !== undefined) {
			parents.push(element.parent);
			element = this.element(element.parent);
			if(!element)
				return [];
		}
		return parents;
	}

	isOnStack(id: Id): boolean {
		let parents = this.parents(id);
		for(let parentId of parents) {
			let parent = this.element(parentId);
			if(parent && parent.type === 'stack')
				return true;
		}
		return false;
	}

	amIPeeking(id: Id): boolean {
		return this.state.isPeeking(id, this.userId);
	}

	isSomeonePeeking(id: Id): boolean {
		return this.state.isSomeonePeeking(id);
	}

	selection(): Id[] {
		let user = this.me();
		return (user ? user.selections : []);
	}

	isSelectedByMe(id: Id): boolean {
		return this.state.isSelected(id, this.userId);
	}

	elementSelection(id: Id): Id[] {
		return this.state.elementSelection(id);
	}

	elementsOnTop(elementId: Id): Id[] {
		let element = this.element(elementId);
		if(!element || element.position === undefined)
			return [];

		let pos = vec2.subtract(vec2.create(), vec2.fromValues(element.position.x, element.position.y), vec2e.rotate(vec2.create(), vec2.fromValues(element.pivot[0], element.pivot[1]), element.position.rotation));
		let a   = vec2e.rotate(vec2.create(), vec2.fromValues(element.width,  0.0), element.position.rotation);
		let b   = vec2e.rotate(vec2.create(), vec2.fromValues(0.0, element.height), element.position.rotation);
		let inv = mat2.create();
		mat2.invert(inv, mat2.fromValues(a[0], a[1], b[0], b[1]));

		function isOnTop(position: vec2): boolean {
			let localPos = vec2.subtract(vec2.create(), position, pos);
			vec2.transformMat2(localPos, localPos, inv);
			return (localPos[0] >= 0.0 && localPos[0] <= 1.0 && localPos[1] >= 0.0 && localPos[1] <= 1.0);
		}

		let ids: Id[] = [];

		// first check all following elements on the same layer
		let layer = this.layer(element.layer);
		if(!layer)
			return [];

		let position = layer.elements.indexOf(elementId);
		if(position != -1) {
			for(let i = position + 1; i < layer.elements.length; i++) {
				let id = layer.elements[i];
				let e  = this.element(id);
				if(e && e.position !== undefined && isOnTop(vec2.fromValues(e.position.x, e.position.y)))
					ids.push(id);
			}
		}

		// next check all root elements from the following layers
		for(let l = this.state.layerIds.indexOf(element.layer) + 1; l < this.state.layerIds.length; l++) {
			let layer = this.layer(this.state.layerIds[l]);
			if(layer) {
				for(let j = 0; j < layer.elements.length; j++) {
					let id = layer.elements[j];
					let e  = this.element(id);
					if(e && e.position !== undefined && isOnTop(vec2.fromValues(e.position.x, e.position.y)))
						ids.push(id);
				}
			}
		}

		return ids;
	}

	getElementOrder(ids: Id[]): Id[] {
		let count = 0;
		let order: { [index: number]: number } = {};
		for(let l = 0; l < this.state.layerIds.length; l++) {
			let layerId = this.state.layerIds[l];
			let layer   = this.state.layer(layerId);
			if(layer) {
				for(let e = 0; e < layer.elements.length; e++) {
					let elementId = layer.elements[e];
					order[elementId] = count++;
				}
			}
		}

		let sorted = ids.slice();
		sorted.sort((a, b) => (order[a] - order[b]));
		return sorted;
	}

	currentGameUid(): string {
		return this.state.gameUid;
	}

	saveGame(): JsonState {
		return this.state.save();
	}

	loadGame(data: JsonState): void {
		this.execute({ type: 'Load', data: data });
	}

	createUserId(): Id {
		let id = this.state.nextUserId;
		this.execute({ type: 'CreateUserId', id: id });
		return id;
	}

	createLayerId(): Id {
		let id = this.state.nextLayerId;
		this.execute({ type: 'CreateLayerId', id: id });
		return id;
	}

	createGroupId(): Id {
		let id = this.state.nextGroupId;
		this.execute({ type: 'CreateGroupId', id: id });
		return id;
	}

	createElementId(): Id {
		let id = this.state.nextElementId;
		this.execute({ type: 'CreateElementId', id: id });
		return id;
	}

	createDrawingId(): Id {
		let id = this.state.nextDrawingId;
		this.execute({ type: 'CreateDrawingId', id: id });
		return id;
	}

	createLayer(properties?: LayerProperties): Id {
		if(properties === undefined)
			properties = {};

		let id: Id = (properties.id !== undefined) ? properties.id : this.createLayerId();
		delete properties.id;

		this.execute({ type: 'CreateLayer', id: id, properties: properties });
		return id;
	}

	createGroup(properties?: GroupProperties): Id {
		if(properties === undefined)
			properties = {};

		let id: Id = (properties.id !== undefined) ? properties.id : this.createGroupId();
		delete properties.id;

		this.execute({ type: 'CreateGroup', id: id, properties: properties });
		return id;
	}

	private convertBaseElementProperties<T extends Partial<BaseElementProperties>>(properties: T & CreateBaseElementProperties): void {
		if(properties.id === undefined)
			properties.id = this.createElementId();

		if(properties.layer === undefined) {
			if(properties.parent !== undefined) {
				let parent = this.element(properties.parent);
				if(parent)
					properties.layer = parent.layer;
			}
			if(this.state.layerIds.length > 0)
				properties.layer = this.state.layerIds[this.state.layerIds.length - 1];
			else // create an implicit base layer
				properties.layer = this.createLayer();
		}

		if(properties.pivot === undefined && properties.width !== undefined && properties.height !== undefined)
			properties.pivot = [properties.width / 2, properties.height / 2];

		if(properties.position === undefined && (!properties.parent || (this.element(properties.parent) && (<Element>this.element(properties.parent)).type !== 'stack')))
			properties.position = { x: 0, y: 0, rotation: 0 };

		if(properties.position !== undefined) {
			if(properties.x !== undefined) {
				properties.position.x = properties.x;
				delete properties.x;
			}
			if(properties.left !== undefined) {
				properties.position.x = properties.left + (properties.pivot ? properties.pivot[0] : 0);
				delete properties.left;
			}
			if(properties.right !== undefined) {
				properties.position.x = properties.right + (properties.pivot ? properties.pivot[0] : 0) - (properties.width || 0);
				delete properties.right;
			}
			if(properties.y !== undefined) {
				properties.position.y = properties.y;
				delete properties.y;
			}
			if(properties.top !== undefined) {
				properties.position.y = properties.top + (properties.pivot ? properties.pivot[1] : 0);
				delete properties.top;
			}
			if(properties.bottom !== undefined) {
				properties.position.y = properties.bottom + (properties.pivot ? properties.pivot[1] : 0) - (properties.height || 0);
				delete properties.bottom;
			}
			if(properties.rotation !== undefined) {
				properties.position.rotation = properties.rotation;
				delete properties.rotation;
			}
		}

		// TODO allow using relTo with stack children
		if(properties.relTo !== undefined) {
			let e = this.element(properties.relTo);
			if(properties.position && e && e.position) {
				properties.position.x        += e.position.x;
				properties.position.y        += e.position.y;
				properties.position.rotation += e.position.rotation;
			}
			delete properties.relTo;
		}
	}

	createTextElement(properties: CreateTextElementProperties): Id {
		this.convertBaseElementProperties(properties);

		this.execute({ type: 'CreateTextElement', id: properties.id, layerId: properties.layer, properties: properties });

		return properties.id;
	}

	createElement(properties: CreateRegularElementProperties): Id {
		if(properties.faces !== undefined && properties.face === undefined)
			properties.face = 0;
		if(properties.width === undefined && properties.faces !== undefined && properties.face !== undefined && typeof properties.faces[properties.face] === 'string' && this.resources[<string>properties.faces[properties.face]] !== undefined)
			properties.width = this.resources[<string>properties.faces[properties.face]].width;
		if(properties.height === undefined && properties.faces !== undefined && properties.face !== undefined && typeof properties.faces[properties.face] === 'string' && this.resources[<string>properties.faces[properties.face]] !== undefined)
			properties.height = this.resources[<string>properties.faces[properties.face]].height;

		this.convertBaseElementProperties(properties);

		if(properties.faces !== undefined && properties.faces.length > 1) {
			if(properties.defaultAction === undefined)
				properties.defaultAction = ['flip'];
			if(properties.wheelUpAction === undefined)
				properties.wheelUpAction = ['flip'];
			if(properties.wheelDownAction === undefined)
				properties.wheelDownAction = ['flipDown'];
		}

		this.execute({ type: 'CreateElement', id: properties.id, layerId: properties.layer, properties: properties });

		return properties.id;
	}

	createStack(properties: CreateStackElementProperties): Id {
		this.convertBaseElementProperties(properties);

		if(properties.defaultAction === undefined)
			properties.defaultAction = ['flip'];

		this.execute({ type: 'CreateStack', id: properties.id, layerId: properties.layer, properties: properties });

		return properties.id;
	}

	deleteElement(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		// delete all children first recursively
		let children = element.children.slice();
		for(let i = 0; i < children.length; i++)
			this.deleteElement(children[i]);

		// implicit stacks get deleted automatically. account for that
		if(this.state.elementExists(id)) {
			// send element to the layer to trigger all pop actions
			this.pop(id);

			this.execute({ type: 'DeleteElement', id: id });
		}
	}

	cloneElement(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		this.cloneElementAt(id, element.parent, element.position);
	}

	cloneElementAt(id: Id, parent?: Id, position?: Position): Id {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		let newId = this.createElementId();
		this.execute({ type: 'CloneElementAt', id: id, newId: newId, parent: parent, position: position });

		let children = element.children;
		for(let i = 0; i < children.length; i++)
			this.cloneElementAt(children[i], newId, undefined);

		return newId;
	}

	setColor(color: number[]): void {
		this.execute({ type: 'SetColor', userId: this.userId, color: color });
	}

	renameMyself(name: string): void {
		this.execute({ type: 'RenameUser', userId: this.userId, name: name });
	}

	togglePin(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.pinned)
			this.execute({ type: 'Unpin', id: id });
		else
			this.execute({ type: 'Pin', id: id });
	}

	pin(id: Id): void {
		this.execute({ type: 'Pin', id: id });
	}

	unpin(id: Id): void {
		this.execute({ type: 'Unpin', id: id });
	}

	sendToTop(id: Id): void {
		this.execute({ type: 'SendToTop', id: id });
	}

	sendToBottom(id: Id): void {
		this.execute({ type: 'SendToBottom', id: id });
	}

	sendToLayer(id: Id, layerId: Id): void {
		this.pop(id);
		this.execute({ type: 'SendToLayer', id: id, layerId: layerId });
	}

	addToGroup(id: Id, groupId: Id): void {
		this.execute({ type: 'AddToGroup', id: id, groupId: groupId });
	}

	removeFromGroup(id: Id, groupId: Id): void {
		this.execute({ type: 'RemoveFromGroup', id: id, groupId: groupId });
	}

	makeStackable(id: Id): void {
		this.execute({ type: 'MakeStackable', id: id });
	}

	makeUnstackable(id: Id): void {
		this.execute({ type: 'MakeUnstackable', id: id });
	}

	flipRandomAnimated(...ids: Id[]): void {
		let times = 6;
		let delay = 50;
		for(let i = 0; i < times; i++) {
			let iterator = i;
			window.setTimeout(() => {
				for(let j = 0; j < ids.length; j++)
					this.flipRandom(ids[j]);
				if(iterator == times - 1)
					this.commit("Faces flipped randomly");
				this.commit();
				this.stateChanged.emit();
			}, i * delay);
		}
	}

	flipRandom(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(element.faces !== undefined && element.faces.length > 0)
				this.flipTo(Math.floor(Math.random() * element.faces.length), id);
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.flipRandom(children[i]);
		}
	}

	flip(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(element.faces !== undefined && element.face !== undefined && element.faces.length > 0)
				this.flipTo((element.face + 1) % element.faces.length, id);
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.flip(children[i]);
		}
	}

	flipDown(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(element.faces !== undefined && element.face !== undefined && element.faces.length > 0)
				this.flipTo((element.face + element.faces.length - 1) % element.faces.length, id);
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.flipDown(children[i]);
		}
	}

	flipTo(face: number, id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element')
			this.execute({ type: 'Flip', id: id, face: face });
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.flipTo(face, children[i]);
		}
	}

	peek(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(element.faces !== undefined && element.face !== undefined && element.faces.length > 0)
				this.execute({ type: 'Peek', id: id, userId: this.userId, face: (element.face + 1) % element.faces.length });
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.peek(children[i]);
		}
	}

	unpeek(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(element.face !== undefined)
				this.execute({ type: 'Peek', id: id, userId: this.userId, face: element.face });
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.unpeek(children[i]);
		}
	}

	togglePeek(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(this.amIPeeking(id)) {
				if(element.faces !== undefined && element.faces.length > 0)
					this.execute({ type: 'Peek', id: id, userId: this.userId, face: (element.peeks[this.userId] + 1) % element.faces.length });
			}
			else
				this.peek(id);
		}
		else {
			let children = element.children.slice();
			for(let i = 0; i < children.length; i++)
				this.togglePeek(children[i]);
		}
	}

	select(id: Id): void {
		this.execute({ type: 'Select', id: id, userId: this.userId });
	}

	deselect(id: Id): void {
		this.execute({ type: 'Deselect', id: id, userId: this.userId });
	}

	deselectAll(): void {
		for(let id of this.selection().slice())
			this.execute({ type: 'Deselect', id: id, userId: this.userId });
	}

	selectGroup(id: Id): void {
		this.execute({ type: 'SelectGroup', id: id, userId: this.userId });
	}

	rename(id: Id, name: string): void {
		this.execute({ type: 'Rename', id: id, name: name });
	}

	moveTo(x: number, y: number, rotation: number, id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined) {
			let parent = this.element(element.parent);
			if(!parent)
				throw new Error('element ' + element.parent + ' does not exist');

			if(parent.type === 'stack')
				this.pop(element.id);
		}

		this.execute({ type: 'MoveTo', id: id, x: x, y: y, rotation: rotation });
	}

	moveBy(id: Id, x: number, y: number, rotation: number): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined) {
			let parent = this.element(element.parent);
			if(!parent)
				throw new Error('element ' + element.parent + ' does not exist');

			if(parent.type === 'stack')
				this.pop(element.id);
		}

		if(element.position !== undefined)
			this.execute({ type: 'MoveTo', id: id, x: element.position.x + x, y: element.position.y + y, rotation: element.position.rotation + rotation });
	}

	rotateBy(rotation: number, id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined) {
			let parent = this.element(element.parent);
			if(!parent)
				throw new Error('element ' + element.parent + ' does not exist');

			if(parent.type === 'stack')
				this.pop(element.id);
		}

		if(element.position !== undefined)
			this.execute({ type: 'MoveTo', id: id, x: element.position.x, y: element.position.y, rotation: element.position.rotation + rotation });
	}

	snapToGrid(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(!element.grid || element.grid.length == 0)
			return;
		if(element.parent !== undefined && this.elementExists(element.parent) && (<Element>this.element(element.parent)).type === 'stack')
			return;
		if(element.position === undefined)
			return;

		// calculate the new position on the grid
		let pos = element.position;
		if(this.environment)
			pos = this.environment.get(element.grid[0]).apply(null, element.grid.slice(1).concat([{ x: pos.x, y: pos.y, rotation: pos.rotation }]));

		// assign the position to the element
		this.moveTo(pos.x, pos.y, pos.rotation, id);
	}

	moveRightOf(id: Id, leftId: Id): void {
		let left    = this.element(leftId);
		let element = this.element(id);
		if(left && element && left.position !== undefined && element.position !== undefined)
			this.moveTo(left.position.x + left.width, left.position.y, element.position.rotation, id);
	}

	bounds(id: Id): Bounds {
		let element = this.element(id);
		if(!element) {
			return {
				x:      0,
				y:      0,
				width:  0,
				height: 0,
				top:    0,
				left:   0,
				right:  0,
				bottom: 0,
			}
		}

		return {
			x:      element.position ? element.position.x : 0,
			y:      element.position ? element.position.y : 0,
			width:  element.width,
			height: element.height,
			top:    element.position ? (element.position.y - element.pivot[1]) : 0,
			left:   element.position ? (element.position.x - element.pivot[0]) : 0,
			bottom: element.position ? (element.position.y - element.pivot[1] + element.height) : 0,
			right:  element.position ? (element.position.x - element.pivot[0] + element.width ) : 0,
		};
	}

	reverseStack(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		let n = element.children.length;
		let order = iota(n);
		order.reverse();
		this.execute({ type: 'ReorderChildren', stackId: id, order: order });
	}

	shuffleStack(id: Id): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		let n = element.children.length;
		let order = iota(n);
		shuffle(order);
		this.execute({ type: 'ReorderChildren', stackId: id, order: order });
	}

	rotateStackOrder(id: Id, delta: number): void {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		let n = element.children.length;
		if(n == 0)
			return;

		let order = iota(n);
		while(delta > 0) {
			order.unshift(<number>order.pop());
			delta--;
		}
		while(delta < 0) {
			order.push(<number>order.shift());
			delta++;
		}
		this.execute({ type: 'ReorderChildren', stackId: id, order: order });
	}

	popAll(stackId: Id): Id[] {
		let stack = this.element(stackId);
		if(!stack)
			throw new Error('element ' + stackId + ' does not exist');

		let children = stack.children.slice();
		for(let i = 0; i < children.length; i++) {
			let id = children[i];
			this.pop(id);

			let element = this.element(id);
			if(element && element.position) {
				// cascade all popped elements
				this.moveTo(element.position.x + 4 * i, element.position.y + 4 * i, element.position.rotation, id);
			}
		}

		return children;
	}

	popNToRight(stackId: Id, count: number): Id[] {
		let stack = this.element(stackId);
		if(!stack)
			throw new Error('element ' + stackId + ' does not exist');

		let ids = stack.children.slice(-count); // shallow copy
		for(let i = ids.length - 1; i >= 0; i--)
			this.pop(ids[i]);

		let leftId = stackId;
		for(let i = 0; i < ids.length; i++) {
			let id = ids[i];
			this.moveRightOf(id, leftId);
			leftId = id;
		}

		return ids;
	}

	pop(childId: Id): void {
		let element = this.element(childId);
		if(!element || !element.parent)
			return;

		let parent = this.element(element.parent);
		if(!parent)
			return;

		this.execute({ type: 'Pop', childId: childId });

		if(parent.type == 'stack') {
			// execute the popAction
			if(parent.popAction)
				this.executeAction(parent.popAction, element.id);

			// delete implicitely created stacks, when only one child remains
			if(parent.deleteOnLast) {
				if(parent.children.length == 1)
					this.pop(parent.children[0]); // the recursive call deletes the stack
				else if(parent.children.length == 0)
					this.deleteElement(parent.id);
			}
		}
	}

	popFromStack(childId: Id): void {
		let element = this.element(childId);
		if(!element || !element.parent)
			return;

		let parent = this.element(element.parent);
		if(!parent || parent.type !== 'stack')
			return;

		this.pop(childId);
	}

	// appends given string to 'text' of all selected elements of type text
	appendText(text: string): void {
		let selection = this.selection();
		for(let i = 0; i < selection.length; i++) {
			let element = this.element(selection[i]);
			if(element && element.type == 'text')
				this.execute({ type: 'SetText', id: element.id, text: element.text + text });
		}
	}

	// removes the last character from 'text' of all seclted elements of type string
	removeChar(): void {
		let selection = this.selection();
		for(let i = 0; i < selection.length; i++) {
			let element = this.element(selection[i]);
			if(element && element.type == 'text') {
				let content = element.text;
				if(content !== undefined && content.length > 0)
					this.execute({ type: 'SetText', id: element.id, text: content.substr(0, content.length - 1) });
			}
		}
	}

	canPush(childId: Id, targetId: Id): boolean {
		let target = this.element(targetId);
		if(!target)
			return false;

		// allow only pushes to stacks and stackables
		if(target.type != 'stack' && !target.stackable)
			return false;

		// instead of pushing one stack onto another, merge them
		let child = this.element(childId);
		if(!child)
			return false;
		if(child.type == 'stack') {
			for(let i = 0; i < child.children.length; i++) {
				if(this.canPush(child.children[i], targetId))
					return true;
			}
			return false;
		}

		// make sure the stack accepts the element
		if(target.type === 'stack') {
			if(target.accepts !== undefined)
				return (child.groups.indexOf(target.accepts) !== -1);
		}
		else if(target.stackable) {
			if(typeof target.stackable === 'boolean')
				return target.stackable;
			else if(target.stackable.accepts !== undefined)
				return (child.groups.indexOf(target.stackable.accepts) !== -1);
		}

		return true;
	}

	push(childId: Id, targetId: Id): void {
		if(!this.canPush(childId, targetId))
			return;

		let target = this.element(targetId);
		if(!target)
			return;

		// allow only pushes to stacks and stackables
		if(target.type != 'stack' && !target.stackable)
			return;

		// instead of pushing one stack onto another, merge them
		let child = this.element(childId);
		if(!child)
			return;
		if(child.type == 'stack') {
			let elements = child.children.slice(); // make a shallow copy, as most (but maybe not all) children will be removed when pushing
			for(let i = 0; i < elements.length; i++)
				this.push(elements[i], targetId);
			return;
		}

		// make sure the current parent of the child gets notified and the child is a root element
		this.pop(childId);

		// if the target is a stackable, create a new implicit stack and push the target onto it
		if(target.type !== 'stack' && target.stackable) {
			// determine the implicit stack position
			let position = JSON.parse(JSON.stringify(target.position));

			// create the stack and push the target element onto it
			let stack = {
				position:     position, // make a deep copy to avoid changing stackable properties later on
				layer:        target.layer,
				width:        target.width,
				height:       target.height,
				grid:         target.grid,
				deleteOnLast: true
			};
			if(typeof target.stackable !== 'boolean') {
				for(let key in target.stackable)
					(<any>stack)[key] = JSON.parse(JSON.stringify((<any>target.stackable)[key])); // deep copy
			}

			let stackId = this.createStack(stack);
			this.push(targetId, stackId);

			// make the newly create stack our new target
			targetId = stackId;
			target   = this.element(targetId);
			if(!target)
				return;
		}

		this.execute({ type: 'Push', id: childId, parentId: targetId });

		// execute the pushAction
		if(target.type == 'stack' && target.pushAction)
			this.executeAction(target.pushAction, childId);
	}

	clearDrawings(): void {
		this.execute({ type: 'ClearDrawings' });
	}

	startFreehandDrawing(id: Id, x: number, y: number, color: number[]): void {
		this.execute({ type: 'StartFreehandDrawing', id: id, x: x, y: y, color: color });
	}

	continueFreehandDrawing(id: Id, x: number, y: number): void {
		this.execute({ type: 'ContinueFreehandDrawing', id: id, x: x, y: y });
	}

	setTurn(turn: number[]): void {
		let seq = this.state.turnSequence;
		for(let i = 0; i < turn.length && seq; i++) {
			if(seq.type === 'counter')
				seq = seq.child;
			else if(seq.type === 'options')
				seq = seq.options[turn[i]].child;
			else
				seq = undefined;
		}

		while(seq && seq.initial !== undefined) {
			let initial = seq.initial;
			turn.push(initial);

			if(seq.type === 'counter')
				seq = seq.child;
			else if(seq.type === 'options')
				seq = seq.options[initial].child;
			else
				break;
		}

		this.execute({ type: 'SetTurn', turn: turn });
	}

	setTurnSequence(turnSequence: TurnSequence): void {
		this.execute({ type: 'SetTurnSequence', turnSequence: turnSequence });

		let turn: number[] = [];
		function collectDefaultTurn(turnSequence?: TurnSequence) {
			if(!turnSequence)
				return;
			if(turnSequence.initial === undefined)
				return;
			let initial = turnSequence.initial;

			turn.push(turnSequence.initial);

			if(turnSequence.type === 'counter')
				collectDefaultTurn(turnSequence.child);
			else if(turnSequence.type === 'options')
				collectDefaultTurn(turnSequence.options[initial].child);
		}
		collectDefaultTurn(turnSequence);

		this.setTurn(turn);
	}

	selectedWriteableElements(): Element[] {
		let selection = this.selection();
		if(selection.length == 0)
			return [];

		let textElements = [];
		for(let i = 0; i < selection.length; i++) {
			let element = this.element(selection[i]);
			if(element && element.type == 'text' && element.writeable)
				textElements.push(element);
		}
		return textElements;
	}

	executeAction(action: Action, ...args: any[]): any {
		if(!this.environment)
			return undefined;
		let f = this.environment.get(action[0]);
		if(f === undefined)
			f = this.environment.get('Seneton.' + action[0]).bind(this);
		return f.apply(null, action.slice(1).concat(args));
	}
}
