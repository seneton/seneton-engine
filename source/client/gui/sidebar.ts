import * as $ from 'jquery';

import * as Colors from '../../shared/colors';

import { Client } from '../client';
import { Seneton } from '../seneton';

import { Mode, Table } from './table';


export class SideBar {
	client:  Client;
	seneton: Seneton;
	table:   Table;
	element: HTMLDivElement;

	constructor(client: Client, seneton: Seneton, table: Table) {
		this.client  = client;
		this.seneton = seneton;
		this.table   = table;

		this.element = document.createElement('div');	
		this.element.classList.add('sidebar');
		this.element.classList.add('collapsed');

		{
			let title = document.createElement('div');
			title.classList.add('sidebar-title');
			title.innerText = 'Seneton';
			this.element.appendChild(title);
		}

		let html = '';
		html += '<div class="sidebar-expander"></div>';
		html += '<div class="expanded-content">';
		html += '	<div id="sidebar-game-banner"></div>';
		html += '	<div class="sidebar-buttons">';
		html += '	<div class="button icon-drag" id="button-mode-drag"></div>';
		html += '	<div class="button icon-draw" id="button-mode-draw"></div>';
		html += '	<div class="button icon-erase" id="button-erase-drawings"></div>';
		html += '	<div class="button icon-save" id="button-save-game"></div>';
		html += '	<div class="button icon-open" id="button-load-game"></div>';
		html += '	</div>';
		html += '	<div class="notebook active-1">';
		html += '		<div class="tabs">';
		html += '			<div class="tab active" data-page="1">';
		html += '				<div class="icon icon-library"></div>';
		html += '			</div>';
		html += '			<div class="tab" data-page="2">';
		html += '				<div class="icon icon-user"></div>';
		html += '			</div>';
		html += '			<div class="tab" data-page="3">';
		html += '				<div class="icon icon-layers"></div>';
		html += '			</div>';
		html += '			<div class="tab" data-page="4">';
		html += '				<div class="icon icon-history"></div>';
		html += '			</div>';
		html += '		</div>';
		html += '		<div class="pages">';
		html += '			<div class="page page-1 active">';
		html += '				<div id="games-list"></div>';
		html += '			</div>';
		html += '			<div class="page page-2">';
		html += '				<div id="users-list"></div>';
		html += '				<hr />';
		html += '				Change Color:';
		html += '				<div id="colors-list"></div>';
		html += '				<hr />';
		html += '				<a href="#" id="button-rename">Rename</a>';
		html += '			</div>';
		html += '			<div class="page page-3" id="groups-list"></div>';
		html += '			<div class="page page-4" id="log-entries"></div>';
		html += '		</div>';
		html += '	</div>';
		html += '</div>';
		html += '<div class="collapsed-content">';
		html += '	<div class="button icon-drag" id="button-mode-drag2"></div>';
		html += '	<div class="button icon-draw" id="button-mode-draw2"></div>';
		html += '	<div class="button icon-erase" id="button-erase-drawings2"></div>';
		html += '	<div class="button icon-save" id="button-save-game2"></div>';
		html += '	<div class="button icon-open" id="button-load-game2"></div>';
		html += '</div>';
		this.element.innerHTML += html;

		$(this.element).find('.sidebar-expander').on('click', () => {
			this.element.classList.toggle('collapsed');
		});

		$(this.element).find('.notebook .tabs .tab').each((index, element) => {
			$(element).on('click', () => {
				$(this.element).find('.notebook .active').removeClass('active');
				$(element).addClass('active');
				$(this.element).find('.notebook .page-' + $(element).data('page')).addClass('active');
			});
		});

		$(this.element).find('#button-mode-drag, #button-mode-drag2').on('click', () => {
			this.table.toggleMode(Mode.DRAG);
		});
		$(this.element).find('#button-mode-draw, #button-mode-draw2').on('click', () => {
			this.table.toggleMode(Mode.DRAW);
		});
		this.table.modeChanged.on(() => {
			$(this.element).find('#button-mode-drag, #button-mode-drag2').toggleClass('active', this.table.mode === Mode.DRAG);
			$(this.element).find('#button-mode-draw, #button-mode-draw2').toggleClass('active', this.table.mode === Mode.DRAW);
		});
		$(this.element).find('#button-erase-drawings').on('click', () => {
			this.seneton.clearDrawings();
			this.seneton.commit();
			this.table.requestRender();
		});
		$(this.element).find('#button-save-game, #button-save-game2').on('click', () => {
			let d = new Date();
			let name = 'Savegame ' + (this.seneton.game ? this.seneton.game.name : '') + ' ' +
						 d.getFullYear()              + '-' +
				('00' + (d.getMonth() + 1)).slice(-2) + '-' +
				('00' +  d.getDate()      ).slice(-2) + ' ' +
				('00' +  d.getHours()     ).slice(-2) + '-' +
				('00' +  d.getMinutes()   ).slice(-2) + '-' +
				('00' +  d.getSeconds()   ).slice(-2);

			let json = JSON.stringify(this.seneton.saveGame());
			let blob = new Blob([json], { type: 'application/json' });

			let a = document.createElement('a');
			a.download = name + '.json';
			a.href     = URL.createObjectURL(blob);
			a.click();
		});
		$(this.element).find('#button-load-game, #button-load-game2').on('click', () => {
			let input = document.createElement('input');
			input.setAttribute('type', 'file');
			input.onchange = () => {
				let reader = new FileReader();
				reader.addEventListener("loadend", () => {
					this.seneton.loadGame(JSON.parse(reader.result));
					this.seneton.commit('Game loaded');
				});
				if(input.files)
					reader.readAsText(input.files[0]);
			};
			input.click();
		});
		$(this.element).find('#button-rename').on('click', () => {
			let me   = this.seneton.me();
			let name = prompt('New Name?', me ? me.name : '');
			if(name !== null) {
				this.seneton.renameMyself(name);
				this.seneton.commit('Rename to ' + name);
				this.table.requestRender();
			}
		});

		let updateThumbnail = () => {
			let thumbnail = this.seneton.thumbnailUrl();
			if(thumbnail)
				$(this.element).find('#sidebar-game-banner').css('background-image', 'url(' + thumbnail + ')');
			else
				$(this.element).find('#sidebar-game-banner').css('background-image', 'none');
		};
		this.seneton.gameChanged.on(updateThumbnail);
		this.client.serverGamesChanged.on(updateThumbnail);

		let list = $(this.element).find('#colors-list');
		for(let color of Colors.PlayerColors) {
			list.append($('<div>').css({
				display:         'inline-block',
				width:           '30px',
				height:          '15px',
				backgroundColor: 'rgba(' + Math.round(255 * color[0]) + ',' + Math.round(255 * color[1]) + ',' + Math.round(255 * color[2]) + ',' + color[3] + ')',
				borderRadius:    '4px',
				marginRight:     '4px',
				cursor:          'pointer'
			}).click(() => {
				this.seneton.setColor(color);
				this.seneton.commit('Change color');
				this.table.requestRender();
			}));
		}

		this.client.historyEntryAdded.on(() => {
			let id = this.client.history.length - 1;

			let div = document.createElement('div');
			let hours = '' + this.client.history[id].date.getHours();
			if(hours.length == 1)
				hours = '0' + hours;
			let minutes = '' + this.client.history[id].date.getMinutes();
			if(minutes.length == 1)
				minutes = '0' + minutes;

			let html = '';
			html += '<span class="time">' + hours + ':' + minutes + '</span>';
			let user = this.client.history[id].user;
			if(user !== undefined) {
				let c = user.color;
				html += '<div class="color" style="background:rgb(' + Math.round(255 * c[0]) + ', ' + Math.round(255 * c[1]) + ', ' + Math.round(255 * c[2]) + ');"></div> ';
			}
			html += this.client.history[id].description;
			div.onmouseenter = () => {
				this.table.renderState = this.client.history[id].state;
				this.table.requestRender();
			};
			div.innerHTML = html;

			div.onclick = () => {
				this.seneton.loadGame(this.client.history[id].state);
				this.seneton.commit("Undo");
			};

			let logEntries = $(this.element).find('#log-entries')[0];
			let pages  = logEntries.parentElement;
			let bottom = pages ? (pages.scrollTop + pages.clientHeight >= pages.scrollHeight) : 0;

			let depth = 0;
			let group = logEntries;
			let seq   = this.client.history[id].state.turnSequence;
			while(depth < this.client.history[id].state.turn.length && group.lastElementChild && group.lastElementChild.getAttribute('class') == 'group' && +(group.lastElementChild.getAttribute('data-turn') || 0) == this.client.history[id].state.turn[depth]) {
				group = <HTMLElement>group.lastElementChild;
				if(seq.type === 'counter')
					seq = seq.child;
				else if(seq.type === 'options')
					seq = seq.options[this.client.history[id].state.turn[depth]].child;
				depth++;
			}

			$(group).find('.group-title').addClass('collapsed');

			while(depth < this.client.history[id].state.turn.length) {
				let title = document.createElement('div');
				title.setAttribute('class', 'group-title');
				if(seq.type === 'counter')
					title.innerHTML = '<span class="group-expander"></span> ' + seq.name.replace('%n', '' + this.client.history[id].state.turn[depth]);
				else if(seq.type === 'options')
					title.innerHTML = '<span class="group-expander"></span> ' + seq.options[this.client.history[id].state.turn[depth]].name;
				group.appendChild(title);

				let childGroup = document.createElement('div');
				childGroup.setAttribute('class', 'group');
				childGroup.setAttribute('data-turn', '' + this.client.history[id].state.turn[depth]);
				group.appendChild(childGroup);

				$(title).click(() => {
					$(title).toggleClass('collapsed');
				});

				group = childGroup;
				if(seq.type === 'counter')
					seq = seq.child;
				else if(seq.type === 'options')
					seq = seq.options[this.client.history[id].state.turn[depth]].child;
				depth++;
			}

			group.appendChild(div);

			if(bottom && pages)
				pages.scrollTop = pages.scrollHeight;
		});
		$(this.element).find('#log-entries')[0].onmouseleave = () => {
			this.table.renderState = undefined;
			this.table.requestRender();
		};

		this.client.serverGamesChanged.on(() => { this.refreshGameList(); });
		this.refreshGameList();

		this.seneton.groupsChanged.on(() => { this.refreshGroupList(); });
		this.refreshGroupList();

		this.seneton.usersChanged.on(() => { this.refreshUserList(); });
		this.refreshUserList();
	}

	refreshGameList() {
		let list = $(this.element).find('#games-list');
		list.empty();

		for(let game of this.client.serverGames) {
			let thumbnail = (game.thumbnail ? 'url(' + game.url + game.thumbnail + ')' : 'none');

			list.append(
				$('<div>')
				.addClass('game')
				.append(
					$('<div>').addClass('banner').css('background-image', thumbnail)
				)
				.append(
					$('<div>').addClass('title').text(game.name)
				)
				.on('click', () => { this.client.initializeGame(game.uid); })
			);
		}
	}

	refreshGroupList() {
		let list = $(this.element).find('#groups-list');
		list.empty();

		for(let id of this.seneton.state.groupIds) {
			let group = this.seneton.group(id);
			if(group) {
				list.append(
					$('<div>')
					.addClass('group')
					.text(
						group.name + ' (' + group.elements.length + ')'
					).on('click', (event) => {
						if(!event.ctrlKey)
							this.seneton.deselectAll();
						this.seneton.selectGroup(id);
						this.seneton.commit();
						this.table.requestRender();
					})
				);
			}
		}
	}
	
	refreshUserList() {
		let list = $(this.element).find('#users-list');
		list.empty();

		for(let id of this.seneton.state.userIds) {
			let user = this.seneton.user(id);
			if(user) {
				list.append($('<div>').addClass('user').append(
					$('<div>').css({
						display:      'inline-block',
						width:        '30px',
						height:       '15px',
						background:   'rgba(' + Math.round(255 * user.color[0]) + ',' + Math.round(255 * user.color[1]) + ',' + Math.round(255 * user.color[2]) + ', ' + user.color[3] + ')',
						marginRight:  '10px',
						borderRadius: '4px'
					})
				).append(
					$('<span>').text(user.name)
				));
			}
		}
	}
}
