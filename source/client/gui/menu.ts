import * as $ from 'jquery';


interface BaseEntry {
	highlight: boolean;
	disabled: boolean;
}


interface ActionEntry extends BaseEntry {
	type:     'action'
	name:     string;
	callback: () => void;
}


interface SeperatorEntry extends BaseEntry {
	type: 'seperator'
}


interface SubmenuEntry extends BaseEntry {
	type:     'submenu'
	name:     string;
	children: Entry[];
}


interface HtmlEntry extends BaseEntry {
	type: 'html'
	html: string;
}


type Entry = ActionEntry | SeperatorEntry | SubmenuEntry | HtmlEntry;


interface EntryOptions {
	highlight?: boolean;
	disabled?:  boolean;
}


interface EntryElement extends HTMLElement {
	entry?: Entry;
	level?: Number;
	menu?: Menu;
}


export class Menu {
	entries:  Entry[]        = [];
	oncloses: (() => void)[] = [];
	overlay?: HTMLElement    = undefined;
	menus:    HTMLElement[]  = [];

	onclose(callback: () => void) {
		this.oncloses.push(callback);
	}

	_action(array: Entry[], name: string, callback: () => void, options?: EntryOptions) {
		options = options || {};
		array.push({
			type:      'action',
			name:      name,
			callback:  callback,
			highlight: (options.highlight === true),
			disabled:  (options.disabled  === true)
		});
	}

	_sep(array: Entry[], options?: EntryOptions) {
		options = options || {};
		array.push({
			type:      'seperator',
			highlight: (options.highlight === true),
			disabled:  (options.disabled  === true)
		});
	}

	_submenu(array: Entry[], name: string, options?: EntryOptions) {
		options = options || {};
		let entry: SubmenuEntry = {
			type:      'submenu',
			name:      name,
			children:  [],
			highlight: (options.highlight === true),
			disabled:  (options.disabled  === true)
		};
		array.push(entry);
		return {
			action: (name: string, callback: () => void, options?: EntryOptions) => {
				return this._action(entry.children, name, callback, options);
			},
			sep: (options?: EntryOptions) => {
				return this._sep(entry.children, options);
			},
			submenu: (name: string, options?: EntryOptions) => {
				return this._submenu(entry.children, name, options);
			},
			html: (html: string, options?: EntryOptions) => {
				return this._html(entry.children, html, options);
			}
		};
	};

	_html(array: Entry[], html: string, options?: EntryOptions) {
		options = options || {};
		array.push({
			type:      'html',
			html:      html,
			highlight: (options.highlight === true),
			disabled:  (options.disabled  === true)
		});
	}

	action(name: string, callback: () => void, options?: EntryOptions) {
		return this._action(this.entries, name, callback, options);
	}

	sep(options?: EntryOptions) {
		return this._sep(this.entries, options);
	}

	submenu(name: string, options?: EntryOptions) {
		return this._submenu(this.entries, name, options);
	}

	html(html: string, options?: EntryOptions) {
		return this._html(this.entries, html, options);
	}

	show(x: number, y: number) {
		this.overlay = document.createElement('div');
		this.overlay.style['overflow'] = 'hidden';
		this.overlay.style['position'] = 'fixed';
		this.overlay.style['zIndex']   = '1000000';
		this.overlay.style['top']      = '0px';
		this.overlay.style['left']     = '0px';
		this.overlay.style['width']    = '100%';
		this.overlay.style['height']   = '100%';
		document.body.appendChild(this.overlay);

		let menu = document.createElement('div');
		this.menus.push(menu);
		menu.classList.add('menu');
		menu.style['position'] = 'absolute';
		menu.style['top']      = y + 'px';
		menu.style['left']     = x + 'px';
		this.addDOMEntries(this.entries, menu, 0);
		this.overlay.appendChild(menu);

		if($(menu).offset().left + $(menu).width() > $(this.overlay).width())
			menu.style['left'] = ($(this.overlay).width() - $(menu).width()) + 'px';
		if($(menu).offset().top + $(menu).outerHeight() > $(this.overlay).height()) {
			menu.style['top']  = (y - $(menu).outerHeight()) + 'px';
			menu.classList.add('upwards');
		}
		else
			menu.classList.add('downwards');

		let closeOnUp = false;
		this.overlay.addEventListener('mouseup', event => {
			if(closeOnUp)
				this.close();
			event.stopPropagation();
		}, false);
		this.overlay.addEventListener('mousedown', event => {
			closeOnUp = true;
			event.stopPropagation();
		}, false);

		function onMouseMove(event: MouseEvent) {
			if(event.button == 2) // moving the cursor while holding the right mouse button
				closeOnUp = true;
			document.removeEventListener('mousemove', onMouseMove, true);
		}
		document.addEventListener('mousemove', onMouseMove, true);
	}

	addDOMEntries(entries: Entry[], menu: HTMLElement, level: number) {
		for(let i = 0; i < entries.length; i++) {
			let e = entries[i];

			let entry = <EntryElement>document.createElement('div');
			entry.entry = e;
			entry.menu  = this;
			entry.level = level;
			entry.classList.add('entry');
			if(e.highlight)
				entry.classList.add('highlight');
			if(e.disabled)
				entry.classList.add('disabled');

			if(e.type == 'action') {
				let actionEntry = e;
				entry.classList.add('action');
				entry.innerHTML = e.name;
				entry.onmouseup = event => {
					if(!e.disabled) {
						if(actionEntry.callback !== undefined)
							actionEntry.callback();
						this.close();
					}
					event.stopPropagation();
				};
				entry.onmouseenter = event => {
					event.stopPropagation();
					this.closeLevel(level + 1);
				};
			}
			else if(e.type == 'seperator') {
				entry.classList.add('seperator');
				entry.onmouseup = event => {
					event.stopPropagation();
				};
				entry.onmouseenter = event => {
					event.stopPropagation();
					this.closeLevel(level + 1);
				};
			}
			else if(e.type == 'submenu') {
				let submenuEntry = e;
				entry.classList.add('submenu');
				entry.innerHTML = e.name;
				entry.onmouseup = event => {
					event.stopPropagation();
				};
				entry.onmouseenter = event => {
					event.stopPropagation();
					this.closeLevel(level + 1);
					entry.classList.add('active');
					if(!submenuEntry.disabled) {
						let submenu = document.createElement('div');
						this.menus.push(submenu);
						submenu.classList.add('menu');
						submenu.style['position'] = 'absolute';
						submenu.style['top']      = $(entry).offset().top + 'px';
						submenu.style['left']     = ($(entry).offset().left + $(entry).outerWidth()) + 'px';
						this.addDOMEntries(submenuEntry.children, submenu, level + 1);
						this.overlay.appendChild(submenu);

						if($(submenu).offset().left + $(submenu).width() > $(this.overlay).width())
							submenu.style['left'] = ($(entry).offset().left - $(submenu).width()) + 'px';
						if($(submenu).offset().top + $(submenu).outerHeight() > $(this.overlay).height())
							submenu.style['top']  = ($(this.overlay).height() - $(submenu).outerHeight()) + 'px';
					}
				};
			}
			else if(e.type == 'html') {
				entry.classList.add('html');
				entry.innerHTML = e.html;
				entry.onmouseup = event => {
					event.stopPropagation();
				};
				entry.onmouseenter = event => {
					event.stopPropagation();
					this.closeLevel(level + 1);
				};
			}

			menu.appendChild(entry);
		}
	}

	close() {
		if(this.overlay)
			document.body.removeChild(this.overlay);
		for(let i = 0; i < this.oncloses.length; i++)
			this.oncloses[i]();
	}

	closeLevel(level: number) {
		while(this.menus.length > level) {
			let e = this.menus.pop();
			if(e && e.parentNode)
				e.parentNode.removeChild(e);
		}
		$(this.menus[this.menus.length - 1]).find('.active').removeClass('active');
	}
}
