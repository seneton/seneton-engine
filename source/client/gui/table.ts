import * as $ from 'jquery';

import { DrawingType, Element, Id, Position, RegularElement, StackElement, State, TextElement } from '../../shared/state';

import { Seneton } from '../seneton';

import { Menu } from './menu';

import { Event } from '../util/events';
import * as webgl from '../util/webgl';
import { radian, vec2, vec2e } from '../util/geometry';


interface Bookmark {
	position: vec2;
	scale:    number;
	angle:    number;
}


export enum Mode {
	NORMAL,
	DRAG,
	DRAW
}


export class Table {
	seneton:       Seneton;

	canvas:         HTMLCanvasElement;
	overlay:        HTMLCanvasElement;
	ctx:            webgl.Context<Id>;
	octx:           CanvasRenderingContext2D;
	mode:           Mode;
	toggledMode:    Mode;
	temporaryMode?: Mode;
	position:       vec2;
	angle:          number = 0.0;
	scale:          number = 1.0;
	scaleTarget:    number = 1.0;
	lastRender:     number = 0;
	dragging:       boolean = false;
	dropTarget?:    Id = undefined;
	tempSelection:  {[index: number]: boolean} = {};
	bookmarks:      {[index: number]: Bookmark} = {};
	renderState?:   State;
	imageCache:     Map<any[], HTMLCanvasElement | undefined> = new Map();

	private initialSelection: Id[] = [];

	// Events
	public modeChanged = new Event;

	constructor(seneton: Seneton) {
		this.seneton = seneton;

		this.canvas = document.createElement('canvas');
		this.canvas.classList.add('table');
		this.ctx        = new webgl.Context(<WebGLRenderingContext>this.canvas.getContext('webgl'));
		this.ctx.onload = () => { this.requestRender(); };

		this.overlay = document.createElement('canvas');
		this.overlay.classList.add('overlay');
		this.octx = <CanvasRenderingContext2D>this.overlay.getContext('2d');

		this.mode = Mode.NORMAL;
		this.modeChanged.emit();

		this.position = vec2.fromValues(0.0, 0.0);

		this.seneton.gameChanged.on(() => {
			this.reset();
		});

		this.seneton.stateChanged.on(() => {
			this.requestRender();
		});

		this.seneton.environmentChanged.on(() => {
			this.requestRender();
		});

		this.overlay.addEventListener('mousedown', event => { this.onMouseDown  (event) }, false);
		this.overlay.addEventListener('wheel'    , event => { this.onMouseWheel (event) }, false);
		this.overlay.addEventListener('dblclick' , event => { this.onDoubleClick(event) }, false);
	}

	toggleMode(m: Mode) {
		if(this.toggledMode == m)
			this.toggledMode = Mode.NORMAL;
		else
			this.toggledMode = m;
		this.mode = (this.temporaryMode !== undefined ? this.temporaryMode : this.toggledMode);
		this.modeChanged.emit();
	}

	enterTemporaryMode(m: Mode) {
		this.temporaryMode = m;
		this.mode = (this.temporaryMode !== undefined ? this.temporaryMode : this.toggledMode);
		this.modeChanged.emit();
	}

	leaveTemporaryMode(m: Mode) {
		if(this.toggledMode == m)
			this.toggledMode = Mode.NORMAL;
		this.temporaryMode = undefined;
		this.mode = (this.temporaryMode !== undefined ? this.temporaryMode : this.toggledMode);
		this.modeChanged.emit();
	}

	public onKeyPress(event: KeyboardEvent): void {
		// append characters to selected text element if exactly one is selected
		let textElems = this.seneton.selectedWriteableElements();
		if(textElems.length > 0) {
			this.seneton.appendText(String.fromCharCode(event.which));
			this.seneton.commit();
			this.requestRender();
		}
	}

	public onKeyDown(event: KeyboardEvent): void {
		let textElems = this.seneton.selectedWriteableElements();

		switch(event.which || event.keyCode) {
			case 8: // [Backspace]
				if(this.seneton.selectedWriteableElements().length > 0) {
					// forward it to selected text fields
					event.preventDefault();
					this.seneton.removeChar();
					this.seneton.commit();
					this.requestRender();
				}
				break;

			case 9: // [Tab]
				if(this.seneton.selectedWriteableElements().length > 0) {
					// select next/previous element
					let id2select = (event.shiftKey ? textElems[0].id - 1 : textElems[0].id + 1);
					if(this.seneton.elementExists(id2select) && (<Element>this.seneton.element(id2select)).type == 'text') {
						event.preventDefault();
						this.seneton.select(id2select);
						this.seneton.deselect(textElems[0].id);
						this.seneton.commit();
						this.requestRender();
					}
				}
				break;

			case 36:  // [Home]
			case 192: // [^]
				if(this.seneton.selectedWriteableElements().length == 0) {
					event.preventDefault();
					this.gotoHome();
				}
				break;

			case 32: // [Space]
				if(this.seneton.selectedWriteableElements().length == 0) {
					event.preventDefault();
					this.enterTemporaryMode(Mode.DRAG);
				}
				break;

			case 68: // [D]
				if(this.seneton.selectedWriteableElements().length == 0) {
					if(event.ctrlKey) {
						event.preventDefault();
						this.seneton.clearDrawings();
						this.seneton.commit();
						this.requestRender();
					}
					else {
						event.preventDefault();
						this.enterTemporaryMode(Mode.DRAW);
					}
				}
				break;

			default:
				if(event.which >= 48 && event.which <= 57) { // [1] - [0]
					if(this.seneton.selectedWriteableElements().length == 0) {
						event.preventDefault();
						let index = event.which - 48;
						if(event.ctrlKey)
							this.saveBookmark(index);
						else
							this.gotoBookmark(index);
					}
				}
				break;
		}
	}

	public onKeyUp(event: KeyboardEvent): void {
		switch(event.which || event.keyCode) {
			case 32: // [Space]
				this.leaveTemporaryMode(Mode.DRAG);
				event.preventDefault();
				break;

			case 68: // [D]
				this.leaveTemporaryMode(Mode.DRAW);
				event.preventDefault();
				break;
		}
	}

	private onMouseDown(event: MouseEvent): void {
		if(this.mode == Mode.DRAG) {
			if(event.button == 0)
				this.dragTable(event);
			else if(event.button == 2)
				this.rotateTable(event);
		}
		else if(this.mode == Mode.DRAW)
			this.startDrawing(event);
		else if(event.shiftKey) {
			let cursor = vec2.fromValues(event.pageX, event.pageY);
			let id = this.pick(cursor);
			if(id) {
				let stack = this.seneton.skipStackChildren(id);
				if(stack && stack.type == 'stack') {
					this.seneton.deselect(stack.id);
					if(event.button == 0 && stack.children.length > 0) {
						let child = this.seneton.element(stack.children[stack.children.length - 1]);
						if(child) {
							let c = child; // narrow to <Element>
							for(let id of this.seneton.selection().slice()) {
								let element = this.seneton.element(id);
								if(element && element.parent === undefined)
									this.seneton.moveBy(element.id, c.width, 0, 0);
							}

							this.seneton.pop(child.id);
							this.seneton.moveBy(child.id, stack.width, 0, 0);
							this.seneton.select(child.id);

							if(event.ctrlKey)
								this.seneton.peek(child.id);
						}
					}
					else if(event.button == 2 && this.seneton.selection().length > 0) {
						let child = this.seneton.element(this.seneton.selection()[this.seneton.selection().length - 1]);
						if(child) {
							this.seneton.push(child.id, stack.id);
							this.seneton.deselect(child.id);
						}
					}

					this.seneton.commit('Element popped');
					this.requestRender();
				}

				event.preventDefault();
			}
			return;
		}
		else {
			if(event.button == 0) {
				if(event.detail == 1)
					this.initialSelection = this.seneton.selection().slice(); // save the current selection in case this is a double click
				this.selectElements(event);
			}
			else if(event.button == 2) {
				if(event.ctrlKey)
					this.rotateElements(event);
				else {
					let cursor = vec2.fromValues(event.pageX, event.pageY);
					let id     = this.pick(cursor);
					if(id !== undefined) {
						let element = this.seneton.element(id);
						if(element)
							this.openContextMenu(event.pageX, event.pageY, element);
					}
				}
			}
		}
	}

	private onDoubleClick(event: MouseEvent): void {
		if(event.shiftKey)
			return;

		// prevent other double-click events, e.g. on the underlying stack
		event.stopPropagation();

		let cursor = vec2.fromValues(event.pageX, event.pageY);
		let id     = this.pick(cursor);

		if(id !== undefined) {
			// the first click of this double click has probably destroyed the selection. restore it
			this.seneton.deselectAll();
			for(let id of this.initialSelection)
				this.seneton.select(id);

			let element = this.seneton.element(id);
			if(event.ctrlKey) {
				this.seneton.togglePeek(id);
				this.seneton.commit('Peek');
			}
			else if(element && element.defaultAction) {
				this.seneton.executeAction(element.defaultAction, id);
				this.seneton.commit('Default Action');
			}
			this.requestRender();
		}
	}

	private onMouseWheel(event: WheelEvent) {
		if(this.mode == Mode.DRAG)
			this.zoom(vec2.fromValues(event.pageX, event.pageY), (event.deltaY < 0) ? 1.1 : 1.0 / 1.1);
		else {
			// prevent other double-click events, e.g. on the underlying stack
			event.stopPropagation();

			let cursor = vec2.fromValues(event.pageX, event.pageY);
			let id     = this.pick(cursor);

			if(id !== undefined) {
				let element = this.seneton.element(id);
				let root    = this.seneton.skipStackChildren(id);
				if(root && root.type === 'stack') {
					this.seneton.rotateStackOrder(root.id, (event.deltaY < 0 ? -1 : 1));
					this.seneton.commit();
					this.requestRender();
				}
				else if(element && event.deltaY > 0 && element.wheelDownAction !== undefined) {
					this.seneton.executeAction(element.wheelDownAction, id);
					this.seneton.commit();
					this.requestRender();
				}
				else if(element && event.deltaY < 0 && element.wheelUpAction !== undefined) {
					this.seneton.executeAction(element.wheelUpAction, id);
					this.seneton.commit();
					this.requestRender();
				}
			}
		}
	}

	openContextMenu(x: number, y: number, element: Element) {
		let initialSelection = !this.seneton.isSelectedByMe(element.id);

		if(initialSelection) {
			this.seneton.deselectAll();
			this.seneton.select(element.id);
			this.requestRender();
		}

		let menu = new Menu;
		menu.onclose(() => {
			if(initialSelection) {
				this.seneton.deselectAll();
				this.requestRender();
			}
		});

		menu.action('Send To Top', () => {
			for(let id of this.seneton.selection().slice())
				this.seneton.sendToTop(id);
			this.seneton.commit('Sent to top');
			this.requestRender();
		});
		menu.action('Send To Bottom', () => {
			for(let id of this.seneton.selection().slice())
				this.seneton.sendToBottom(id);
			this.seneton.commit('Sent to bottom');
			this.requestRender();
		});
		{
			let submenu = menu.submenu('Flip');
			submenu.action('Toggle Flip', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.flip(id);
				this.seneton.commit('Flipped');
				this.requestRender();
			});
			submenu.action('Frontface', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.flipTo(0, id);
				this.seneton.commit('Flipped');
				this.requestRender();
			});
			submenu.action('Backface', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.flipTo(1, id);
				this.seneton.commit('Flipped');
				this.requestRender();
			});
		}
		{
			let submenu = menu.submenu('Peek');
			submenu.action('Toggle Peek', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.togglePeek(id);
				this.seneton.commit('Peeked');
				this.requestRender();
			});
			submenu.action('Peek', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.peek(id);
				this.seneton.commit('Peeked');
				this.requestRender();
			});
			submenu.action('Unpeek', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.unpeek(id);
				this.seneton.commit('Peeked');
				this.requestRender();
			});
		}
		{
			let submenu = menu.submenu('Pin');
			submenu.action('Toggle Pin', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.togglePin(id);
				this.seneton.commit('Pinned');
				this.requestRender();
			});
			submenu.action('Pin', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.pin(id);
				this.seneton.commit('Pinned');
				this.requestRender();
			});
			submenu.action('Unpin', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.unpin(id);
				this.seneton.commit('Unpinned');
				this.requestRender();
			});
			if(element.type == 'stack') {
				menu.action('Pop All Children', () => {
					for(let id of this.seneton.selection().slice())
						this.seneton.popAll(id);
					this.seneton.commit('Popped all children');
					this.requestRender();
				});
				menu.action('Shuffle Children', () => {
					let times = 10;
					let ids   = this.seneton.selection().slice();
					for(let i = 0; i < times; i++) {
						let iterator = i;
						window.setTimeout(() => {
							for(let j = 0; j < ids.length; j++)
								this.seneton.shuffleStack(ids[j]);

							// only send the last shuffle
							if(iterator == times - 1)
								this.seneton.commit('Shuffled');
							this.requestRender();
						}, i * 50);
					}
				});
				menu.action('Reverse Children', () => {
					for(let id of this.seneton.selection().slice())
						this.seneton.reverseStack(id);
					this.seneton.commit('Stack reversed');
					this.requestRender();
				});
				menu.action('Rename Stack', () => {
					let name = prompt('New Name?', element.title);
					if(name !== null) {
						let n = name; // narrow to <string>
						for(let id of this.seneton.selection().slice())
							this.seneton.rename(id, n);
						this.seneton.commit('Renamed to ' + name);
						this.requestRender();
					}
				});
			}
		}
		{
			let submenu = menu.submenu('Advanced');
			submenu.action('Make stackable', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.makeStackable(id);
				this.seneton.commit('Made stackable');
				this.requestRender();
			});
			submenu.action('Make unstackable', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.makeUnstackable(id);
				this.seneton.commit('Made unstackable');
				this.requestRender();
			});
			submenu.action('Clone', () => {
				for(let id of this.seneton.selection().slice())
					this.seneton.cloneElement(id);
				this.seneton.commit('Cloned');
				this.requestRender();
			});
			submenu.action('Select all children', () => {
				for(let id of this.seneton.selection().slice()) {
					let element = this.seneton.element(id);
					if(element) {
						for(let i = 0; i < element.children.length; i++)
							this.seneton.select(element.children[i]);
					}
				}
				this.requestRender();
			});
			submenu.action('Dump Positions to console', () => {
				let s = '';
				for(let id of this.seneton.selection().slice()) {
					let e = this.seneton.element(id);
					if(!e || e.position === undefined)
						continue;
					if(s)
						s += ', ';
					s += '{ x: ' + e.position.x + ', y: ' + e.position.y;
					if(e.position.rotation != 0.0)
						s += ', rotation: ' + radian.grad(e.position.rotation);
					s += ' }';
				}
				console.log(s);
			});
			submenu.action('Dump to console', () => {
				for(let id of this.seneton.selection().slice())
					console.log(this.seneton.element(id));
			});
		}
		menu.sep();
		menu.action('Delete Element', () => {
			// iterate cautiously as deleting an element deletes all of its children
			while(this.seneton.selection().length > 0)
				this.seneton.deleteElement(this.seneton.selection()[0]);
			this.seneton.commit('Deleted');
			this.requestRender();
		});

		menu.show(x + 5, y);
	}

	view2table(pos: vec2): vec2 {
		let center = vec2.fromValues(this.canvas.width / 2, this.canvas.height / 2);
		let p = vec2.subtract(vec2.create(), pos, center);
		vec2.scale(p, p, 1.0 / this.scale);
		vec2e.rotate(p, p, -this.angle);
		vec2.add(p, p, this.position);
		return p;
	}

	pick(pos: vec2): number | undefined {
		return this.ctx.pick(pos);
	}

	reset(): void {
		this.position    = vec2.fromValues(0.0, 0.0);
		this.angle       = 0.0;
		this.scale       = 1.0;
		this.scaleTarget = 1.0;

		this.dragging      = false;
		this.dropTarget    = undefined;
		this.tempSelection = {};

		this.imageCache = new Map();

		this.requestRender();
	}

	resize(width: number, height: number): void {
		this.canvas.setAttribute('width',  '' + window.innerWidth );
		this.canvas.setAttribute('height', '' + window.innerHeight);
		this.overlay.setAttribute('width',  '' + window.innerWidth );
		this.overlay.setAttribute('height', '' + window.innerHeight);
		this.ctx.resize(width, height);
		this.requestRender();
	}

	saveBookmark(bookmark: number): void {
		this.bookmarks[bookmark] = {
			position: vec2.clone(this.position),
			scale:    this.scale,
			angle:    this.angle
		};
	}

	gotoView(position: vec2, scale: number, angle: number): void {
		this.scaleTarget = scale;
		let oldPos = vec2.clone(this.position), oldScale = this.scale, oldAngle = this.angle;

		let i      = 1;
		let frames = 10;
		let animate = () => {
			let t = i / frames;

			vec2.lerp(this.position, oldPos, position, t);
			this.scale = (1 - t) * oldScale + t * scale;
			this.angle = radian.lerp(oldAngle, angle, t);
			this.render();

			if(i < frames) {
				i++;
				requestAnimationFrame(animate);
			}
		};

		requestAnimationFrame(animate);
	}

	gotoBookmark(bookmark: number): void {
		if(this.bookmarks[bookmark] !== undefined)
			this.gotoView(this.bookmarks[bookmark].position, this.bookmarks[bookmark].scale, this.bookmarks[bookmark].angle);
	}

	gotoHome(): void {
		// TODO at some point this should be [0,0]
		this.gotoView(vec2.fromValues(0.0, 0.0), 1.0, 0.0);
	}

	zoom(position: vec2, zoom: number): void {
		zoom *= this.scaleTarget / this.scale;

		let tablePos = this.view2table(position);

		let delta = vec2.subtract(vec2.create(), tablePos, this.position);
		vec2.scale(delta, delta, 1.0 / zoom);
		let target = vec2.subtract(vec2.create(), tablePos, delta);

		this.position    = target;
		this.scale      *= zoom;
		this.scaleTarget = this.scale;
		this.render();
	}

	dragTable(event: MouseEvent): void {
		let start  = vec2.fromValues(event.pageX, event.pageY);
		let oldPos = vec2.clone(this.position);

		let move = (event: MouseEvent) => {
			let cursor = vec2.fromValues(event.pageX, event.pageY);
			let delta  = vec2.subtract(vec2.create(), cursor, start);
			vec2e.rotate(delta, delta, -this.angle);
			vec2.scale(delta, delta, 1.0 / this.scale);
			vec2.subtract(this.position, oldPos, delta);
			this.requestRender();
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	rotateTable(event: MouseEvent): void {
		let center = vec2.fromValues(this.canvas.width / 2, this.canvas.height / 2);
		let cursor = vec2.fromValues(event.pageX, event.pageY);
		let delta  = this.angle - vec2e.angle(vec2.subtract(vec2.create(), cursor, center));

		let dot = $('<div>').css({
			position:     'absolute',
			left:         center[0] - 3,
			top:          center[1] - 3,
			width:        6,
			height:       6,
			background:   'white',
			border:       '1px solid black',
			borderRadius: '4px'
		}).appendTo(document.body);

		let move = (event: MouseEvent) => {
			let cursor = vec2.fromValues(event.pageX, event.pageY);
			this.angle = delta + vec2e.angle(vec2.subtract(vec2.create(), cursor, center));

			this.requestRender();
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);
			dot.remove();
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	rotateElements(event: MouseEvent): void {
		if(this.seneton.selection().length == 0)
			return;

		let vCursor = vec2.fromValues(event.pageX, event.pageY);
		let tCursor = this.view2table(vCursor);

		let selectedElements: Id[] = this.seneton.selection().slice();

		let index: { [index: number]: boolean } = {};
		for(let i = 0; i < selectedElements.length; i++)
			index[selectedElements[i]] = true;

		// ignore all children of selected parents to prevent cumulative movements
		for(let i = 0; i < selectedElements.length; i++) {
			let id = selectedElements[i];
			let parents = this.seneton.parents(id);
			for(let j = 0; j < parents.length; j++) {
				if(index[parents[j]] !== undefined) {
					selectedElements.splice(i, 1);
					i--;
					break;
				}
			}
		}

		let allElements:        Id[] = selectedElements.slice();
		let additionalElements: Id[] = [];
		let relativeParents:    { [index: number]: Id } = {};

		for(let i = 0; i < selectedElements.length; i++) {
			let id = selectedElements[i];
			let el = this.seneton.element(id);

			if(!el || !el.friction)
				continue;

			for(let i of this.seneton.elementsOnTop(id)) {
				if(index[i] === undefined) {
					allElements.push(i);
					additionalElements.push(i);
					relativeParents[i] = id;
					index[i] = true;
				}
			}
		}

		// stack children cannot be rotated. pop them!
		for(let id of allElements) {
			this.seneton.popFromStack(id);
			this.seneton.sendToTop(id);
		}
		this.seneton.commit();

		// render all elements immediately, so we have access to their pivots
		this.render();

		// calculate the common pivot point of all selected elements
		let vCenter = vec2.create();
		let count   = 0;
		for(let id of selectedElements) {
			let vPivot = this.ctx.pivot(id);
			if(vPivot) {
				vec2.add(vCenter, vCenter, vPivot);
				count++;
			}
		}
		if(count == 0)
			return;
		vec2.scale(vCenter, vCenter, 1.0 / count);
		vCenter[0] = Math.round(vCenter[0]);
		vCenter[1] = Math.round(vCenter[1]);

		let tCenter = this.view2table(vCenter);

		let initialPositions: { [index: number]: { position: vec2; angle: number; } } = {};
		for(let id of allElements) {
			let pos = this.seneton.state.absolutePosition(id);
			initialPositions[id] = {
				position: vec2.fromValues(pos.x, pos.y),
				angle:    pos.rotation
			};
		}

		let startAngle = vec2e.angle(vec2.subtract(vec2.create(), vCursor, vCenter));

		let dot = $('<div>').css({
			position:     'absolute',
			left:         vCenter[0] - 3,
			top:          vCenter[1] - 3,
			width:        6,
			height:       6,
			background:   'white',
			border:       '1px solid black',
			borderRadius: '4px'
		}).appendTo(document.body);

		let move = (event: MouseEvent) => {
			let vCursor = vec2.fromValues(event.pageX, event.pageY);
			let angle   = vec2e.angle(vec2.subtract(vec2.create(), vCursor, vCenter)) - startAngle;

			for(let id of selectedElements) {
				let element = this.seneton.element(id);
				if(!element)
					continue;

				let initial = initialPositions[id];

				let tDelta = vec2.subtract(vec2.create(), initial.position, tCenter);
				vec2e.rotate(tDelta, tDelta, angle);
				let tPosition = vec2.add(vec2.create(), tCenter, tDelta);

				let relPos: Position = { x: Math.round(tPosition[0]), y: Math.round(tPosition[1]), rotation: initial.angle + angle };
				if(element.parent !== undefined)
					relPos = this.seneton.state.relativePosition(relPos, element.parent);
				this.seneton.moveTo(relPos.x, relPos.y, relPos.rotation, id);
			}

			for(let id of selectedElements)
				this.seneton.snapToGrid(id);

			for(let id of additionalElements) {
				let element = this.seneton.element(id);
				if(!element || !element.position)
					continue;

				let parent = this.seneton.element(relativeParents[id]);
				if(!parent || !parent.position)
					continue;
				
				let initialChild  = initialPositions[element.id];
				let initialParent = initialPositions[parent.id];
				let current = parent.position;

				let tDelta = vec2.subtract(vec2.create(), initialChild.position, initialParent.position);
				vec2e.rotate(tDelta, tDelta, parent.position.rotation - initialParent.angle);
				let tPosition = vec2.add(vec2.create(), initialParent.position, tDelta);
				vec2.subtract(tPosition, tPosition, initialParent.position);
				vec2.add(tPosition, tPosition, vec2.fromValues(parent.position.x, parent.position.y));

				let relPos: Position = { x: Math.round(tPosition[0]), y: Math.round(tPosition[1]), rotation: initialChild.angle + (parent.position.rotation - initialParent.angle) };
				if(element.parent !== undefined)
					relPos = this.seneton.state.relativePosition(relPos, element.parent);
				this.seneton.moveTo(relPos.x, relPos.y, relPos.rotation, id);
			}

			for(let id of additionalElements)
				this.seneton.snapToGrid(id);

			this.seneton.commit();

			this.requestRender();
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);
			dot.remove();

			// send the rotation once more so other clients receive the final position together with a description
			for(let id of allElements) {
				let e = this.seneton.element(id);
				if(e && e.position)
					this.seneton.moveTo(e.position.x, e.position.y, e.position.rotation, id);
			}

			this.seneton.commit('Element rotated');
			this.requestRender();
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	selectOrDragElement(event: MouseEvent, element: Element): void {
		let initialSelection = !this.seneton.isSelectedByMe(element.id);

		let cursor = vec2.fromValues(event.pageX, event.pageY);

		if(!initialSelection) {
			if(event.ctrlKey)
				this.seneton.deselect(element.id);
		}
		else {
			if(!event.ctrlKey)
				this.seneton.deselectAll();
			this.seneton.select(element.id);
		}
		this.seneton.commit();

		this.requestRender();

		let move = (event: MouseEvent) => {
			function sqr(x: number) { return x * x; }
			let distance = Math.sqrt(sqr(event.pageX - cursor[0]) + sqr(event.pageY - cursor[1]));
			if(distance <= 3) // you call that a mouse-move? that was nothing...
				return;

			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up,   false);

			this.dragElement(cursor, event, element, () => {
				if(initialSelection)
					this.seneton.deselectAll();
			});
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up,   false);
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up,   false);
	}

	dragElement(vBeginPos: vec2, event: MouseEvent, element: Element, onDrop: Function): void {
		let tBeginPos = this.view2table(vBeginPos);

		this.dragging = true;

		let selectedElements: Id[] = this.seneton.selection().slice();

		let index: { [index: number]: boolean } = {};
		for(let i = 0; i < selectedElements.length; i++)
			index[selectedElements[i]] = true;

		// ignore all children of selected parents to prevent cumulative movements
		for(let i = 0; i < selectedElements.length; i++) {
			let id = selectedElements[i];
			let parents = this.seneton.parents(id);
			for(let j = 0; j < parents.length; j++) {
				if(index[parents[j]] !== undefined) {
					selectedElements.splice(i, 1);
					i--;
					break;
				}
			}
		}

		let allElements:        Id[] = selectedElements.slice();
		let additionalElements: Id[] = [];
		let relativeParents:    { [index: number]: Id } = {};

		for(let i = 0; i < selectedElements.length; i++) {
			let id = selectedElements[i];
			let el = this.seneton.element(id);

			if(!el || !el.friction)
				continue;

			for(let i of this.seneton.elementsOnTop(id)) {
				if(index[i] === undefined) {
					allElements.push(i);
					additionalElements.push(i);
					relativeParents[i] = id;
					index[i] = true;
				}
			}
		}

		for(let id of allElements)
			this.seneton.popFromStack(id);

		let sortedElements = this.seneton.getElementOrder(allElements);

		for(let id of sortedElements) {
			this.seneton.sendToTop(id);
			if(event.ctrlKey)
				this.seneton.peek(id);
		}
		this.seneton.commit();

		let initialPositions: { [index: number]: { position: vec2; angle: number; } } = {};
		for(let id of allElements) {
			let pos = this.seneton.state.absolutePosition(id);
			initialPositions[id] = {
				position: vec2.fromValues(pos.x, pos.y),
				angle:    pos.rotation
			};
		}

		// store the initial deltas of all selected elements
		// we need them later on to calculate individual drag positions
		let deltas: { [index: number]: vec2 } = {}
		for(let id of allElements) {
			deltas[id] = vec2.subtract(vec2.create(), initialPositions[id].position, tBeginPos);
		}

		this.requestRender();

		let move = (event: MouseEvent) => {
			let cursor = vec2.fromValues(event.pageX, event.pageY);

			this.dropTarget = undefined;
			let ids = this.ctx.pickAll(cursor);
			for(let i = 0; i < ids.length; i++) {
				let id = ids[i];
				if(this.seneton.isSelectedByMe(id))
					continue;

				let stack = this.seneton.skipStackChildren(id);
				if(!stack)
					continue;
				if(this.seneton.isSelectedByMe(stack.id))
					continue;
				if(stack.pinned)
					break;

				for(let i = 0; i < selectedElements.length; i++) {
					if(this.seneton.canPush(selectedElements[i], stack.id)) {
						this.dropTarget = stack.id;
						break;
					}
				}

				break;
			}

			for(let id of selectedElements) {
				let element = this.seneton.element(id);
				if(!element || !element.position)
					continue;

				let d = deltas[id];

				let pos = vec2.add(vec2.create(), this.view2table(cursor), d);

				let relPos: Position = { x: pos[0], y: pos[1], rotation: element.position.rotation };
				if(element.parent !== undefined)
					relPos = this.seneton.state.relativePosition(relPos, element.parent);
				this.seneton.moveTo(relPos.x, relPos.y, element.position.rotation, id);
			}

			if(this.dropTarget === undefined) {
				for(let id of selectedElements)
					this.seneton.snapToGrid(id);
			}

			for(let id of additionalElements) {
				let element = this.seneton.element(id);
				if(!element)
					continue;

				let parent = this.seneton.element(relativeParents[id]);
				if(!parent || !parent.position)
					continue;
					
				let initialChild  = initialPositions[element.id];
				let initialParent = initialPositions[parent.id];
				let current = parent.position;

				let tDelta = vec2.subtract(vec2.create(), initialChild.position, initialParent.position);
				vec2e.rotate(tDelta, tDelta, parent.position.rotation - initialParent.angle);
				let tPosition = vec2.add(vec2.create(), initialParent.position, tDelta);
				vec2.subtract(tPosition, tPosition, initialParent.position);
				vec2.add(tPosition, tPosition, vec2.fromValues(parent.position.x, parent.position.y));

				let relPos: Position = { x: tPosition[0], y: tPosition[1], rotation: initialChild.angle + (parent.position.rotation - initialParent.angle) };
				if(element.parent !== undefined)
					relPos = this.seneton.state.relativePosition(relPos, element.parent);
				this.seneton.moveTo(relPos.x, relPos.y, relPos.rotation, id);
			}

			for(let id of additionalElements)
				this.seneton.snapToGrid(id);

			this.seneton.commit();

			this.requestRender();
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up,   false);

			this.dragging = false;

			if(this.dropTarget !== undefined) {
				let sortedElements = this.seneton.getElementOrder(selectedElements);
				for(let id of sortedElements)
					this.seneton.push(id, this.dropTarget);
				this.seneton.deselectAll();
				this.dropTarget = undefined;
			}
			else {
				// send the movement once more so other clients receive the final position together with a description
				for(let id of allElements) {
					let e = this.seneton.element(id);
					if(e && e.position)
						this.seneton.moveTo(e.position.x, e.position.y, e.position.rotation, id);
				}
			}

			if(onDrop)
				onDrop();

			this.seneton.commit('Element moved');
			this.requestRender();
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	selectElements(event: MouseEvent): void {
		let cursor = vec2.fromValues(event.pageX, event.pageY);

		let id = this.ctx.pick(cursor);
		if(id !== undefined) {
			let element = this.seneton.element(id);
			if(element && !element.pinned) {
				this.selectOrDragElement(event, element);
				return;
			}
		}

		if(!event.ctrlKey) {
			this.seneton.deselectAll();
			this.requestRender();
		}
		this.seneton.commit();

		let move = (event: MouseEvent) => {
			function sqr(x: number) { return x * x; }
			let distance = Math.sqrt(sqr(event.pageX - cursor[0]) + sqr(event.pageY - cursor[1]));
			if(distance <= 3) // you call that a mouse-move? that was nothing...
				return;

			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);

			this.selectRectangle(cursor, event);
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	selectRectangle(begin: Float32Array, event: MouseEvent): void {
		let end = vec2.fromValues(event.pageX, event.pageY);

		let rect = $('<div>').addClass('selection-rectangle').css({
			boxSizing: 'border-box',
			position:  'absolute',
			zIndex:    100,
			left:      Math.min(begin[0], end[0]),
			top:       Math.min(begin[1], end[1]),
			width:     Math.max(begin[0], end[0]) - Math.min(begin[0], end[0]),
			height:    Math.max(begin[1], end[1]) - Math.min(begin[1], end[1])
		}).appendTo(document.body);

		let move = (event: MouseEvent) => {
			end = vec2.fromValues(event.pageX, event.pageY);

			rect.css({
				left:   Math.min(begin[0], end[0]),
				top:    Math.min(begin[1], end[1]),
				width:  Math.max(begin[0], end[0]) - Math.min(begin[0], end[0]),
				height: Math.max(begin[1], end[1]) - Math.min(begin[1], end[1])
			});

			this.tempSelection = {};
			let ids = this.ctx.pickRect(Math.min(begin[0], end[0]), Math.min(begin[1], end[1]), Math.max(begin[0], end[0]), Math.max(begin[1], end[1]));
			for(let i = 0; i < ids.length; i++) {
				if(this.seneton.isRoot(ids[i]))
					this.tempSelection[ids[i]] = true;
			}

			this.requestRender();
		};

		let up = () => {
			document.removeEventListener('mousemove', move, false);
			document.removeEventListener('mouseup'  , up  , false);

			rect.remove();

			this.tempSelection = {};
			let ids = this.ctx.pickRect(Math.min(begin[0], end[0]), Math.min(begin[1], end[1]), Math.max(begin[0], end[0]), Math.max(begin[1], end[1]));
			for(let i = 0; i < ids.length; i++) {
				if(this.seneton.isRoot(ids[i]))
					this.seneton.select(ids[i]);
			}
			this.seneton.commit();

			this.requestRender();
		};

		document.addEventListener('mousemove', move, false);
		document.addEventListener('mouseup'  , up  , false);
	}

	startDrawing(event: MouseEvent): void {
		let vCursor = vec2.fromValues(event.pageX, event.pageY);
		let tCursor = this.view2table(vCursor);
		let lastCursor = tCursor;

		let id = this.seneton.createDrawingId();
		let me = this.seneton.me();

		this.seneton.startFreehandDrawing(id, tCursor[0], tCursor[1], me ? me.color : [1.0, 1.0, 1.0, 1.0]);
		this.seneton.commit('starts drawing');

		let move = (event: MouseEvent) => {
			let vCursor = vec2.fromValues(event.pageX, event.pageY);
			let tCursor = this.view2table(vCursor);

			// ignore single pixel distances
			if(vec2.distance(tCursor, lastCursor) < 2)
				return;

			if(!this.seneton.drawingExists(id)) { // someone may have cleared all drawings in the meantime
				id = this.seneton.createDrawingId();
				this.seneton.startFreehandDrawing(id, lastCursor[0], lastCursor[1], me ? me.color : [1.0, 1.0, 1.0, 1.0]);
			}
			this.seneton.continueFreehandDrawing(id, tCursor[0], tCursor[1]);
			this.seneton.commit();
			this.requestRender();

			lastCursor = tCursor;
		};

		let drop = () => {
			this.overlay.removeEventListener('mousemove', move, false);
			this.overlay.removeEventListener('mouseup',   drop, false);
		};

		this.overlay.addEventListener('mousemove', move, false);
		this.overlay.addEventListener('mouseup',   drop, false);
	}

	requestRender(): void {
		requestAnimationFrame((t: number) => {
			if(t === this.lastRender)
				return;
			this.lastRender = t;
			this.render();
		});
	}

	render(): void {
		let viewState = (this.renderState !== undefined) ? this.renderState : this.seneton.state;

		this.octx.clearRect(0, 0, this.overlay.width, this.overlay.height);
		this.octx.save();
		this.octx.translate(Math.round(this.overlay.width / 2), Math.round(this.overlay.height / 2));
		this.octx.scale(this.scale, this.scale);
		this.octx.rotate(this.angle);
		this.octx.translate(-this.position[0], -this.position[1]);
		for(let id of viewState.drawingIds) {
			let d = viewState.drawing(id);
			switch(d.type) {
				case DrawingType.Freehand:
					this.octx.beginPath();
					this.octx.moveTo(d.start[0], d.start[1]);
					for(let p of d.points)
						this.octx.lineTo(p[0], p[1]);
					this.octx.lineWidth = 3.0;
					this.octx.strokeStyle = 'rgba(' + Math.round(255 * d.color[0]) + ', ' + Math.round(255 * d.color[1]) + ', ' + Math.round(255 * d.color[2]) + ', ' + d.color[3] + ')';
					this.octx.stroke();
					break;
			}
		}
		this.octx.restore();

		this.ctx.clear();
		this.ctx.translate(vec2.fromValues(Math.round(this.canvas.width / 2), Math.round(this.canvas.height / 2)));
		this.ctx.scale(this.scale);
		this.ctx.rotate(this.angle);
		this.ctx.translate(vec2.fromValues(-this.position[0], -this.position[1]));

		for(let l = 0; l < viewState.layerIds.length; l++) {
			let layerId = viewState.layerIds[l];
			let layer   = viewState.layer(layerId);
			if(!layer)
				continue;

			for(let e = 0; e < layer.elements.length; e++) {
				let elementId = layer.elements[e];
				let element   = viewState.element(elementId);
				if(element && element.position) {
					this.ctx.push();
					this.ctx.translate(vec2.fromValues(element.position.x, element.position.y));
					this.ctx.rotate(element.position.rotation);
					this.renderElementShadow(viewState, element, this.seneton.userId);
					this.ctx.pop();
				}
			}

			for(let e = 0; e < layer.elements.length; e++) {
				let elementId = layer.elements[e];
				let element   = viewState.element(elementId);
				if(element && element.position) {
					this.ctx.push();
					this.ctx.translate(vec2.fromValues(element.position.x, element.position.y));
					this.ctx.rotate(element.position.rotation);
					this.renderElement(viewState, element, this.seneton.userId);
					this.ctx.pop();
				}
			}
		}
	}

	renderElement(viewState: State, element: Element, userId: Id): void {
		switch(element.type) {
			case 'stack':   this.renderStack         (viewState, element, userId); break;
			case 'element': this.renderRegularElement(viewState, element, userId); break;
			case 'text':    this.renderTextElement   (viewState, element, userId); break;
			default:        break;
		}
	}

	renderElementShadow(viewState: State, element: Element, userId: Id): void {
		switch(element.type) {
			case 'stack':   this.renderStackShadow         (viewState, element, userId); break;
			case 'element': this.renderRegularElementShadow(viewState, element, userId); break;
			case 'text':    this.renderTextElementShadow   (viewState, element, userId); break;
			default:        break;
		}
	}

	renderStack(viewState: State, element: StackElement, userId: Id): void {
		this.ctx.setPivot(element.id);
		this.ctx.push();
		this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
		{
			this.ctx.push();
			this.ctx.translate(vec2.fromValues(-1.0, -20.0));
			let options = <any>{
				id:    element.id,
				color: element.background
			};
			if(viewState.isSelected(element.id, userId) || this.tempSelection[element.id]) {
				let user = viewState.user(userId);
				if(user) {
					options.shadow      = 15.0;
					options.shadowColor = user.color;
				}
			}
			else if(viewState.elementSelection(element.id).length > 0) {
				let user = viewState.user(viewState.elementSelection(element.id)[0]);
				if(user) {
					options.shadow      = 15.0;
					options.shadowColor = user.color;
				}
			}
			else if(element.id == this.dropTarget) {
				options.shadow      = 15.0;
				options.shadowColor = [1.0, 1.0, 1.0, 1.0];
			}
			this.ctx.renderRect(element.width + 2.0, element.height + 21.0, options);
			this.ctx.translate(vec2.fromValues(3.0, 3.0));
			let title = element.title;
			if(title.length > 0)
				title += ' ';
			title += '[' + element.children.length + ']';
			this.ctx.translate(vec2.fromValues(Math.round(0.5 * element.width), 0.0));
			this.ctx.renderText(title, {
				size:        13,
				align:       webgl.TextAlign.Center,
				color:       [1.0, 1.0, 1.0, 1.0],
				shadow:      element.deleteOnLast ? 2.0 : 0.0,
				shadowColor: [0.0, 0.0, 0.0, 1.0]
			});
			this.ctx.pop();
		}
		if(element.image && element.children.length == 0) {
			let url = this.seneton.resourceUrl(element.image);
			if(url) {
				this.ctx.renderImage(url, {
					id:     element.id,
					width:  element.width,
					height: element.height,
					tint:   element.tint
				});
			}
		}
		this.ctx.pop();
		if(element.children.length > 0) {
			if(element.delta.x === 0 && element.delta.y === 0 && element.delta.rotation === 0) {
				let childId = element.children[element.children.length - 1];
				let child   = viewState.element(childId);
				if(child) {
					let scale = Math.min(1.0, element.width / child.width, element.height / child.height);

					this.ctx.push();
					this.ctx.scale(scale);
					this.renderElement(viewState, child, this.seneton.userId);
					this.ctx.pop();
				}
			}
			else {
				this.ctx.push();
				for(let childId of element.children) {
					let child = viewState.element(childId);
					if(child) {
						let scale = Math.min(1.0, element.width / child.width, element.height / child.height);

						this.ctx.push();
						this.ctx.scale(scale);
						this.renderElement(viewState, child, this.seneton.userId);
						this.ctx.pop();

						this.ctx.rotate(element.delta.rotation);
						this.ctx.translate(vec2.fromValues(element.delta.x, element.delta.y));
					}
				}
				this.ctx.pop();
			}
		}
	}

	renderRegularElement(viewState: State, element: RegularElement, userId: Id): void {
		this.ctx.setPivot(element.id);

		this.ctx.push();
		this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
		let face = viewState.isPeeking(element.id, userId) ? element.peeks[userId] : element.face;
		if(face !== undefined && element.faces !== undefined && element.faces.length > 0) {
			let options: webgl.RenderImageOptions<Id> = {
				id:     element.id,
				width:  element.width,
				height: element.height,
				tint:   element.tint
			};
			if(viewState.isSelected(element.id, userId) || this.tempSelection[element.id]) {
				let user  = viewState.user(userId);
				let color = user ? user.color : [1.0, 1.0, 1.0, 1.0];
				options.shadow      = 15.0;
				options.shadowColor = color;
			}
			else if(viewState.elementSelection(element.id).length > 0) {
				let user  = viewState.user(viewState.elementSelection(element.id)[0]);
				let color = user ? user.color : [1.0, 1.0, 1.0, 1.0];
				options.shadow      = 15.0;
				options.shadowColor = color;
			}
			else if(element.id == this.dropTarget) {
				options.shadow      = 15.0;
				options.shadowColor = [1.0, 1.0, 1.0, 1.0];
			}

			let src = element.faces[face];
			if(typeof src === 'string') {
				let url = this.seneton.resourceUrl(src);
				if(url !== undefined)
					this.ctx.renderImage(url, options);
			}
			else if(Array.isArray(src)) {
				if(this.imageCache.has(src)) {
					let image = this.imageCache.get(src);
					if(image !== undefined)
						this.ctx.renderImage(image, options);
				}
				else {
					let action = src;
					let result = this.seneton.executeAction(action);
					if(result instanceof Promise) {
						this.imageCache.set(action, undefined);
						result.then(canvas => {
							this.imageCache.set(action, canvas);
							this.requestRender();
						});
					}
					else if(result instanceof HTMLCanvasElement) {
						this.imageCache.set(action, result);
						this.ctx.renderImage(result, options);
					}
				}
			}
		}
		if(viewState.isSomeonePeeking(element.id)) {
			let w = 0.9 * element.width;
			let h = w / 336 * 187;
			this.ctx.translate(vec2.fromValues(0.5 * (element.width - w), 0.5 * (element.height - h)));
			this.ctx.renderImage('images/peeking.svg', {
				width:  w,
				height: h,
				tint:   [0.4, 0.4, 0.4, 0.4]
			});
		}
		this.ctx.pop();

		if(face == 0) {
			for(let childId of element.children) {
				let child = viewState.element(childId);
				if(child && child.position) {
					this.ctx.push();
					this.ctx.translate(vec2.fromValues(child.position.x, child.position.y));
					this.ctx.rotate(child.position.rotation);
					this.renderElement(viewState, child, userId);
					this.ctx.pop();
				}
			}
		}
	}

	renderTextElement(viewState: State, element: TextElement, userId: Id): void {
		this.ctx.setPivot(element.id);
		this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));

		let options: webgl.RenderTextOptions<Id> = {
			id   : element.id,
			color: (viewState.isSelected(element.id, userId) && element.activeBackground !== undefined) ? element.activeBackground : element.background
		};

		if(viewState.isSelected(element.id, userId) || this.tempSelection[element.id]) {
			let user  = viewState.user(userId);
			let color = user ? user.color : [1.0, 1.0, 1.0, 1.0];
			options.shadow = 15.0;
			options.shadowColor = color;
		}
		else if(viewState.elementSelection(element.id).length > 0) {
			let user  = viewState.user(viewState.elementSelection(element.id)[0]);
			let color = user ? user.color : [1.0, 1.0, 1.0, 1.0];
			options.shadow = 15.0;
			options.shadowColor = color;
		}

		this.ctx.renderRect(element.width, element.height, options);

		this.ctx.push();
		switch(element.align) {
			case 'left':   break;
			case 'center': this.ctx.translate(vec2.fromValues(Math.round(0.5 * element.width), 0.0)); break;
			case 'right':  this.ctx.translate(vec2.fromValues(element.width, 0.0));                   break;
		}
		if(element.text !== undefined) {
			this.ctx.renderText(element.text, {
				font:  element.font,
				size:  element.size,
				align: (<{ [index: string]: webgl.TextAlign }>{ left: webgl.TextAlign.Left, center: webgl.TextAlign.Center, right: webgl.TextAlign.Right })[element.align],
				color: element.color
			});
		}
		this.ctx.pop();
	}

	renderStackShadow(viewState: State, element: StackElement, userId: Id): void {
		if(element.deleteOnLast)
			return;
		if(element.shadowRadius <= 0.0)
			return;

		this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
		this.ctx.translate(vec2.fromValues(-1.0, -20.0));
		this.ctx.translate(new Float32Array(element.shadowOffset));
		this.ctx.renderRect(element.width + 2.0, element.height + 21.0, {
			shadow:      element.shadowRadius,
			shadowColor: element.shadowColor,
			color:       [0.0, 0.0, 0.0, 0.0]
		});
	}

	renderRegularElementShadow(viewState: State, element: RegularElement, userId: Id): void {
		if(element.shadowRadius <= 0.0)
			return;

		let face = viewState.isPeeking(element.id, userId) ? element.peeks[userId] : element.face;
		if(face !== undefined && element.faces !== undefined && element.faces.length > 0) {
			let src = element.faces[face];
			if(typeof src === 'string') {
				let url = this.seneton.resourceUrl(src);
				if(url) {
					this.ctx.push();
					this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
					this.ctx.translate(new Float32Array(element.shadowOffset));
					this.ctx.renderImage(url, {
						shadow:      element.shadowRadius,
						shadowColor: element.shadowColor,
						width:       element.width,
						height:      element.height,
						tint:        [0.0, 0.0, 0.0, 0.0]
					});
					this.ctx.pop();
				}
			}
			else if(Array.isArray(src)) {
				if(this.imageCache.has(src)) {
					let image = this.imageCache.get(src);
					if(image !== undefined) {
						this.ctx.push();
						this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
						this.ctx.translate(new Float32Array(element.shadowOffset));
						this.ctx.renderImage(image, {
							shadow:      element.shadowRadius,
							shadowColor: element.shadowColor,
							width:       element.width,
							height:      element.height,
							tint:        [0.0, 0.0, 0.0, 0.0]
						});
						this.ctx.pop();
					}
				}
				else {
					let action = src;
					let result = this.seneton.executeAction(action);
					if(result instanceof Promise) {
						this.imageCache.set(action, undefined);
						result.then(canvas => {
							this.imageCache.set(action, canvas);
							this.requestRender();
						});
					}
					else if(result instanceof HTMLCanvasElement) {
						this.imageCache.set(action, result);

						this.ctx.push();
						this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
						this.ctx.translate(new Float32Array(element.shadowOffset));
						this.ctx.renderImage(result, {
							shadow:      element.shadowRadius,
							shadowColor: element.shadowColor,
							width:       element.width,
							height:      element.height,
							tint:        [0.0, 0.0, 0.0, 0.0]
						});
						this.ctx.pop();
					}
				}
			}
		}

		if(face == 0) {
			for(let childId of element.children) {
				let child = viewState.element(childId);
				if(child && child.position) {
					this.ctx.push();
					this.ctx.translate(vec2.fromValues(child.position.x, child.position.y));
					this.ctx.rotate(child.position.rotation);
					this.renderElementShadow(viewState, child, userId);
					this.ctx.pop();
				}
			}
		}
	}

	renderTextElementShadow(viewState: State, element: TextElement, userId: Id): void {
		if(element.shadowRadius <= 0.0)
			return;

		this.ctx.translate(vec2.fromValues(-element.pivot[0], -element.pivot[1]));
		this.ctx.translate(new Float32Array(element.shadowOffset));
		this.ctx.renderRect(element.width, element.height, {
			shadow     : element.shadowRadius,
			shadowColor: element.shadowColor,
			color      : [0.0, 0.0, 0.0, 0.0]
		});
	}
}
