import * as $ from 'jquery';

import { Seneton } from '../seneton'


export class TurnBar {
	private seneton: Seneton;
	element: HTMLDivElement;

	constructor(seneton: Seneton) {
		this.seneton = seneton;

		this.element = document.createElement('div');

		this.seneton.turnChanged.on(() => { this.refresh(); });
		this.refresh();
	}

	refresh() {
		$(this.element).empty();

		let seq   = this.seneton.state.turnSequence;
		let depth = 0;
		let turn  = this.seneton.state.turn;
		let y     = 0;
		while(seq) {
			let group = $('<div>').addClass('turn-group');

			if(seq.type === 'counter') {
				let currentDepth = depth;
				group.append(
					$('<div>').addClass('counter')
					.append(
						$('<div>').addClass('prev')
						.text("<")
						.on('click', () => {
							this.seneton.setTurn(turn.slice(0, currentDepth).concat(turn[currentDepth] - 1));
							this.seneton.commit();
						})
					)
					.append(
						$('<div>').addClass('name')
						.text(
							seq.name.replace('%n', '' + turn[depth])
						)
					)
					.append(
						$('<div>').addClass('next')
						.text(">")
						.on('click', () => {
							this.seneton.setTurn(turn.slice(0, currentDepth).concat(turn[currentDepth] + 1));
							this.seneton.commit();
						})
					)
				);

				seq = seq.child;
				depth++;
			}
			else if(seq.type === 'options') {
				let value = depth < turn.length ? turn[depth] : -1;
				let opts = $('<div>').addClass('options').appendTo(group);
				let currentDepth = depth;
				for(const [i, option] of seq.options.entries()) {
					opts.append(
						$('<div>').addClass('option')
						.text(option.name)
						.toggleClass('active', i == value)
						.on('click', () => {
							this.seneton.setTurn(turn.slice(0, currentDepth).concat(i));
							this.seneton.commit();
						})
					);
				}

				seq = depth < turn.length ? seq.options[turn[depth]].child : undefined;
				depth++;
			}

			group.css({ maxWidth: window.innerWidth + 'px' });
			$(this.element).append(group);
			group.css({ top: y, left: Math.floor((window.innerWidth - (group.outerWidth() || 0)) / 2) });
			y += (group.outerHeight() || 0);
		}
	}
}
