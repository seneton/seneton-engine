export class HelpBar {
	helpbar: HTMLElement;
	expanded: boolean;
	expander: HTMLElement;
	iframe: HTMLIFrameElement;

	constructor(src: string, globals: { [index:string]: any }) {
		this.expanded = false;

		this.helpbar = document.createElement('div');
		this.helpbar.setAttribute('id', 'helpbar');
		this.helpbar.style['right'] = '-300px';
		this.helpbar.style['width'] = '300px';

		this.onResize();
		window.addEventListener('resize', this.onResize.bind(this), false);

		this.expander = document.createElement('div');
		this.expander.setAttribute('id', 'helpbar-expander');
		this.helpbar.appendChild(this.expander);

		this.expander.addEventListener('click', () => {
			if(this.expanded) {
				this.helpbar.style['right'] = '-300px';
				this.helpbar.classList.remove('expanded');
				this.expanded = false;
			}
			else {
				this.helpbar.style['right'] = '0px';
				this.helpbar.classList.add('expanded');
				this.expanded = true;
			}
		}, false);

		this.iframe = document.createElement('iframe');
		this.iframe.addEventListener('load', () => {
			for(let key in globals)
				(<any>this.iframe.contentWindow)[key] = globals[key];
		});
		this.iframe.src = src;
		this.helpbar.appendChild(this.iframe);

		document.body.appendChild(this.helpbar);
	}

	onResize() {
		this.helpbar.style['height'] = window.innerHeight + 'px';
	}

	destroy() {
		document.body.removeChild(this.helpbar);
		window.removeEventListener('resize', this.onResize, false);
	}
}
