import { Action, Id, Position } from '../shared/state';

import { Seneton } from './seneton';

import { vec2, vec2e } from './util/geometry';


// helper functions
export function rad(grad: number): number {
	return (grad / 180 * Math.PI);
}

export function grad(rad: number): number {
	return (rad * 180 / Math.PI);
}

export function sqr(x: number): number {
	return (x * x);
}


export type GridFunction = (pos: Position) => Position;

export function griddy(dx: number, dy: number, dr: number, pos: Position): Position {
	return {
		x:        Math.round(pos.x / dx) * dx,
		y:        Math.round(pos.y / dy) * dy,
		rotation: rad(Math.round(grad(pos.rotation) / dr) * dr)
	};
}


export function vHexagonal(dx: number, dy: number, pos: Position): Position {
	let even = Math.round(pos.y / dy) % 2;
	return {
		x:        Math.round(pos.x / dx - even * 0.5) * dx + even * 0.5 * dx,
		y:        Math.round(pos.y / dy) * dy,
		rotation: rad(Math.round(grad(pos.rotation) / 180) * 180)
	};
}


export function rectGridWithOffsets(oX: number, oY: number, deltaX: number, deltaY: number, countX: number, countY: number, pos: Position): Position {
	let realOffsetX = oX - (countX * deltaX) * 0.5 + deltaX * 0.5;
	let realOffsetY = oY - (countY * deltaY) * 0.5 + deltaY * 0.5;

	pos.x = Math.min(countX - 1, Math.max(0, Math.round((pos.x - realOffsetX) / deltaX))) * deltaX + realOffsetX;
	pos.y = Math.min(countY - 1, Math.max(0, Math.round((pos.y - realOffsetY) / deltaY))) * deltaY + realOffsetY;
	return pos;
}


export function rectGridWithOffsetsAndRotation(oX: number, oY: number, oR: number, deltaX: number, deltaY: number, deltaR: number, countX: number, countY: number, countR: number, pos: Position): Position {
	let realOffsetX = oX - (countX * deltaX) * 0.5 + deltaX * 0.5;
	let realOffsetY = oY - (countY * deltaY) * 0.5 + deltaY * 0.5;

	pos.x = Math.min(countX - 1, Math.max(0, Math.round((pos.x - realOffsetX) / deltaX))) * deltaX + realOffsetX;
	pos.y = Math.min(countY - 1, Math.max(0, Math.round((pos.y - realOffsetY) / deltaY))) * deltaY + realOffsetY;
	pos.rotation = ((pos.rotation - rad(oR)) % (2.0*Math.PI) + 2.0*Math.PI) % (2.0*Math.PI);
	pos.rotation = rad(Math.min(countR - 1, Math.max(0, Math.round(grad(pos.rotation) / deltaR))) * deltaR + oR);
	return pos;
}


export function pointGrid(...points: { x: number; y: number; rotation?: number }[]): Position {
	if(points.length === 0)
		throw new Error('pointGrid called without arguments');

	let pos = <Position>points.pop();

	let best     = { x: 999999, y: 999999, rotation: 999999 };
	let bestDist = 999999;
	for(let point of points) {
		let dist = Math.sqrt(sqr(point.x - pos.x) + sqr(point.y - pos.y));
		if(dist < bestDist) {
			best     = { x: point.x, y: point.y, rotation: rad(point.rotation) || 0 };
			bestDist = dist;
		}
	}
	return best;
}


export function relativeTo(seneton: Seneton, id: Id, grid: GridFunction | Action, pos: Position): Position {
	let e = seneton.element(id);
	if(!e || e.parent || e.position === undefined)
		return { x: 999999, y: 999999, rotation: 999999 };

	pos.rotation -= e.position.rotation;
	let v = vec2e.rotate(vec2.create(), vec2.fromValues(pos.x - e.position.x, pos.y - e.position.y), -e.position.rotation);
	pos.x = v[0];
	pos.y = v[1];
	pos = (typeof grid === 'function') ? grid(pos) : seneton.executeAction(grid, pos);
	v = vec2e.rotate(vec2.create(), vec2.fromValues(pos.x, pos.y), e.position.rotation);
	pos.x = v[0] + e.position.x;
	pos.y = v[1] + e.position.y;
	pos.rotation += e.position.rotation;
	return pos;
}


export function magnet(seneton: Seneton, distance: number, grid: GridFunction | Action, pos: Position): Position {
	let backup = { x: pos.x, y: pos.y, rotation: pos.rotation };
	pos = (typeof grid === 'function') ? grid(pos) : seneton.executeAction(grid, pos);
	if(vec2.distance([pos.x, pos.y], [backup.x, backup.y]) > distance)
		pos = backup;
	return pos;
}


export function relativeToGroup(seneton: Seneton, groupId: Id, grid: GridFunction | Action, pos: Position): Position {
	if(!seneton.groupExists(groupId))
		return { x: 999999, y: 999999, rotation: 999999 };

	let group = seneton.group(groupId);
	if(!group)
		return { x: 999999, y: 999999, rotation: 999999 };

	let grids: GridFunction[] = [];
	for(let id of group.elements)
		grids.push(pos => relativeTo(seneton, id, grid, pos));

	return oneOf(seneton, grids, pos);
}


export function fixRotation(seneton: Seneton, grid: GridFunction | Action, rotation: number, pos: Position): Position {
	pos = (typeof grid === 'function') ? grid(pos) : seneton.executeAction(grid, pos);
	pos.rotation = rotation;
	return pos;
}


export function rotationGrid(deltaR: number, countR: number, oR: number, pos: Position): Position {
	pos.rotation = ((pos.rotation - rad(oR)) % (2.0*Math.PI) + 2.0*Math.PI) % (2.0*Math.PI);
	pos.rotation = rad(Math.min(countR - 1, Math.max(0, Math.round(grad(pos.rotation) / deltaR))) * deltaR + oR);
	return pos;
}


export function oneOf(seneton: Seneton, grids: (GridFunction | Action)[], pos: Position): Position {
	let best     = { x: 999999, y: 999999, rotation: 999999 };
	let bestDist = 999999;
	for(let grid of grids) {
		let point = { x: pos.x, y: pos.y, rotation: pos.rotation };
		point = (typeof grid === 'function') ? grid(point) : seneton.executeAction(grid, point);
		let dist  = Math.sqrt(sqr(point.x - pos.x) + sqr(point.y - pos.y));
		if(dist < bestDist) {
			best     = point;
			bestDist = dist;
		}
	}
	return best;
}


export function offsetBy(seneton: Seneton, dx: number, dy: number, grid: GridFunction | Action, pos: Position): Position {
	pos.x -= dx;
	pos.y -= dy;
	pos = (typeof grid === 'function') ? grid(pos) : seneton.executeAction(grid, pos);
	pos.x += dx;
	pos.y += dy;
	return pos;
}
