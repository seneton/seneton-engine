import * as $ from 'jquery';
import * as io from 'socket.io-client';

import { GameInfo, Uid } from '../shared/gameinfo';
import { Command, Id, JsonState, State, User } from '../shared/state';

import * as grids from './grids';
import { Seneton, ResourcesList } from './seneton';
import { HelpBar } from './gui/helpbar';
import { SideBar } from './gui/sidebar';
import { Table   } from './gui/table';
import { TurnBar } from './gui/turnbar';

import { Environment } from './util/environment';
import { Event } from './util/events';
import { vec2, mat2, vec2e } from './util/geometry';


// History entries serve as undo states. Each one contains an immutable copy of the state.
// Due to substructure sharing the overhead for this is fairly small.
export interface HistoryEntry {
	user?:       User;
	date:        Date;
	description: string;
	commands:    Command[];
	state:       State;
}


// The main class containing the full client application, including the UI, the server connection, and the game state.
export class Client {
	// connection to the server
	private socket: SocketIOClient.Socket;

	// list of sent messages that the server has not yet responded to
	private sentMessages: { description: string; commands: Command[]; }[] = [];

	// the current authoritative state of the server
	private serverState: State;

	// list of games that the server reported
	public serverGames: GameInfo[] = [];
	public serverGamesIndex: Map<Uid, GameInfo> = new Map<Uid, GameInfo>();
	public serverGamesChanged = new Event;

	// main Seneton environment
	public seneton = new Seneton();

	// list of undo states
	public history: HistoryEntry[] = [];
	public historyEntryAdded = new Event;

	constructor() {
		// initialize the server state to something that does not crash
		this.serverState = new State();
		this.serverState.immutable();

		// initialize the connection to the server and setup all message handlers
		this.socket = io();
		this.socket.on('hello',    this.onHello.bind(this));
		this.socket.on('accept',   this.onAccept.bind(this));
		this.socket.on('decline',  this.onDecline.bind(this));
		this.socket.on('commands', this.onCommands.bind(this));

		// initialize main client state
		this.seneton.commitCreated.on(commit => {
			// ignore empty commands, to allow requesting changes without having to check if something was actually changed
			if(commit.commands.length == 0)
				return;

			// construct the message that will be sent
			let message = {
				description: commit.description || '',
				commands:    commit.commands.slice(0) // create a shallow copy of the commands
			};

			// send the message, and remember what we sent so we can roll-back it later if needed
			this.sentMessages.push(message);
			this.socket.emit('commands', message);
		});

		// initialize the UI

		// create the table
		let table = new Table(this.seneton);
		document.body.appendChild(table.canvas);
		document.body.appendChild(table.overlay);

		// keep the table canvas always at full screen
		table.resize(window.innerWidth, window.innerHeight);
		window.addEventListener('resize', () => {
			table.resize(window.innerWidth, window.innerHeight);
		}, false);

		// let the table handle global key events
		document.addEventListener('keypress', table.onKeyPress.bind(table), false);
		document.addEventListener('keydown', table.onKeyDown.bind(table), false);
		document.addEventListener('keyup', table.onKeyUp.bind(table), false);

		// prevent some default browser events from disturbing the table
		document.addEventListener('contextmenu', (event) => {
			event.preventDefault();
		}, false);
		document.addEventListener('selectstart', (event) => {
			event.preventDefault();
		}, false);

		// create left sidebar
		let sidebar = new SideBar(this, this.seneton, table);
		document.body.appendChild(sidebar.element);

		// create top turnbar
		let turnbar = new TurnBar(this.seneton);
		document.body.appendChild(turnbar.element);

		// create helpbar on the right
		// not every game has a helpbar, so we keep it optional
		let helpbar: HelpBar | undefined = undefined;
		// update the helpbar if a different game is started
		this.seneton.gameChanged.on(() => {
			// destroy the old helpbar
			if(helpbar !== undefined) {
				helpbar.destroy();
				helpbar = undefined;
			}

			// create a new helpbar, if the game needs it
			let help = this.seneton.helpUrl();
			if(help)
				helpbar = new HelpBar(help, { table: table });
		});

		// change the background image if a game is active
		// TODO this should really be handled somewhere else...
		this.seneton.stateChanged.on(() => {
			if(this.seneton.state.layerIds.length > 0 && document.body.style.backgroundImage != 'url(images/background.jpg)')
				document.body.style.backgroundImage = 'url(images/background.jpg)';
		});
	}

	// Start a new game by setting the appropriate meta-data and running the initialization script.
	// The function needs to load external resources and therefore returns a promise, but you don't actually have to wait for it just to start a new game.
	async initializeGame(uid: Uid): Promise<void> {
		this.seneton.execute({ type: 'StartGame', gameUid: uid });

		// load all game resources
		let game      = this.serverGamesIndex.get(this.seneton.state.gameUid);
		let libraries = await this.loadLibraries(game);
		let resources = await this.loadResources(game);
		this.seneton.setGame(game, libraries, resources);

		// run the initialization script
		let initScript = await this.loadInitScript(game);
		this.seneton.environment.eval(initScript);

		// commit all changes
		this.seneton.commit('Game initialized');
		this.seneton.stateChanged.emit();
	}

	// Message handler for 'hello' message. This message will only be received once, before anything else
	private async onHello(data: { userId: Id; games: GameInfo[]; state: JsonState; }): Promise<void> {
		// initialize the game list
		this.serverGames = data.games;
		this.serverGamesIndex.clear();
		for(let game of this.serverGames)
			this.serverGamesIndex.set(game.uid, game);
		this.serverGamesChanged.emit();

		// add a history entry indicating the estabilished connection
		this.history.push({
			user:        undefined,
			date:        new Date(),
			description: 'Connection established',
			commands:    [],
			state:       this.serverState
		});
		this.historyEntryAdded.emit();

		// initialize the server state
		this.serverState = State.fromJson(data.state);
		this.serverState.immutable();

		// initialize the client state
		this.seneton.userId = data.userId;
		this.seneton.state = this.serverState.mutable();
		this.seneton.usersChanged.emit();
		this.seneton.layersChanged.emit();
		this.seneton.groupsChanged.emit();
		this.seneton.turnChanged.emit();
		this.seneton.stateChanged.emit();

		// load all game resources
		let game      = this.serverGamesIndex.get(this.seneton.state.gameUid);
		let libraries = await this.loadLibraries(game);
		let resources = await this.loadResources(game);
		this.seneton.setGame(game, libraries, resources);
	}

	// Message handler for 'accept' message. This messages indicates that the server accepted our commands, so we advance our server state
	private onAccept(): void {
		// drop the declined message from our list
		let message = this.sentMessages.shift();
		if(!message)
			throw new Error('received "accept" without having sent a message');

		// advance the server state
		this.serverState = this.serverState.mutable();
		for(let i = 0; i < message.commands.length; i++)
			this.serverState.execute(message.commands[i]);
		this.serverState.immutable();

		// The client state does not have to be updated,
		// as it already contains the accepted changes
		// and possible additional ones that the server has not yet processed.

		// We only keep history entries for meaningfull actions.
		// Otherwise every pixel-movement would cause an entry.
		if(message.description.length > 0) {
			this.history.push({
				user:        this.seneton.me(),
				date:        new Date(),
				description: message.description,
				commands:    message.commands.slice(),
				state:       this.serverState
			});
			this.historyEntryAdded.emit();
		}
	}

	// Message handler for 'decline' message. This messages indicates that the server declined our commands, so we need to roll them back
	private onDecline(): void {
		// drop the declined message from our list
		let declinedMessage = this.sentMessages.shift();
		if(!declinedMessage)
			throw new Error('received "decline" without having sent a message');

		// roll back all our local state changes
		this.seneton.state = this.serverState.mutable();

		// re-emit all events for the declined commands to ensure the view updates to the old state
		for(let i = 0; i < declinedMessage.commands.length; i++)
			this.seneton.emitEvents(declinedMessage.commands[i]);

		// re-apply all further commands that have not yet been accepted or declined
		for(let i = 0; i < this.sentMessages.length; i++) {
			for(let j = 0; j < this.sentMessages[i].commands.length; j++)
				this.seneton.state.execute(this.sentMessages[i].commands[j]);
		}
		this.seneton.stateChanged.emit();
	}

	// Message handler for 'commands' message. This message indicates that another user issued some commands, which may have been processed before our messages
	private async onCommands(msg: { user: Id; commands: Command[]; description: string; }): Promise<void> {
		// remember the current game Uid to check for game changes
		// TODO there has to be a cleaner way for this... maybe checking for a Load command?
		let oldGameUid = this.seneton.state.gameUid;

		// keep a reference to the user in case it will be deleted in this set of commands
		let user = undefined;
		if(this.serverState.users[msg.user] !== undefined)
			user = this.serverState.users[msg.user];

		// progress the server state
		this.serverState = this.serverState.mutable();
		for(let command of msg.commands)
			this.serverState.execute(command);
		this.serverState.immutable();

		// if the user was created in this set of commands, retrieve it again
		if(user === undefined && this.serverState.users[msg.user] !== undefined)
			user = this.serverState.users[msg.user];

		// add a new history entry
		if(msg.description.length > 0) {
			this.history.push({
				user:        user,
				date:        new Date(),
				description: msg.description,
				commands:    msg.commands.slice(),
				state:       this.serverState
			});
			this.historyEntryAdded.emit();
		}

		// reconstruct the client state, by applying all previously sent messages onto the new server state
		this.seneton.state = this.serverState.mutable();
		for(let command of msg.commands)
			this.seneton.emitEvents(command);
		for(let message of this.sentMessages) {
			for(let command of message.commands) {
				this.seneton.state.execute(command);
				this.seneton.emitEvents(command);
			}
		}
		this.seneton.stateChanged.emit();

		// if the commands changed the active game, some resources have to be updated
		if(this.seneton.state.gameUid !== oldGameUid) {
			let game      = this.serverGamesIndex.get(this.seneton.state.gameUid);
			let libraries = await this.loadLibraries(game);
			let resources = await this.loadResources(game);
			this.seneton.setGame(game, libraries, resources);
		}
	}

	private async loadResources(game: GameInfo | undefined): Promise<ResourcesList> {
		if(!game || !game.url)
			return {};

		return await $.ajax({
			url:      game.url + 'resources.json',
			dataType: 'json'
		});
	}

	private async loadInitScript(game: GameInfo | undefined): Promise<string> {
		if(!game || !game.url || !game.initscript)
			return "";

		return await $.ajax({
			url:      game.url + game.initscript,
			dataType: 'text'
		});
	}

	private async loadLibraries(game: GameInfo | undefined): Promise<string[]> {
		if(!game || !game.url)
			return [];

		let libraryFilenames = game.libraries || [];
		let libraries = <string[]> await Promise.all(libraryFilenames.map(async libraryFilename => $.ajax({
			url:      game.url + libraryFilename,
			dataType: 'text'
		})));
		return libraries;
	}
}
