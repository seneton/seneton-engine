module.exports = {
	devtool: 'source-map',
	entry: './app.ts',
	output: {
		path: require('path').join(__dirname, '../../public/'),
		filename: 'bundle.js'
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		loaders: [
			{ test: /\.tsx?$/, loader: 'ts-loader' }
		]
	}
}
