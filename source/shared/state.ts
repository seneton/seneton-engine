import { Uid } from './gameinfo';


export type Id = number;


export interface JsonState {
	gameUid:       Uid;
	nextUserId:    Id;
	nextLayerId:   Id;
	nextGroupId:   Id;
	nextElementId: Id;
	nextDrawingId: Id;
	userIds:       Id[];
	layerIds:      Id[];
	groupIds:      Id[];
	users:         { [index: number]: User };
	layers:        { [index: number]: Layer };
	groups:        { [index: number]: Group };
	elements:      { [index: number]: Element };
	drawingIds:    Id[];
	drawings:      { [index: number]: Drawing };
	turn:          number[];
	turnSequence?: TurnSequence;
}


export interface StartGameCommand {
	type:    'StartGame',
	gameUid: string;
}

export interface LoadCommand {
	type: 'Load',
	data: JsonState
}

export interface CreateUserIdCommand {
	type: 'CreateUserId',
	id:   Id
}

export interface CreateLayerIdCommand {
	type: 'CreateLayerId',
	id:   Id
}

export interface CreateGroupIdCommand {
	type: 'CreateGroupId',
	id:   Id
}

export interface CreateElementIdCommand {
	type: 'CreateElementId',
	id:   Id
}

export interface CreateDrawingIdCommand {
	type: 'CreateDrawingId',
	id:   Id
}

export interface CreateUserCommand {
	type:       'CreateUser',
	id:         Id,
	properties: CreateUserProperties
}

export interface DeleteUserCommand {
	type: 'DeleteUser',
	id:   Id
}

export interface CreateLayerCommand {
	type:       'CreateLayer',
	id:         Id,
	properties: CreateLayerProperties
}

export interface CreateGroupCommand {
	type:       'CreateGroup',
	id:         Id,
	properties: CreateGroupProperties
}

export interface CreateElementCommand {
	type:       'CreateElement',
	id:         Id,
	layerId:    Id,
	properties: Partial<RegularElementProperties>
}

export interface CreateTextElementCommand {
	type:       'CreateTextElement',
	id:         Id,
	layerId:    Id,
	properties: Partial<TextElementProperties>
}

export interface CreateStackCommand {
	type:       'CreateStack',
	id:         Id,
	layerId:    Id,
	properties: Partial<StackElementProperties>
}

export interface DeleteElementCommand {
	type: 'DeleteElement',
	id:   Id
}

export interface CloneElementAtCommand {
	type:      'CloneElementAt',
	id:        Id,
	newId:     Id,
	parent?:   Id,
	position?: Position
}

export interface SetColorCommand {
	type:   'SetColor',
	userId: Id,
	color:  number[]
}

export interface RenameUserCommand {
	type:   'RenameUser',
	userId: Id,
	name:   string
}

export interface PinCommand {
	type: 'Pin',
	id:   Id
}

export interface UnpinCommand {
	type: 'Unpin',
	id:   Id
}

export interface SendToTopCommand {
	type: 'SendToTop',
	id:   Id
}

export interface SendToBottomCommand {
	type: 'SendToBottom',
	id:   Id
}

export interface SendToLayerCommand {
	type:    'SendToLayer',
	id:      Id
	layerId: Id
}

export interface AddToGroupCommand {
	type:    'AddToGroup',
	id:      Id,
	groupId: Id
}

export interface RemoveFromGroupCommand {
	type:    'RemoveFromGroup',
	id:      Id,
	groupId: Id
}

export interface MakeStackableCommand {
	type: 'MakeStackable',
	id:   Id
}

export interface MakeUnstackableCommand {
	type: 'MakeUnstackable',
	id:   Id
}

export interface FlipCommand {
	type: 'Flip',
	id:   Id,
	face: number
}

export interface PeekCommand {
	type:   'Peek',
	id:     Id,
	userId: Id,
	face:   number
}

export interface SelectCommand {
	type:   'Select',
	id:     Id,
	userId: Id
}

export interface DeselectCommand {
	type:   'Deselect',
	id:     Id,
	userId: Id
}

export interface SelectGroupCommand {
	type:   'SelectGroup',
	id:     Id,
	userId: Id
}

export interface RenameCommand {
	type: 'Rename',
	id:   Id,
	name: string
}

export interface MoveToCommand {
	type:     'MoveTo',
	id:       Id,
	x:        number,
	y:        number,
	rotation: number
}

export interface ReorderChildrenCommand {
	type:    'ReorderChildren',
	stackId: Id,
	order:   number[]
}

export interface PopCommand {
	type:    'Pop',
	childId: Id
}

export interface PushCommand {
	type:     'Push',
	id:       Id,
	parentId: Id
}

export interface ClearDrawingsCommand {
	type: 'ClearDrawings'
}

export interface StartFreehandDrawingCommand {
	type:  'StartFreehandDrawing',
	id:    Id,
	x:     number,
	y:     number,
	color: number[]
}

export interface ContinueFreehandDrawingCommand {
	type: 'ContinueFreehandDrawing',
	id:   Id,
	x:    number,
	y:    number
}

export interface SetTurnCommand {
	type: 'SetTurn',
	turn: number[]
}

export interface SetTurnSequenceCommand {
	type:         'SetTurnSequence',
	turnSequence: TurnSequence
}

export interface SetTextCommand {
	type: 'SetText',
	id:   Id,
	text: string
}

export type Command = StartGameCommand | LoadCommand | CreateUserIdCommand | CreateLayerIdCommand | CreateGroupIdCommand | CreateElementIdCommand | CreateDrawingIdCommand | CreateUserCommand | DeleteUserCommand | CreateLayerCommand | CreateGroupCommand | CreateElementCommand | CreateTextElementCommand | CreateStackCommand | DeleteElementCommand | CloneElementAtCommand | SetColorCommand | RenameUserCommand | PinCommand | UnpinCommand | SendToTopCommand | SendToBottomCommand | SendToLayerCommand | AddToGroupCommand | RemoveFromGroupCommand | MakeStackableCommand | MakeUnstackableCommand | FlipCommand | PeekCommand | SelectCommand | DeselectCommand | SelectGroupCommand | RenameCommand | MoveToCommand | ReorderChildrenCommand | PopCommand | PushCommand | ClearDrawingsCommand | StartFreehandDrawingCommand | ContinueFreehandDrawingCommand | SetTurnCommand | SetTurnSequenceCommand | SetTextCommand;


export interface Position {
	x:        number;
	y:        number;
	rotation: number;
}

export interface Layer {
	id:       Id;
	elements: Id[];
}

export interface Group {
	id:       Id;
	name:     string;
	elements: Id[];
}

export type Action = any[];

export interface BaseElementProperties {
	title:            string;
	parent?:          Id;
	groups:           Id[];
	pinned:           boolean;
	position?:        Position;
	pivot:            number[];
	grid:             any[];
	width:            number;
	height:           number;
	tint:             number[];
	shadowRadius:     number;
	shadowColor:      number[];
	shadowOffset:     number[];
	friction?:        boolean;
	defaultAction?:   Action;
	wheelUpAction?:   Action;
	wheelDownAction?: Action;
}

export interface RegularElementProperties extends BaseElementProperties {
	face:       number;
	faces:      (string | Action)[];
	stackable?: boolean | Partial<StackElementProperties>;
}

export interface StackElementProperties extends BaseElementProperties {
	image:        string;
	accepts?:     Id;
	deleteOnLast: boolean;
	background:   number[];
	delta:        Position;
	pushAction?:  Action;
	popAction?:   Action;
}

export interface TextElementProperties extends BaseElementProperties {
	text:             string;
	font:             string;
	size:             number;
	color:            number[];
	activeBackground: number[];
	writeable:        boolean;
	align:            string;
	background:       number[];
	stackable?:       boolean | Partial<StackElementProperties>;
}

interface BaseElement extends BaseElementProperties {
	id:         Id;
	layer:      Id;
	children:   Id[];
	peeks:      { [index: number]: number };
	selections: { [index: number]: boolean };
}

export interface RegularElement extends BaseElement, RegularElementProperties {
	type: 'element';
}

export interface StackElement extends BaseElement, StackElementProperties {
	type: 'stack';
}

export interface TextElement extends BaseElement, TextElementProperties {
	type: 'text';
}

export type Element = RegularElement | StackElement | TextElement;


export interface User {
	id:         Id;
	name:       string;
	selections: Id[];
	color:      number[];
}

export enum DrawingType { Freehand }

export interface Drawing {
	id:     Id;
	type:   DrawingType;
	start:  number[];
	points: number[][];
	color:  number[];
}

export interface CreateLayerProperties {
}

export interface CreateGroupProperties {
	name?: string;
}

export interface CreateUserProperties {
	name?:  string;
	color?: number[];
}

export interface CounterTurnSequence {
	type:     'counter';
	name:     string;
	initial?: number;
	child?:   TurnSequence;
}

export interface OptionsTurnSequence {
	type:     'options';
	initial?: number;
	options:  { name: string; child?: TurnSequence; }[]
}

export type TurnSequence = CounterTurnSequence | OptionsTurnSequence;

export class State {
	gameUid:       Uid  = '';
	nextUserId:    Id   = 1;
	nextLayerId:   Id   = 1;
	nextGroupId:   Id   = 1;
	nextElementId: Id   = 1;
	nextDrawingId: Id   = 1;
	userIds:       Id[] = [];
	layerIds:      Id[] = [];
	groupIds:      Id[] = [];
	users:         { [index: number]: User    } = {};
	layers:        { [index: number]: Layer   } = {};
	groups:        { [index: number]: Group   } = {};
	elements:      { [index: number]: Element } = {};
	drawingIds:    Id[] = [];
	drawings:      { [index: number]: Drawing } = {};
	turn:          number[]     = [];
	turnSequence?: TurnSequence = undefined;

	// keep the ids to all mutable elements as deep freezing all elements is too slow
	mutableElements: Id[] = [];

	static fromJson(json: JsonState): State {
		let state = new State();
		state.gameUid       = json.gameUid;
		state.nextUserId    = json.nextUserId;
		state.nextLayerId   = json.nextLayerId;
		state.nextGroupId   = json.nextGroupId;
		state.nextElementId = json.nextElementId;
		state.nextDrawingId = json.nextDrawingId;
		state.userIds       = json.userIds;
		state.layerIds      = json.layerIds;
		state.groupIds      = json.groupIds;
		state.users         = json.users;
		state.layers        = json.layers;
		state.groups        = json.groups;
		state.elements      = json.elements;
		state.drawingIds    = json.drawingIds;
		state.drawings      = json.drawings;
		state.turn          = json.turn;
		state.turnSequence  = json.turnSequence;

		for(let id of Object.keys(state.elements))
			state.mutableElements.push(+id);

		return state;
	}

	mutable(): State {
		if(!State.isImmutable(this))
			return this;

		let state = new State();
		state.gameUid       = this.gameUid;
		state.nextUserId    = this.nextUserId;
		state.nextLayerId   = this.nextLayerId;
		state.nextGroupId   = this.nextGroupId;
		state.nextElementId = this.nextElementId;
		state.nextDrawingId = this.nextDrawingId;
		state.userIds       = this.userIds;
		state.layerIds      = this.layerIds;
		state.groupIds      = this.groupIds;
		state.users         = this.users;
		state.layers        = this.layers;
		state.groups        = this.groups;
		state.elements      = this.elements;
		state.drawingIds    = this.drawingIds;
		state.drawings      = this.drawings;
		state.turn          = this.turn;
		state.turnSequence  = this.turnSequence;

		state.mutableElements = [];

		return state;
	}

	immutable(): void {
		State.immutableDeep(this.gameUid);
		State.immutableDeep(this.nextUserId);
		State.immutableDeep(this.nextLayerId);
		State.immutableDeep(this.nextGroupId);
		State.immutableDeep(this.nextElementId);
		State.immutableDeep(this.nextDrawingId);
		State.immutableDeep(this.userIds);
		State.immutableDeep(this.layerIds);
		State.immutableDeep(this.groupIds);
		State.immutableDeep(this.users);
		State.immutableDeep(this.layers);
		State.immutableDeep(this.groups);
		State.immutableDeep(this.drawingIds);
		State.immutableDeep(this.drawings);
		State.immutableDeep(this.turn);
		State.immutableDeep(this.turnSequence);

		// deep freezing all elements is too slow. good thing we kept notes on all mutable elements
		State.immutableShallow(this.elements);
		for(let i = 0; i < this.mutableElements.length; i++) {
			let id      = this.mutableElements[i];
			let element = this.elements[id];
			if(element !== undefined)
				State.immutableDeep(element);
		}
		this.mutableElements = [];

		State.immutableShallow(this);
	}

	private assertMutable(): void {
		if(State.isImmutable(this))
			throw new Error('trying to modify an immutable state');
	}

	save(): JsonState {
		let copy = JSON.parse(JSON.stringify(this));
		delete copy.mutableElements;
		return copy;
	}

	private load(data: JsonState): void {
		let copy = JSON.parse(JSON.stringify(data));

		this.gameUid       = data.gameUid;
		this.nextLayerId   = data.nextLayerId;
		this.nextGroupId   = data.nextGroupId;
		this.nextElementId = data.nextElementId;
		this.nextDrawingId = data.nextDrawingId;
		this.layerIds      = data.layerIds;
		this.groupIds      = data.groupIds;
		this.layers        = data.layers;
		this.groups        = data.groups;
		this.elements      = data.elements;
		this.drawingIds    = data.drawingIds;
		this.drawings      = data.drawings;
		this.turn          = data.turn;
		this.turnSequence  = data.turnSequence;

		for(let id in this.elements) {
			let e = this.elements[id];
			e.peeks      = {};
			e.selections = {};
		}

		this.mutableElements = [];
		for(let id of Object.keys(this.elements))
			this.mutableElements.push(+id);
	}

	user(id: Id): User | undefined {
		return this.users[id];
	}

	layer(id: Id): Layer | undefined {
		return this.layers[id];
	}

	group(id: Id): Group | undefined {
		return this.groups[id];
	}

	groupExists(id: Id): boolean {
		return (this.groups[id] !== undefined);
	}

	element(id: Id): Element | undefined {
		return this.elements[id];
	}

	elementExists(id: Id): boolean {
		return (this.elements[id] !== undefined);
	}

	isRoot(id: Id): boolean {
		let element = this.element(id);
		if(!element)
			return false;
		return (element.parent === undefined);
	}

	rootElement(id: Id): Element | undefined {
		let element = this.element(id);
		if(!element)
			return undefined;
		while(element.parent !== undefined) {
			element = this.element(element.parent);
			if(!element)
				return undefined;
		}
		return element;
	}

	isSomeonePeeking(id: Id): boolean {
		let element = this.element(id);
		if(!element)
			return false;
		for(let userId in element.peeks) // check the first enumerable property, as there might be an 'immutable' flag set
			return true;
		return false;
	}

	isPeeking(id: Id, userId: Id): boolean {
		let element = this.element(id);
		if(!element)
			return false;
		return (element.peeks[userId] !== undefined);
	}

	isSelected(id: Id, userId: Id): boolean {
		let element = this.element(id);
		if(!element)
			return false;
		return (element.selections[userId] === true);
	}

	elementSelection(id: Id): Id[] {
		let element = this.element(id);
		if(!element)
			return [];
		let ids: Id[] = [];
		for(let userId in element.selections)
			ids.push(+userId);
		return ids;
	}

	private mutableUser(id: Id): User {
		this.assertMutable();

		let user = this.user(id);
		if(user === undefined)
			throw new Error('user ' + id + ' not defined');

		let copy = State.mutable(user);
		this.users = State.mutable(this.users);
		this.users[id] = copy;
		return copy;
	}

	private mutableLayer(id: Id): Layer {
		this.assertMutable();

		let layer = this.layer(id);
		if(layer === undefined)
			throw new Error('layer ' + id + ' not defined');

		let copy = State.mutable(layer);
		this.layers = State.mutable(this.layers);
		this.layers[id] = copy;
		return copy;
	}

	private mutableGroup(id: Id): Group {
		this.assertMutable();

		let group = this.group(id);
		if(group === undefined)
			throw new Error('group ' + id + ' not defined');

		let copy = State.mutable(group);
		this.groups = State.mutable(this.groups);
		this.groups[id] = copy;
		return copy;
	}

	private mutableElement(id: Id): Element {
		this.assertMutable();

		let element = this.element(id);
		if(element === undefined)
			throw new Error('element ' + id + ' not defined');

		if(State.isImmutable(element))
			this.mutableElements.push(id);

		let copy = State.mutable(element);
		this.elements = State.mutable(this.elements);
		this.elements[id] = copy;
		return copy;
	}

	private startGame(gameUid: string): void {
		this.assertMutable();

		// clear all selections
		for(let userId in this.users)
			this.mutableUser(+userId).selections = [];

		this.gameUid      = gameUid;
		this.layerIds     = [];
		this.layers       = {};
		this.groupIds     = [];
		this.groups       = {};
		this.elements     = {};
		this.turn         = [];
		this.turnSequence = undefined;
	}

	private createUserId(id: Id): void {
		this.assertMutable();

		if(this.nextUserId !== id)
			throw new Error('someone else already got user id ' + id);

		this.nextUserId++;
	}

	private createLayerId(id: Id): void {
		this.assertMutable();

		if(this.nextLayerId !== id)
			throw new Error('someone else already got layer id ' + id);

		this.nextLayerId++;
	}

	private createGroupId(id: Id): void {
		this.assertMutable();

		if(this.nextGroupId !== id)
			throw new Error('someone else already got group id ' + id);

		this.nextGroupId++;
	}

	private createElementId(id: Id): void {
		this.assertMutable();

		if(this.nextElementId !== id)
			throw new Error('someone else already got element id ' + id);

		this.nextElementId++;
	}

	private createDrawingId(id: Id): void {
		this.assertMutable();

		if(this.nextDrawingId !== id)
			throw new Error('someone else already got drawing id ' + id);

		this.nextDrawingId++;
	}

	private createUser(id: Id, properties: CreateUserProperties): void {
		this.assertMutable();

		// set default values
		let user: User = {
			id:         id,
			name:       'Player ' + id,
			color:      [1.0, 1.0, 1.0, 1.0],
			selections: []
		};

		// apply properties
		if(properties.name !== undefined)
			user.name = properties.name;
		if(properties.color !== undefined)
			user.color = properties.color;

		// add the user to the global state
		this.userIds = State.mutable(this.userIds);
		this.userIds.push(user.id);

		// create an index entry for fast retrieval
		this.users = State.mutable(this.users);
		this.users[user.id] = user;
	}

	private deleteUser(id: Id): void {
		this.assertMutable();

		// remove all selections for this user
		let user = this.user(id);
		if(!user)
			throw new Error('user ' + id + ' undefined');
		for(let i = 0; i < user.selections.length; i++) {
			let elementCopy = this.mutableElement(user.selections[i]);
			elementCopy.selections = State.mutable(elementCopy.selections);
			delete elementCopy.selections[id];
		}

		// remove all peeks for this user
		for(let elementId in this.elements) {
			if(this.elements[elementId].peeks[id] !== undefined) {
				let elementCopy = this.mutableElement(+elementId);
				elementCopy.peeks = State.mutable(elementCopy.peeks);
				delete elementCopy.peeks[id];
			}
		}

		// delete from global state
		this.userIds = State.mutable(this.userIds);
		this.userIds.splice(this.userIds.indexOf(id), 1);

		// clear index entry
		this.users = State.mutable(this.users);
		delete this.users[id];
	}

	private createLayer(id: Id, properties: CreateLayerProperties): void {
		this.assertMutable();

		// set default values
		let layer: Layer = {
			id:       id,
			elements: []
		};

		// we don't have any properties to apply, yet

		// add the element to the global state
		this.layerIds = State.mutable(this.layerIds);
		this.layerIds.push(layer.id);

		// create an index entry for fast retrieval
		this.layers = State.mutable(this.layers);
		this.layers[layer.id] = layer;
	}

	private createGroup(id: Id, properties: CreateGroupProperties): void {
		this.assertMutable();

		// set default values
		let group: Group = {
			id:       id,
			name:     '',
			elements: []
		};

		// apply properties
		if(properties.name !== undefined)
			group.name = properties.name;

		// add the element to the global state
		this.groupIds = State.mutable(this.groupIds);
		this.groupIds.push(group.id);

		// create an index entry for fast retrieval
		this.groups = State.mutable(this.groups);
		this.groups[group.id] = group;
	}

	private createBaseElement(id: Id, layerId: Id, properties: Partial<BaseElementProperties>): BaseElement {
		this.assertMutable();

		// set default values
		let element: BaseElement = {
			id:           id,
			title:        '',
			parent:       undefined,
			children:     [],
			layer:        layerId,
			groups:       [],
			pinned:       false,
			position:     undefined,
			pivot:        [0, 0],
			grid:         [],
			width:        0,
			height:       0,
			peeks:        {},
			selections:   {},
			tint:         [1.0, 1.0, 1.0, 1.0],
			shadowRadius: 5.0,
			shadowColor:  [0.0, 0.0, 0.0, 0.25],
			shadowOffset: [0.0, 0.0]
		};

		// apply properties
		if(properties.title !== undefined)
			element.title = properties.title;
		if(properties.position !== undefined)
			element.position = properties.position;
		if(properties.pivot !== undefined)
			element.pivot = properties.pivot;
		if(properties.width !== undefined)
			element.width = properties.width;
		if(properties.height !== undefined)
			element.height = properties.height;
		if(properties.parent !== undefined)
			element.parent = properties.parent;
		if(properties.groups !== undefined)
			element.groups = properties.groups.slice();
		if(properties.pinned !== undefined)
			element.pinned = properties.pinned;
		if(properties.grid !== undefined)
			element.grid = properties.grid;
		if(properties.shadowRadius !== undefined)
			element.shadowRadius = properties.shadowRadius;
		if(properties.shadowColor !== undefined)
			element.shadowColor = properties.shadowColor;
		if(properties.shadowOffset !== undefined)
			element.shadowOffset = properties.shadowOffset;
		if(properties.tint !== undefined)
			element.tint = properties.tint;
		if(properties.friction !== undefined)
			element.friction = properties.friction;
		if(properties.defaultAction !== undefined)
			element.defaultAction = properties.defaultAction;
		if(properties.wheelUpAction !== undefined)
			element.wheelUpAction = properties.wheelUpAction;
		if(properties.wheelDownAction !== undefined)
			element.wheelDownAction = properties.wheelDownAction;

		// fix position
		if(element.position === undefined) {
			if(element.parent !== undefined) {
				let parent = this.element(element.parent);
				if(!parent)
					throw new Error('element ' + element.parent + ' does not exist');

				if(parent.type === 'stack')
					element.position = undefined;
				else
					element.position = { x: 0, y: 0, rotation: 0 };
			}
			else
				element.position = { x: 0, y: 0, rotation: 0 };
		}

		// add the element to the global state
		if(element.parent !== undefined) {
			let parentCopy = this.mutableElement(element.parent);
			parentCopy.children = State.mutable(parentCopy.children);
			parentCopy.children.push(id);
		}
		else {
			let layerCopy = this.mutableLayer(element.layer);
			layerCopy.elements = State.mutable(layerCopy.elements);
			layerCopy.elements.push(id);
		}

		// add the element to its groups
		for(let groupId of element.groups) {
			let groupCopy = this.mutableGroup(groupId);
			groupCopy.elements = State.mutable(groupCopy.elements);
			groupCopy.elements.push(id);
		}

		return element;
	}

	private createElement(id: Id, layerId: Id, properties: Partial<RegularElementProperties>): void {
		this.assertMutable();

		let baseElement = this.createBaseElement(id, layerId, properties);

		// set element-specific default values
		let element: RegularElement = {
			...baseElement,
			type:  'element',
			face:  0,
			faces: []
		};

		// apply element-specific properties
		if(properties.face !== undefined)
			element.face = properties.face;
		if(properties.faces !== undefined)
			element.faces = properties.faces.slice();
		if(properties.stackable !== undefined)
			element.stackable = JSON.parse(JSON.stringify(properties.stackable));

		// create an index entry for fast retrieval
		this.elements = State.mutable(this.elements);
		this.elements[id] = element;
		this.mutableElements.push(id);
	}

	private createTextElement(id: Id, layerId: Id, properties: Partial<TextElementProperties>): void {
		this.assertMutable();

		let baseElement = this.createBaseElement(id, layerId, properties);

		// set text-specific default values
		let element: TextElement = {
			...baseElement,
			type:             'text',
			text:             '',
			font:             'Arial',
			size:             10,
			color:            [0.0, 0.0, 0.0, 1.0],
			activeBackground: [1.0, 0.0, 0.0, 1.0],
			writeable:        true,
			align:            'center',
			background:       [0.99, 0.99, 0.99, 1.0]
		};

		// apply text-specific properties
		if(properties.text !== undefined)
			element.text = properties.text;
		if(properties.font !== undefined)
			element.font = properties.font;
		if(properties.size !== undefined)
			element.size = properties.size;
		if(properties.color !== undefined)
			element.color = properties.color;
		if(properties.activeBackground !== undefined)
			element.activeBackground = properties.activeBackground;
		if(properties.writeable !== undefined)
			element.writeable = properties.writeable;
		if(properties.align !== undefined)
			element.align = properties.align;
		if(properties.background !== undefined)
			element.background = properties.background;

		// create an index entry for fast retrieval
		this.elements = State.mutable(this.elements);
		this.elements[id] = element;
		this.mutableElements.push(id);
	}

	private createStack(id: Id, layerId: Id, properties: Partial<StackElementProperties>): void {
		this.assertMutable();

		let baseElement = this.createBaseElement(id, layerId, properties);

		// set stack-specific default values
		let element: StackElement = {
			...baseElement,
			type:         'stack',
			image:        '',
			background:   [0.0, 0.0, 0.0, 0.8],
			deleteOnLast: false,
			delta:        { x: 0, y: 0, rotation: 0 }
		};

		// apply stack-specific properties
		if(properties.image !== undefined)
			element.image = properties.image;
		if(properties.accepts !== undefined)
			element.accepts = properties.accepts;
		if(properties.background !== undefined)
			element.background = properties.background;
		else if(element.deleteOnLast)
			element.background = [0.0, 0.0, 0.0, 0.1];
		if(properties.deleteOnLast !== undefined)
			element.deleteOnLast = properties.deleteOnLast;
		if(properties.delta !== undefined)
			element.delta = properties.delta;
		if(properties.pushAction !== undefined)
			element.pushAction = properties.pushAction;
		if(properties.popAction !== undefined)
			element.popAction = properties.popAction;

		// create an index entry for fast retrieval
		this.elements = State.mutable(this.elements);
		this.elements[id] = element;
		this.mutableElements.push(id);
	}

	private deleteElement(id: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');
		if(element.parent !== undefined)
			throw new Error('only root elements can be deleted');
		if(element.children.length > 0)
			throw new Error('child elements have to be deleted first');

		// clear the selection
		for(let userId in element.selections) {
			let userCopy = this.mutableUser(+userId);
			userCopy.selections = State.mutable(userCopy.selections);
			userCopy.selections.splice(userCopy.selections.indexOf(id), 1);
		}

		// remove it from its layer
		let layerCopy      = this.mutableLayer(element.layer);
		layerCopy.elements = State.mutable(layerCopy.elements);
		layerCopy.elements.splice(layerCopy.elements.indexOf(id), 1);

		// remove it from its groups
		for(let groupId of element.groups) {
			let groupCopy      = this.mutableGroup(groupId);
			groupCopy.elements = State.mutable(groupCopy.elements);
			groupCopy.elements.splice(groupCopy.elements.indexOf(id), 1);
		}

		// clear index entry
		this.elements = State.mutable(this.elements);
		delete this.elements[element.id];
	}

	private cloneElementAt(id: Id, newId: Id, parentId?: Id, position?: Position): void {
		this.assertMutable();

		let element = this.element(id);

		let clone: Element = State.isImmutable(element) ? State.mutable(element) : JSON.parse(JSON.stringify(element));
		clone.id         = newId;
		clone.children   = [];
		clone.peeks      = {};
		clone.selections = {};
		clone.parent     = parentId;
		clone.position   = JSON.parse(JSON.stringify(position));

		// add the element to the global state
		if(clone.parent !== undefined) {
			let parentCopy = this.mutableElement(clone.parent);
			parentCopy.children = State.mutable(parentCopy.children);
			parentCopy.children.push(id);
		}
		else {
			let layerCopy = this.mutableLayer(clone.layer);
			layerCopy.elements = State.mutable(layerCopy.elements);
			layerCopy.elements.push(id);
		}

		// add the element to its groups
		for(let groupId of clone.groups) {
			let groupCopy = this.mutableGroup(groupId);
			groupCopy.elements = State.mutable(groupCopy.elements);
			groupCopy.elements.push(id);
		}

		// create an index entry for fast retrieval
		this.elements = State.mutable(this.elements);
		this.elements[id] = clone;
		this.mutableElements.push(id);
	}

	private renameUser(userId: Id, name: string): void {
		this.assertMutable();

		this.mutableUser(userId).name = name;
	}

	private setColor(userId: Id, color: number[]): void {
		this.assertMutable();

		this.mutableUser(userId).color = color;
	}

	private pin(id: Id): void {
		this.assertMutable();

		let elementCopy = this.mutableElement(id);
		elementCopy.pinned = true;

		// remove the element from all selections
		for(let userId in elementCopy.selections) {
			let userCopy = this.mutableUser(+userId);
			userCopy.selections = State.mutable(userCopy.selections);
			userCopy.selections.splice(userCopy.selections.indexOf(id), 1);
		}
		elementCopy.selections = {};
	}

	private unpin(id: Id): void {
		this.assertMutable();

		this.mutableElement(id).pinned = false;
	}

	private sendToTop(id: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined) {
			let parentCopy = this.mutableElement(element.parent);
			parentCopy.children = State.mutable(parentCopy.children);
			parentCopy.children.splice(parentCopy.children.indexOf(id), 1);
			parentCopy.children.push(id);
		}
		else {
			let layerCopy = this.mutableLayer(element.layer);
			layerCopy.elements = State.mutable(layerCopy.elements);
			layerCopy.elements.splice(layerCopy.elements.indexOf(id), 1);
			layerCopy.elements.push(id);
		}
	}

	private sendToBottom(id: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined) {
			let parentCopy = this.mutableElement(element.parent);
			parentCopy.children = State.mutable(parentCopy.children);
			parentCopy.children.splice(parentCopy.children.indexOf(id), 1);
			parentCopy.children.unshift(id);
		}
		else {
			let layerCopy = this.mutableLayer(element.layer);
			layerCopy.elements = State.mutable(layerCopy.elements);
			layerCopy.elements.splice(layerCopy.elements.indexOf(id), 1);
			layerCopy.elements.unshift(id);
		}
	}

	private sendToLayer(id: Id, layerId: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.parent !== undefined)
			throw new Error('only root elements can be sent to other layers');

		if(element.layer === layerId)
			return;

		let oldLayerCopy = this.mutableLayer(element.layer);
		oldLayerCopy.elements = State.mutable(oldLayerCopy.elements);
		oldLayerCopy.elements.splice(oldLayerCopy.elements.indexOf(id), 1);

		let newLayerCopy = this.mutableLayer(layerId);
		newLayerCopy.elements = State.mutable(newLayerCopy.elements);
		newLayerCopy.elements.push(id);

		this.mutableElement(id).layer = layerId;
	}

	private addToGroup(id: Id, groupId: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.groups.indexOf(groupId) != -1)
			return;

		let elementCopy = this.mutableElement(id);
		elementCopy.groups = State.mutable(elementCopy.groups);
		elementCopy.groups.push(groupId);

		let groupCopy = this.mutableGroup(groupId);
		groupCopy.elements = State.mutable(groupCopy.elements);
		groupCopy.elements.push(id);
	}

	private removeFromGroup(id: Id, groupId: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.groups.indexOf(groupId) == -1)
			return;

		let elementCopy = this.mutableElement(id);
		elementCopy.groups = State.mutable(elementCopy.groups);
		elementCopy.groups.splice(elementCopy.groups.indexOf(groupId), 1);

		let groupCopy = this.mutableGroup(groupId);
		groupCopy.elements = State.mutable(groupCopy.elements);
		groupCopy.elements.splice(groupCopy.elements.indexOf(id), 1);
	}

	private makeStackable(id: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element') {
			if(!element.stackable)
				(<RegularElement>this.mutableElement(id)).stackable = true;
		}
		else if(element.type === 'text') {
			if(!element.stackable)
				(<TextElement>this.mutableElement(id)).stackable = true;
		}
		else
			throw new Error('only basic and text elements are stackable');
	}

	private makeUnstackable(id: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type === 'element')
			delete (<RegularElement>this.mutableElement(id)).stackable;
		else if(element.type === 'text')
			delete (<TextElement>this.mutableElement(id)).stackable;
		else
			throw new Error('only basic and text elements are stackable');
	}

	private flip(id: Id, face: number): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type !== 'element')
			throw new Error('only basic elements are flippable');

		let elementCopy = <RegularElement>this.mutableElement(id);

		if(elementCopy.faces && face >= elementCopy.faces.length)
			throw new Error('element has no face ' + face);

		elementCopy.face  = face;
		elementCopy.peeks = {};
	}

	private peek(id: Id, userId: Id, face: number): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.type !== 'element')
			throw new Error('only basic elements can be peeked');

		let elementCopy = <RegularElement>this.mutableElement(id);

		if(elementCopy.faces && face >= elementCopy.faces.length)
			throw new Error('element has no face ' + face);

		elementCopy.peeks = State.mutable(elementCopy.peeks);

		if(face == elementCopy.face)
			delete elementCopy.peeks[userId];
		else
			elementCopy.peeks[userId] = face;
	}

	private select(id: Id, userId: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.selections[userId])
			return;

		let elementCopy = this.mutableElement(id);
		elementCopy.selections = State.mutable(elementCopy.selections);
		elementCopy.selections[userId] = true;

		let userCopy = this.mutableUser(userId);
		userCopy.selections = State.mutable(userCopy.selections);
		userCopy.selections.push(id);
	}

	private deselect(id: Id, userId: Id): void {
		this.assertMutable();

		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		if(element.selections[userId] === undefined)
			return;

		let elementCopy = this.mutableElement(id);
		elementCopy.selections = State.mutable(elementCopy.selections);
		delete elementCopy.selections[userId];

		let userCopy = this.mutableUser(userId);
		userCopy.selections = State.mutable(userCopy.selections);
		userCopy.selections.splice(userCopy.selections.indexOf(id), 1);
	}

	private selectGroup(groupId: Id, userId: Id): void {
		this.assertMutable();

		let group = this.group(groupId);
		if(!group)
			throw new Error('group ' + groupId + ' does not exist');

		for(let id of group.elements)
			this.select(id, userId);
	}

	private rename(id: Id, name: string): void {
		this.assertMutable();

		this.mutableElement(id).title = name;
	}

	private moveTo(id: Id, x: number, y: number, rotation: number): void {
		this.assertMutable();

		let elementCopy = this.mutableElement(id);
		if(elementCopy.parent !== undefined) {
			let parent = this.element(elementCopy.parent);
			if(!parent)
				throw new Error('parent ' + elementCopy.parent + ' does not exist');
			if(parent.type === 'stack')
				throw new Error('stack children cannot be moved');
		}

		elementCopy.position = <Position>State.mutable(elementCopy.position);
		elementCopy.position.x        = x;
		elementCopy.position.y        = y;
		elementCopy.position.rotation = rotation;
	}

	private reorderChildren(stackId: Id, order: number[]): void {
		this.assertMutable();

		let stackCopy = this.mutableElement(stackId);
		if(order.length !== stackCopy.children.length)
			throw new Error('order has bad length ' + order.length + ' instead of ' + stackCopy.children.length);

		let children: Id[] = [];
		for(let i = 0; i < order.length; i++)
			children.push(stackCopy.children[order[i]]);
		stackCopy.children = children;
	}

	/**
	 * Calculates the absolute position of an element on the table.
	 * @param id Element ID. Root and child elements are both allowed.
	 */
	absolutePosition(id: Id): Position {
		let element = this.element(id);
		if(!element)
			throw new Error('element ' + id + ' does not exist');

		let position = [0.0, 0.0];
		let rotation = 0.0;
		if(element.position !== undefined) {
			position = [element.position.x, element.position.y];
			rotation = element.position.rotation;
		}

		while(element.parent !== undefined) {
			let parent = this.element(element.parent);
			if(!parent)
				throw new Error('element ' + element.parent + ' does not exist');
	
			if(parent.type === 'stack') {
				let index = parent.children.indexOf(id);
				if(index === -1)
					throw new Error('element ' + id + ' does not appear as a child of its parent ' + parent.id);

				let localPos = [0.0, 0.0];
				let localRot = 0.0;
				for(let i = 0; i < index; i++) {
					localRot += parent.delta.rotation;
					localPos = [
						localPos[0] + parent.delta.x * Math.cos(localRot) - parent.delta.y * Math.sin(localRot),
						localPos[1] + parent.delta.x * Math.sin(localRot) + parent.delta.y * Math.cos(localRot)
					];
				}

				position = [
					localPos[0] + position[0] * Math.cos(localRot) - position[1] * Math.sin(localRot),
					localPos[1] + position[0] * Math.sin(localRot) + position[1] * Math.cos(localRot)
				];
				rotation += localRot;
			}

			if(parent.position === undefined) {
				position = [0.0, 0.0];
				rotation = 0.0;
			}
			else {
				position = [
					parent.position.x + position[0] * Math.cos(parent.position.rotation) - position[1] * Math.sin(parent.position.rotation),
					parent.position.y + position[0] * Math.sin(parent.position.rotation) + position[1] * Math.cos(parent.position.rotation)
				];
				rotation += parent.position.rotation;
			}

			element = parent;
		}

		return { x: position[0], y: position[1], rotation: rotation };
	}

	/**
	 * Calculates the relative position of one element to another, as if the first was a relative child of the latter.
	 * @param id           The potential child element.
	 * @param relativeToId The potential parent element.
	 */
	relativePosition(position: Position, relativeToId: Id): Position {
		let b = this.absolutePosition(relativeToId);

		let delta = [position.x - b.x, position.y - b.y];
		delta = [
			delta[0] * Math.cos(-b.rotation) - delta[1] * Math.sin(-b.rotation),
			delta[0] * Math.sin(-b.rotation) + delta[1] * Math.cos(-b.rotation)
		];
		return { x: delta[0], y: delta[1], rotation: position.rotation - b.rotation };
	}

	/**
	 * Removes a child element from its parent and places it at the same position on the table.
	 * @param childId Element to be popped. Must be a child of someone.
	 */
	private pop(childId: Id): void {
		this.assertMutable();

		let elementCopy = this.mutableElement(childId);
		if(elementCopy.parent === undefined)
			throw new Error('only pushed elements can be popped');

		// calculate the new position
		let position = this.absolutePosition(childId);

		// remove from parent
		let parentCopy = this.mutableElement(elementCopy.parent);
		parentCopy.children = State.mutable(parentCopy.children);
		parentCopy.children.splice(parentCopy.children.indexOf(childId), 1);

		// add to layer
		let root = this.rootElement(parentCopy.id);
		elementCopy.parent   = undefined;
		elementCopy.position = position;
		let layerCopy = this.mutableLayer(elementCopy.layer);
		layerCopy.elements = State.mutable(layerCopy.elements);
		layerCopy.elements.push(childId);
	}

	private push(id: Id, parentId: Id): void {
		this.assertMutable();

		let childCopy  = this.mutableElement(id);
		let parentCopy = this.mutableElement(parentId);

		if(childCopy.parent !== undefined)
			throw new Error('only root elements can be pushed');

		// calculate the new position
		let position = undefined;
		if(parentCopy.type !== 'stack')
			position = this.relativePosition(this.absolutePosition(id), parentId);

		// adjust old parent state
		let layerCopy = this.mutableLayer(childCopy.layer);
		layerCopy.elements = State.mutable(layerCopy.elements);
		layerCopy.elements.splice(layerCopy.elements.indexOf(id), 1);

		// adjust new parent state
		parentCopy.children = State.mutable(parentCopy.children);
		parentCopy.children.push(id);

		// adjust child state
		childCopy.parent   = parentId;
		childCopy.position = position;
	}

	private clearDrawings(): void {
		this.assertMutable();

		this.drawingIds = [];
		this.drawings   = {};
	}

	drawing(id: Id): Drawing {
		let d = this.drawings[id];
		if(d === undefined)
			throw new Error('drawing ' + id + ' undefined');
		return d;
	}

	drawingExists(id: Id): boolean {
		return (this.drawings[id] !== undefined);
	}

	private mutableDrawing(id: Id) {
		this.assertMutable();

		let drawing = this.drawing(id);

		drawing = State.mutable(drawing);
		this.drawings = State.mutable(this.drawings);
		this.drawings[id] = drawing;
		return drawing;
	}

	private startFreehandDrawing(id: Id, x: number, y: number, color: number[]): void {
		this.assertMutable();

		this.drawings = State.mutable(this.drawings);
		this.drawings[id] = {
			id:     id,
			type:   DrawingType.Freehand,
			start:  [x, y],
			points: [],
			color:  color
		};

		this.drawingIds = State.mutable(this.drawingIds);
		this.drawingIds.push(id);
	}

	private continueFreehandDrawing(id: Id, x: number, y: number): void {
		this.assertMutable();

		let drawingCopy = this.mutableDrawing(id);

		drawingCopy.points = State.mutable(drawingCopy.points);
		drawingCopy.points.push([x, y]);
	}

	private setTurn(turn: number[]): void {
		this.assertMutable();

		this.turn = turn;
	}

	private setTurnSequence(turnSequence: TurnSequence): void {
		this.assertMutable();

		this.turnSequence = turnSequence;
	}

	private setText(id: Id, text: string): void {
		this.assertMutable();

		let elementCopy = this.mutableElement(id);
		if(elementCopy.type !== 'text')
			throw new Error('only text elements have text values');

		elementCopy.text = text;
	}

	execute(command: Command): void {
		switch(command.type) {
			case 'StartGame':               this.startGame              (command.gameUid);                                             break;
			case 'Load':                    this.load                   (command.data);                                                break;
			case 'CreateUserId':            this.createUserId           (command.id);                                                  break;
			case 'CreateLayerId':           this.createLayerId          (command.id);                                                  break;
			case 'CreateGroupId':           this.createGroupId          (command.id);                                                  break;
			case 'CreateElementId':         this.createElementId        (command.id);                                                  break;
			case 'CreateDrawingId':         this.createDrawingId        (command.id);                                                  break;
			case 'CreateUser':              this.createUser             (command.id, command.properties);                              break;
			case 'DeleteUser':              this.deleteUser             (command.id);                                                  break;
			case 'CreateLayer':             this.createLayer            (command.id, command.properties);                              break;
			case 'CreateGroup':             this.createGroup            (command.id, command.properties);                              break;
			case 'CreateElement':           this.createElement          (command.id, command.layerId, command.properties);             break;
			case 'CreateTextElement':       this.createTextElement      (command.id, command.layerId, command.properties);             break;
			case 'CreateStack':             this.createStack            (command.id, command.layerId, command.properties);             break;
			case 'DeleteElement':           this.deleteElement          (command.id);                                                  break;
			case 'CloneElementAt':          this.cloneElementAt         (command.id, command.newId, command.parent, command.position); break;
			case 'SetColor':                this.setColor               (command.userId, command.color);                               break;
			case 'RenameUser':              this.renameUser             (command.userId, command.name);                                break;
			case 'Pin':                     this.pin                    (command.id);                                                  break;
			case 'Unpin':                   this.unpin                  (command.id);                                                  break;
			case 'SendToTop':               this.sendToTop              (command.id);                                                  break;
			case 'SendToBottom':            this.sendToBottom           (command.id);                                                  break;
			case 'SendToLayer':             this.sendToLayer            (command.id, command.layerId);                                 break;
			case 'AddToGroup':              this.addToGroup             (command.id, command.groupId);                                 break;
			case 'RemoveFromGroup':         this.removeFromGroup        (command.id, command.groupId);                                 break;
			case 'MakeStackable':           this.makeStackable          (command.id);                                                  break;
			case 'MakeUnstackable':         this.makeUnstackable        (command.id);                                                  break;
			case 'Flip':                    this.flip                   (command.id, command.face);                                    break;
			case 'Peek':                    this.peek                   (command.id, command.userId, command.face);                    break;
			case 'Select':                  this.select                 (command.id, command.userId);                                  break;
			case 'Deselect':                this.deselect               (command.id, command.userId);                                  break;
			case 'SelectGroup':             this.selectGroup            (command.id, command.userId);                                  break;
			case 'Rename':                  this.rename                 (command.id, command.name);                                    break;
			case 'MoveTo':                  this.moveTo                 (command.id, command.x, command.y, command.rotation);          break;
			case 'ReorderChildren':         this.reorderChildren        (command.stackId, command.order);                              break;
			case 'Pop':                     this.pop                    (command.childId);                                             break;
			case 'Push':                    this.push                   (command.id, command.parentId);                                break;
			case 'ClearDrawings':           this.clearDrawings          ();                                                            break;
			case 'StartFreehandDrawing':    this.startFreehandDrawing   (command.id, command.x, command.y, command.color);             break;
			case 'ContinueFreehandDrawing': this.continueFreehandDrawing(command.id, command.x, command.y);                            break;
			case 'SetTurn':                 this.setTurn                (command.turn);                                                break;
			case 'SetTurnSequence':         this.setTurnSequence        (command.turnSequence);                                        break;
			case 'SetText':                 this.setText                (command.id, command.text);                                    break;

			default:
				throw new Error('unknown command: ' + JSON.stringify(command));
		}
	}

	private static mutable<T>(o: T): T {
		if(typeof(o) !== 'object' || o === null || o === undefined)
			return o;

		if(!State.isImmutable(o))
			return o;

		if(Array.isArray(o))
			return <T>(<any>o).slice();

		// create a mutable, shallow copy
		let mo: { [index:string]: any } = {};
		for(let key in o)
			mo[key] = (<any>o)[key];
		return <T><any>mo;
	}

	private static immutableDeep(o: any): void {
		if(State.isImmutable(o))
			return;

		if(Array.isArray(o)) {
			for(let i = 0; i < o.length; i++)
				State.immutableDeep(o[i]);
		}
		else {
			for(let key in o)
				State.immutableDeep(o[key]);
		}
		Object.defineProperty(o, 'immutable', { value: true });
	}

	private static immutableShallow(o: any): void {
		if(State.isImmutable(o))
			return;

		Object.defineProperty(o, 'immutable', { value: true });
	}

	private static isImmutable(o: any): boolean {
		return (typeof(o) !== 'object' || o === null || o === undefined || o.immutable === true);
	}
}
