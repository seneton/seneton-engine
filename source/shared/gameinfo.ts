export type Uid = string;


export interface GameConfig {
	uid:        Uid;
	name:       string;
	initscript: string;
	libraries?: string[];
	thumbnail?: string;
	help?:      string;

}


export interface GameInfo extends GameConfig {
	// this will be set by the server
	url: string;
}
