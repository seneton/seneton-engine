type Id = number;

interface SenetonPosition {
	x:        number;
	y:        number;
	rotation: number;
}

interface Layer {
	id:       Id;
	elements: Id[];
}

interface Group {
	id:       Id;
	name:     string;
	elements: Id[];
}

type Action = any[];

interface BaseElementProperties {
	title:            string;
	parent?:          Id;
	groups:           Id[];
	pinned:           boolean;
	position?:        SenetonPosition;
	pivot:            number[];
	grid:             any[];
	width:            number;
	height:           number;
	tint:             number[];
	shadowRadius:     number;
	shadowColor:      number[];
	shadowOffset:     number[];
	friction?:        boolean;
	defaultAction?:   Action;
	wheelUpAction?:   Action;
	wheelDownAction?: Action;
}

interface RegularElementProperties extends BaseElementProperties {
	face:       number;
	faces:      (string | Action)[];
	stackable?: boolean | Partial<StackElementProperties>;
}

interface StackElementProperties extends BaseElementProperties {
	image:        string;
	accepts?:     Id;
	deleteOnLast: boolean;
	background:   number[];
	delta:        SenetonPosition;
	pushAction?:  Action;
	popAction?:   Action;
}

interface TextElementProperties extends BaseElementProperties {
	text:             string;
	font:             string;
	size:             number;
	color:            number[];
	activeBackground: number[];
	writeable:        boolean;
	align:            string;
	background:       number[];
	stackable?:       boolean | Partial<StackElementProperties>;
}

interface BaseElement extends BaseElementProperties {
	id:         Id;
	layer:      Id;
	children:   Id[];
	peeks:      { [index: number]: number };
	selections: { [index: number]: boolean };
}

interface RegularElement extends BaseElement, RegularElementProperties {
	type: 'element';
}

interface StackElement extends BaseElement, StackElementProperties {
	type: 'stack';
}

interface TextElement extends BaseElement, TextElementProperties {
	type: 'text';
}

type SenetonElement = RegularElement | StackElement | TextElement;

interface CreateLayerProperties {
}

interface CreateGroupProperties {
	name?: string;
}

interface CreateBaseElementProperties extends Partial<BaseElementProperties> {
	id?:       Id;
	layer?:    Id;
	x?:        number;
	y?:        number;
	left?:     number;
	right?:    number;
	top?:      number;
	bottom?:   number;
	rotation?: number;
	relTo?:    Id;
}

interface CreateRegularElementProperties extends CreateBaseElementProperties, Partial<RegularElementProperties> {
}

interface CreateStackElementProperties extends CreateBaseElementProperties, Partial<StackElementProperties> {
}

interface CreateTextElementProperties extends CreateBaseElementProperties, Partial<TextElementProperties> {
}

interface User {
	id:         Id;
	name:       string;
	selections: Id[];
	color:      number[];
}

declare enum DrawingType { Freehand }

interface Drawing {
	id:     Id;
	type:   DrawingType;
	start:  number[];
	points: number[][];
	color:  number[];
}

interface CounterTurnSequence {
	type:     'counter';
	name:     string;
	initial?: number;
	child?:   TurnSequence;
}

interface OptionsTurnSequence {
	type:     'options';
	initial?: number;
	options:  { name: string; child?: TurnSequence; }[]
}

type TurnSequence = CounterTurnSequence | OptionsTurnSequence;

interface LayerProperties extends CreateLayerProperties {
	id?: Id;
}

interface GroupProperties extends CreateGroupProperties {
	id?: Id;
}

type Uid = string;

interface GameConfig {
	uid:        Uid;
	name:       string;
	initscript: string;
	libraries?: string[];
	thumbnail?: string;
	help?:      string;

	// this will be set by the server
	url?:       string;
}

interface Bounds {
	x:      number;
	y:      number;
	width:  number;
	height: number;
	top:    number;
	left:   number;
	right:  number;
	bottom: number;
}

interface SenetonContext {
	currentGame(): GameConfig | undefined;
	thumbnailUrl(uid: Uid): string | undefined;
	helpUrl(uid: Uid): string | undefined;
	resourceUrl(uid: Uid, path: string): string | undefined;
	me(): User | undefined;
	user(id: Id): User | undefined;
	layer(id: Id): Layer | undefined;
	group(id: Id): Group | undefined;
	element(id: Id): SenetonElement | undefined;
	drawing(id: Id): Drawing;
	groupExists(id: Id): boolean;
	elementExists(id: Id): boolean;
	drawingExists(id: Id): boolean;
	isRoot(id: Id): boolean;
	rootElement(id: Id): SenetonElement | undefined;
	skipStackChildren(id: Id): SenetonElement | undefined;
	parents(id: Id): Id[];
	isOnStack(id: Id): boolean;
	amIPeeking(id: Id): boolean;
	isSomeonePeeking(id: Id): boolean;
	selection(): Id[];
	isSelectedByMe(id: Id): boolean;
	elementSelection(id: Id): Id[];
	elementsOnTop(elementId: Id): Id[];
	getElementOrder(ids: Id[]): Id[];
	currentGameUid(): string;
	createUserId(): Id;
	createLayerId(): Id;
	createGroupId(): Id;
	createElementId(): Id;
	createDrawingId(): Id;
	createLayer(properties?: LayerProperties): Id;
	createGroup(properties?: GroupProperties): Id;
	createImplicitGroup(name: string): Id;
	createTextElement(properties: CreateTextElementProperties): Id;
	createElement(properties: CreateRegularElementProperties): Id;
	createStack(properties: CreateStackElementProperties): Id;
	deleteElement(id: Id): void;
	cloneElement(id: Id): void;
	cloneElementAt(id: Id, parent?: Id, position?: SenetonPosition): Id;
	setColor(color: number[]): void;
	renameMyself(name: string): void;
	togglePin(id: Id): void;
	pin(id: Id): void;
	unpin(id: Id): void;
	sendToTop(id: Id): void;
	sendToBottom(id: Id): void;
	sendToLayer(id: Id, layerId: Id): void;
	addToGroup(id: Id, groupId: Id): void;
	removeFromGroup(id: Id, groupId: Id): void;
	makeStackable(id: Id): void;
	makeUnstackable(id: Id): void;
	flipRandomAnimated(...ids: Id[]): void;
	flipRandom(id: Id): void;
	flip(id: Id): void;
	flipDown(id: Id): void;
	flipTo(face: number, id: Id): void;
	peek(id: Id): void;
	unpeek(id: Id): void;
	togglePeek(id: Id): void;
	select(id: Id): void;
	deselect(id: Id): void;
	deselectAll(): void;
	selectGroup(id: Id): void;
	rename(id: Id, name: string): void;
	moveTo(x: number, y: number, rotation: number, id: Id): void;
	moveBy(id: Id, x: number, y: number, rotation: number): void;
	rotateBy(rotation: number, id: Id): void;
	snapToGrid(id: Id): void;
	moveRightOf(id: Id, leftId: Id): void;
	bounds(id: Id): Bounds;
	reverseStack(id: Id): void;
	shuffleStack(id: Id): void;
	rotateStackOrder(id: Id, delta: number): void;
	popAll(stackId: Id): Id[];
	popNToRight(stackId: Id, count: number): Id[];
	pop(childId: Id): void;
	popFromStack(childId: Id): void;
	// appends given string to 'text' of all selected elements of type text
	appendText(text: string): void;
	// removes the last character from 'text' of all seclted elements of type string
	removeChar(): void;
	canPush(childId: Id, targetId: Id): boolean;
	push(childId: Id, targetId: Id): void;
	clearDrawings(): void;
	startFreehandDrawing(id: Id, x: number, y: number, color: number[]): void;
	continueFreehandDrawing(id: Id, x: number, y: number): void;
	setTurn(turn: number[]): void;
	setTurnSequence(turnSequence: TurnSequence): void;
	selectedWriteableElements(): SenetonElement[];
	executeAction(action: Action, ...args: any[]): any;
}

declare const Seneton: SenetonContext;

declare function rad(x: number): number;
declare function grad(x: number): number;
