#!/bin/sh

for i in */
do
	pushd "$i" > /dev/null
	echo "$i"
	git pull --rebase
	popd > /dev/null
done
