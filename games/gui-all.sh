#!/bin/sh

for i in */
do
	pushd "$i" > /dev/null
	echo "$i"
	git gui
	popd > /dev/null
done
