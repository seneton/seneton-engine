#!/bin/sh

for i in */
do
	pushd "$i" > /dev/null
	echo "$i"
	git push
	popd > /dev/null
done
