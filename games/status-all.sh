#!/bin/sh

for i in */
do
	pushd "$i" > /dev/null
	echo "$i"
	git status
	popd > /dev/null
done
